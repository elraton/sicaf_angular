using System.Diagnostics;
	using System.Threading.Tasks;
 	using System.Collections.Generic;
	
	#r "System.Drawing.dll"
	using System.Drawing.Printing;	
	
	#r "System.Drawing.dll"
	using System.Drawing;	
	
	
  public class Startup
  {
    public async void Invoke(dynamic input)
    {
      PrintDocument recordDoc = new PrintDocument();

      Startup s = new Startup();

      recordDoc.DocumentName = "Ticket de Atención";
      recordDoc.PrintPage += new PrintPageEventHandler(s.Imprimir_Ticket_Atencion);
      recordDoc.PrintController = new StandardPrintController();

      
      recordDoc.DefaultPageSettings.PrinterSettings.PrinterName = "Epson TM-T20II";
      // PrinterSettings ps = new PrinterSettings();
      // ps.PrinterName = "Epson TM-T20II";
      // recordDoc.PrinterSettings = ps;
      recordDoc.Print();
      recordDoc.Dispose();
      return;
    }
    private void Imprimir_Ticket_Atencion(object sender, PrintPageEventArgs e)
        {
            float x = 10;
            float y = 5;
            float width = 270.0F;
            float height = 0F;

            Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
            Font drawFontArial10Bold = new Font("Arial", 8, FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 8, FontStyle.Regular);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;

            string text = "TICKET DE ATENCION";
            e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
            y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;  
        
        }
	}