"use strict";
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var electron_1 = require("electron");
var updater = require("electron-updater");
var autoUpdater = updater.autoUpdater;
var log = require("electron-log");
var fs = require('fs');
var path = require("path");
var url = require("url");
var edge = require('electron-edge-js');
var win;
var ticketWindow;
electron_1.app.on('ready', createWindow);
electron_1.app.on('activate', function () {
    if (win === null) {
        createWindow();
    }
});
///////////////////
// Auto upadater //
///////////////////
autoUpdater.requestHeaders = { "PRIVATE-TOKEN": "Q_z18F8jVQxqCotJsSJK" };
autoUpdater.autoDownload = true;
autoUpdater.setFeedURL({
    provider: "generic",
    url: "http://sicaf.fisiovida.com/installer"
});
autoUpdater.on('checking-for-update', function () {
    sendStatusToWindow('Checking for update...');
});
autoUpdater.on('update-available', function (info) {
    sendStatusToWindow('Update available.');
    var dialogOpts = {
        type: 'info',
        buttons: [],
        title: 'Actualización de aplicación',
        message: 'Se ha encontrado una nueva actualización',
        detail: 'Se comenzara a descargar, se le avisara cuando este lista.'
    };
    electron_1.dialog.showMessageBox(dialogOpts, function (response) {
        if (response == 0) {
            console.log('algo');
        }
    });
});
autoUpdater.on('update-not-available', function (info) {
    sendStatusToWindow('Update not available.');
});
autoUpdater.on('error', function (err) {
    sendStatusToWindow('Error in auto-updater.');
});
autoUpdater.on('download-progress', function (progressObj) {
    var log_message = "Download speed: " + progressObj.bytesPerSecond;
    log_message = log_message + ' - Downloaded ' + parseInt(progressObj.percent) + '%';
    log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
    log.info(log_message);
    sendStatusToWindow(log_message);
});
autoUpdater.on('update-downloaded', function (info) {
    sendStatusToWindow('Update downloaded; will install in 1 seconds');
});
autoUpdater.on('update-downloaded', function (info) {
    var dialogOpts = {
        type: 'info',
        buttons: ['Reiniciar'],
        title: 'Actualización de aplicación',
        message: 'Se termino la descarga'
    };
    electron_1.dialog.showMessageBox(dialogOpts, function (response) {
        if (response == 0) {
            autoUpdater.quitAndInstall();
        }
    });
    /*setTimeout(function () {
        autoUpdater.quitAndInstall();
    }, 1000);*/
});
//autoUpdater.checkForUpdates();
function sendStatusToWindow(message) {
    console.log(message);
}
var ticketAtencion = edge.func(function () {
    /*
    using System;
      using System.Diagnostics;
      using System.Threading.Tasks;
    using System.Collections.Generic;
      
      #r "System.Drawing.dll"
      using System.Drawing.Printing;
      
      #r "System.Drawing.dll"
      using System.Drawing;
      
      
    public class Startup
    {
  
      String title, patient, sede, personal, cita, date, hour, session;
      bool promotion;
  
      public async Task<object> Invoke(dynamic input)
      {
  
        title = (string)input.title;
        patient = (string)input.patient;
        sede = (string)input.sede;
        personal = (string)input.personal;
        cita = (string)input.cita;
        date = (string)input.date;
        hour = (string)input.hour;
        session = (string)input.session;
        promotion = (bool)input.promotion;
  
        PrintDocument recordDoc = new PrintDocument();
  
        Startup s = new Startup();
  
        recordDoc.DocumentName = "Ticket de Atención";
        recordDoc.PrintPage += new PrintPageEventHandler(s.Imprimir_Ticket_Atencion);
        recordDoc.PrintController = new StandardPrintController();
  
        
        // recordDoc.DefaultPageSettings.PrinterSettings.PrinterName = "EPSON TM-T20II";
        PrinterSettings ps = new PrinterSettings();
        ps.PrinterName = "Epson TM-T20II";
        recordDoc.PrinterSettings = ps;
        
        try {
          recordDoc.Print();
          recordDoc.Dispose();
        } catch (Exception e) {
          return e;
        }
        return "success";
      }
      private void Imprimir_Ticket_Atencion(object sender, PrintPageEventArgs e)
      {
        float x = 10;
        float y = 5;
        float width = 270.0F;
        float height = 0F;
  
        Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
        Font drawFontArial10Bold = new Font("Arial", 8, FontStyle.Bold);
        Font drawFontArial10Regular = new Font("Arial", 8, FontStyle.Regular);
        SolidBrush drawBrush = new SolidBrush(Color.Black);
  
        StringFormat drawFormatCenter = new StringFormat();
        drawFormatCenter.Alignment = StringAlignment.Center;
        StringFormat drawFormatLeft = new StringFormat();
        drawFormatLeft.Alignment = StringAlignment.Near;
        StringFormat drawFormatRight = new StringFormat();
        drawFormatRight.Alignment = StringAlignment.Far;
  
        string text = title;
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Paciente:";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = patient;
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "Sede : " + sede;
        e.Graphics.DrawString(text, drawFontArial10Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Bold).Height;
  
        text = "Terapeuta : " + personal;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Cita : " + cita;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Fecha : " + date + " Hora : " + hour;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
        
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "Sesion : " + session;
        e.Graphics.DrawString(text, drawFontArial10Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        if ( promotion ) {
          text = "PROMOCION";
          e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
          y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
          text = "----";
          e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
          y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
          text = "\n";
          e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
          y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
        }
      
      }
      }
  */
});
var ticketVenta = edge.func(function () {
    /*
    using System;
      using System.Diagnostics;
      using System.Threading.Tasks;
    using System.Collections.Generic;
      
      #r "System.Drawing.dll"
      using System.Drawing.Printing;
      
      #r "System.Drawing.dll"
      using System.Drawing;
      
      
    public class Startup
    {
  
      String title, product, brand, type, model, quantity, total;
  
      public async Task<object> Invoke(dynamic input)
      {
  
        title = (string)input.title;
        product = (string)input.product;
        brand = (string)input.brand;
        type = (string)input.type;
        model = (string)input.model;
        quantity = (string)input.quantity;
        total = (string)input.total;
  
        PrintDocument recordDoc = new PrintDocument();
  
        Startup s = new Startup();
  
        recordDoc.DocumentName = "Ticket de Atención";
        recordDoc.PrintPage += new PrintPageEventHandler(s.Imprimir_Ticket_Atencion);
        recordDoc.PrintController = new StandardPrintController();
  
        
        // recordDoc.DefaultPageSettings.PrinterSettings.PrinterName = "EPSON TM-T20II";
        PrinterSettings ps = new PrinterSettings();
        ps.PrinterName = "Epson TM-T20II";
        recordDoc.PrinterSettings = ps;
        
        try {
          recordDoc.Print();
          recordDoc.Dispose();
        } catch (Exception e) {
          return e;
        }
        return "success";
      }
      private void Imprimir_Ticket_Atencion(object sender, PrintPageEventArgs e)
      {
        float x = 10;
        float y = 5;
        float width = 270.0F;
        float height = 0F;
  
        Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
        Font drawFontArial10Bold = new Font("Arial", 8, FontStyle.Bold);
        Font drawFontArial10Regular = new Font("Arial", 8, FontStyle.Regular);
        SolidBrush drawBrush = new SolidBrush(Color.Black);
  
        StringFormat drawFormatCenter = new StringFormat();
        drawFormatCenter.Alignment = StringAlignment.Center;
        StringFormat drawFormatLeft = new StringFormat();
        drawFormatLeft.Alignment = StringAlignment.Near;
        StringFormat drawFormatRight = new StringFormat();
        drawFormatRight.Alignment = StringAlignment.Far;
  
        string text = title;
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Producto : " + product;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Marca : " + brand;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Tipo : " + type;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Modelo/Color : " + model;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Cantidad : " + quantity;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
        
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Total : " + total;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
      
      }
      }
  */
});
var ticketPago = edge.func(function () {
    /*
    using System;
      using System.Diagnostics;
      using System.Threading.Tasks;
    using System.Collections.Generic;
      
      #r "System.Drawing.dll"
      using System.Drawing.Printing;
      
      #r "System.Drawing.dll"
      using System.Drawing;
      
      
    public class Startup
    {
  
      String title, patient, price, service, payment, date, hour;
  
      public async Task<object> Invoke(dynamic input)
      {
  
        title = (string)input.title;
        patient = (string)input.patient;
        price = (string)input.price;
        service = (string)input.service;
        payment = (string)input.payment;
        date = (string)input.date;
        hour = (string)input.hour;
  
        PrintDocument recordDoc = new PrintDocument();
  
        Startup s = new Startup();
  
        recordDoc.DocumentName = "Ticket de Atención";
        recordDoc.PrintPage += new PrintPageEventHandler(s.Imprimir_Ticket_Atencion);
        recordDoc.PrintController = new StandardPrintController();
  
        
        // recordDoc.DefaultPageSettings.PrinterSettings.PrinterName = "EPSON TM-T20II";
        PrinterSettings ps = new PrinterSettings();
        ps.PrinterName = "Epson TM-T20II";
        recordDoc.PrinterSettings = ps;
        
        try {
          recordDoc.Print();
          recordDoc.Dispose();
        } catch (Exception e) {
          return e;
        }
        return "success";
      }
      private void Imprimir_Ticket_Atencion(object sender, PrintPageEventArgs e)
      {
        float x = 10;
        float y = 5;
        float width = 270.0F;
        float height = 0F;
  
        Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
        Font drawFontArial10Bold = new Font("Arial", 8, FontStyle.Bold);
        Font drawFontArial10Regular = new Font("Arial", 8, FontStyle.Regular);
        SolidBrush drawBrush = new SolidBrush(Color.Black);
  
        StringFormat drawFormatCenter = new StringFormat();
        drawFormatCenter.Alignment = StringAlignment.Center;
        StringFormat drawFormatLeft = new StringFormat();
        drawFormatLeft.Alignment = StringAlignment.Near;
        StringFormat drawFormatRight = new StringFormat();
        drawFormatRight.Alignment = StringAlignment.Far;
  
        string text = title;
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Paciente:";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = patient;
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Importe : " + price;
        e.Graphics.DrawString(text, drawFontArial10Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Servicio : " + service;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Metodo de pago : " + payment;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "Fecha : " + date + " Hora : " + hour;
        e.Graphics.DrawString(text, drawFontArial10Regular, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial10Regular).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "VERIFIQUE EL IMPORTE Y EL SERVICIO SOLICITADO.";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "UNA VEZ FIRMADO NO SE ACEPTARAN RECLAMOS POSTERIORES.";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "\n";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "FIRMA: ------------------------------";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
  
        text = "DNI:                                 .";
        e.Graphics.DrawString(text, drawFontArial12Bold, drawBrush, new RectangleF(x, y, width, height), drawFormatCenter);
        y += e.Graphics.MeasureString(text, drawFontArial12Bold).Height;
      
      }
      }
  */
});
electron_1.ipcMain.on('readyToPrintPDF', function (event) {
    console.log(event);
    /*ticketPago('JavaScript', function (error, result) {
      if (error) throw error;
      console.log(result);
    });*/
});
electron_1.ipcMain.on('ping', function (event, content) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
    return tslib_1.__generator(this, function (_a) {
        //ticketWindow.webContents.send("printTicket", content);
        //ticketWindow.show();
        if (content.title == 'TICKET DE PAGO') {
            console.log('ticket de pago');
            ticketPago(JSON.parse(JSON.stringify(content)), function (error, result) {
                if (error)
                    throw error;
                win.webContents.send('pong', 'SUCCESS');
            });
        }
        if (content.title == 'TICKET DE ATENCION') {
            console.log('ticket de atencion');
            ticketAtencion(JSON.parse(JSON.stringify(content)), function (error, result) {
                if (error)
                    throw error;
                win.webContents.send('pong', 'SUCCESS');
            });
        }
        if (content.title == 'TICKET DE VENTA') {
            console.log('ticket de venta');
            ticketVenta(JSON.parse(JSON.stringify(content)), function (error, result) {
                if (error)
                    throw error;
                win.webContents.send('pong', 'SUCCESS');
            });
        }
        return [2 /*return*/];
    });
}); });
function createWindow() {
    // printTicketPayment("LUIS VACCARI", 0, "FISIOTERAPIA ADULTO", "EFECTIVO", new Date());
    win = new electron_1.BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true,
            plugins: true
        },
    });
    win.maximize();
    //win = new BrowserWindow({ fullscreen: true })
    win.loadURL(url.format({
        pathname: path.join(__dirname, "/../../dist/index.html"),
        protocol: 'file:',
        slashes: true,
    }));
    //win.webContents.openDevTools()
    win.on('closed', function () {
        win = null;
        ticketWindow = null;
    });
    log.transports.file.level = "debug";
    autoUpdater.logger = log;
    autoUpdater.checkForUpdates();
}
//# sourceMappingURL=main.js.map