/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF, registerLocaleData } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import localeEs from '@angular/common/locales/es';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { AuthGuard } from './auth-guard.service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NbAuthModule, NbPasswordAuthStrategy, NbAuthJWTToken } from '@nebular/auth';
import { ProfileService } from './pages/profile/profile.service';
import { ProfileModule } from './pages/profile/profile.module';

// localStorage.setItem('baseurl', 'http://127.0.0.1:8000/api/v1/');
localStorage.setItem('baseurl', 'http://apisicaf.fisiovida.com/api/v1/');
registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'username',
          baseEndpoint: 'http://apisicaf.fisiovida.com/',
          login: {
            endpoint: 'api/v1/login',
          },
          register: {
            endpoint: 'api/v1/register',
          },
          token: {
            class: NbAuthJWTToken,
            key: 'success.token',
          }
        }),
      ],
      forms: {
        login: {
          redirectDelay: 0,
          showMessages: {
            success: true,
          }
        },
      },
    }),
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: LOCALE_ID, useValue: 'es' },
    { provide: APP_BASE_HREF, useValue: '/' },
    AuthGuard,
    ProfileService
  ],
})
export class AppModule {
}
