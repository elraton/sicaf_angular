import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
  {
    path: 'personal/categories',
    loadChildren: './personal_category/personal_category.module#Personal_CategoryModule',
  },
  {
    path: 'personal/schedules',
    loadChildren: './personal_schedules/personal_schedules.module#Personal_SchedulesModule',
  },
  {
    path: 'services/rates',
    loadChildren: './rates/rates.module#RatesModule',
  },
  {
    path: 'services/promos',
    loadChildren: './promos/promos.module#PromosModule',
  },
  {
    path: 'personal',
    loadChildren: './personal/personal.module#PersonalModule',
  },
  {
    path: 'assistance',
    loadChildren: './dialing/dialing.module#DialingModule',
  },
  {
    path: 'patients',
    loadChildren: './patients/patients.module#PatientsModule',
  },
  {
    path: 'treatments',
    loadChildren: './treatments/treatments.module#TreatmentsModule',
  },
  {
    path: 'cash/income',
    loadChildren: './incomecash/incomecash.module#IncomecashModule',
  },
  {
    path: 'cash/outcome',
    loadChildren: './outcomecash/outcomecash.module#OutcomecashModule',
  },
  {
    path: 'cash/sales',
    loadChildren: './salescash/salescash.module#SalescashModule',
  },
  {
    path: 'attentions',
    loadChildren: './dailyattention/dailyattention.module#DailyattentionModule',
  },
  {
    path: 'schedule',
    loadChildren: './schedule/schedule.module#ScheduleModule',
  },
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfileModule',
  },
  {
    path: 'users',
    loadChildren: './users/users.module#UsersModule',
  },
  {
    path: 'reports',
    loadChildren: './reports/reports.module#ReportsModule',
  },
  {
    path: 'agreements',
    loadChildren: './agreements/agreements.module#AgreementsModule',
  },
  {
    path: '',
    redirectTo: 'schedule',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
