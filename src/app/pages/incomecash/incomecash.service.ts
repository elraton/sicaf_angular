import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class IncomecashService {
    
    constructor(private http: HttpClient) {}

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'ingresocaja/list ');
    }

    getUser() {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'getUser');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'ingresocaja/add ', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'ingresocaja/edit ', data);
    }

    remove(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'ingresocaja/remove', data);
    }
}