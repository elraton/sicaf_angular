import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { IncomecashRoutingModule } from './incomecash-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { IncomecashComponent } from './incomecash.component';
import { AddIncomecashComponent } from './addincomecash/addincomecash.component';
import { EditIncomecashComponent } from './editincomecash/editincomecash.component';
import { ListIncomecashComponent } from './listincomecash/listincomecash.component';

// service
import { IncomecashService } from './incomecash.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UiSwitchModule } from 'ngx-toggle-switch';

const COMPONENTS = [
  IncomecashComponent,
  AddIncomecashComponent,
  EditIncomecashComponent,
  ListIncomecashComponent
];

const SERVICES = [
  IncomecashService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  IncomecashRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
  UiSwitchModule,
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class IncomecashModule { }
