import { Component, OnInit } from '@angular/core';
import { IncomecashService } from '../incomecash.service';
import { Personal_Category } from '../../../models/personal_category';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { IncomeCash } from '../../../models/incomecash';

@Component({
  selector: 'ngx-incomecash-list',
  styleUrls: ['./listincomecash.component.scss'],
  templateUrl: './listincomecash.component.html',
})
export class ListIncomecashComponent implements OnInit{

  data: IncomeCash[];
  data_parsed = [];

  loading = true;
  constructor(
    private service: IncomecashService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      FECHA: {
        title: 'Fecha',
        type: 'string',
      },
      DNI: {
        title: 'DNI',
        type: 'string',
      },
      NOMBRE_APELLIDO: {
        title: 'Paciente',
        type: 'string',
      },
      TIPO_PACIENTE: {
        title: 'Tpo paciente',
        type: 'string',
      },
      TIPO_SERVICIO: {
        title: 'Servicio',
        type: 'string',
      },
      CANTIDAD: {
        title: 'Cantidad',
        type: 'number',
      },
      TOTAL_PAGADO: {
        title: 'Total',
        type: 'number',
      },
      TIPO_COMPROBANTE: {
        title: 'Comprobante',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.service.get().subscribe(
      response => {
        this.data = JSON.parse(JSON.stringify(response));
        for ( const xx of this.data) {
          this.data_parsed.push({
            id: xx.id,
            FECHA: xx.FECHA.split(' ')[0],
            DNI: xx.DNI,
            NOMBRE_APELLIDO: xx.NOMBRE_APELLIDO,
            TIPO_PACIENTE: xx.TIPO_PACIENTE,
            TIPO_SERVICIO: xx.TIPO_SERVICIO,
            CANTIDAD: xx.CANTIDAD,
            TOTAL_PAGADO: xx.TOTAL_PAGADO,
            TIPO_COMPROBANTE: xx.TIPO_COMPROBANTE,
          });
        }
        this.source.load(this.data_parsed);
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  onAdd(event): void {
    this.router.navigate(['/pages/cash/income/add']);
  }

  onEdit(event): void {
    const data = event.data;
    if (window.confirm('No se puede editar los ingresos')) {
      return;
    } else {
      return;
    }
    const cash = this.data.find( x => x.id == data.id);
    localStorage.setItem('edit', JSON.stringify(cash));
    this.router.navigate(['/pages/cash/income/edit']);
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if (window.confirm('No puedes eliminar ingresos')) {
      /*this.service.remove(data).subscribe(
        success => {
          this.source.remove(data);
          this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          this.showToast(NbToastStatus.SUCCESS, 'Ocurrio un error', '');
        }
      );*/
      return;
    } else {
      return;
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}