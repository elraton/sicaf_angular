import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { IncomecashService } from '../incomecash.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { IncomeCash } from '../../../models/incomecash';
import { Patient } from '../../../models/patient';
import { PatientsService } from '../../patients/patients.service';
import { TreatmentsService } from '../../treatments/treatments.service';
import { Treatment } from '../../../models/treatment';
import { RatesService } from '../../rates/rates.service';
import { Rate } from '../../../models/rate';

@Component({
  selector: 'ngx-incomecash-edit',
  styleUrls: ['./editincomecash.component.scss'],
  templateUrl: './editincomecash.component.html',
  providers: [PatientsService, TreatmentsService, RatesService]
})
export class EditIncomecashComponent {
  Form: FormGroup;
  data: IncomeCash = JSON.parse(localStorage.getItem('edit'));
  user;
  patient: Patient;
  oldQuantity = 0;
  patientList: Patient[] = [];
  loading = false;
  alltreatments: Treatment[] = [];
  treatmentList: Treatment[] = [];
  treatmentSelected: Treatment;
  serviceSelected: Rate;
  servicesList: Rate[] = [];
  treatmentList_parsed = [];

  statics = new Statics();

  payment_method = this.statics.PAYMENT_METHOD;
  comprobantes = this.statics.COMPROBANTES;

  showpromotions = true;

  promotions = [
    {id: 1, value: 'Promoción 3 x 2'},
    {id: 2, value: 'Promoción 10 + 1'},
    {id: 3, value: 'Promoción 20 + 2'},
    {id: 4, value: 'Campaña Evaluación T.F 50%'},
    {id: 5, value: 'Tarifa Promocional'}
  ];

  promotions_parsed = [
    {id: 1, value: 'Promoción 3 x 2'},
    {id: 2, value: 'Promoción 10 + 1'},
    {id: 3, value: 'Promoción 20 + 2'},
    {id: 4, value: 'Campaña Evaluación T.F 50%'},
    {id: 5, value: 'Tarifa Promocional'}
  ];
  
  constructor(
    private service: IncomecashService,
    private toastrService: NbToastrService,
    private treatmentService: TreatmentsService,
    private location: Location,
    private patientService: PatientsService,
    private router: Router,
    private servicesService: RatesService,
  ) {
    this.generateForm();
    this.service.getUser().subscribe(
      data => {
        this.user = JSON.parse(JSON.stringify(data)).success;
      },
      error => {}
    )
  }

  generateForm() {
    this.oldQuantity = this.data.CANTIDAD;

    const promo = this.promotions.find( x => x.value == this.data.PROMO_MENSUAL);
    this.Form = new FormGroup({
      DNI: new FormControl({value: this.data.DNI, disabled: true}),
      NOMBRE_APELLIDO: new FormControl({value: this.data.NOMBRE_APELLIDO, disabled: true}),
      CANTIDAD: new FormControl({value: this.data.CANTIDAD, disabled: true}, Validators.required),
      PRECIO_UNIDAD: new FormControl({ value:0, disabled: true}, Validators.required),
      SUBTOTAL: new FormControl({ value:0, disabled: true}, Validators.required),
      IGV: new FormControl({ value:0, disabled: true}, Validators.required),
      TOTAL: new FormControl({ value:0, disabled: true}, Validators.required),
      DESCUENTO: new FormControl({ value:0, disabled: true}, Validators.required),
      TOTAL_DESCUENTO: new FormControl({ value:0, disabled: true}, Validators.required),
      TOTAL_PAGADO: new FormControl({ value:0, disabled: true}, Validators.required),
      PROMO_MENSUAL: new FormControl({value: this.data.PROMO_MENSUAL, disabled: false}, Validators.required),
      COD_PROMOCION: new FormControl({value: '', disabled: true}),
      TIPO_CODIGO: new FormControl({ value: 0, disabled: true}, Validators.required), // de cual tabla escoge
      FORMA_PAGO: new FormControl({value: this.data.FORMA_PAGO, disabled: true}, Validators.required), // efectivo o tarjeta
      TIPO_COMPROBANTE: new FormControl({value: this.data.TIPO_COMPROBANTE, disabled: true}, Validators.required), // Boleta // factura // ticket
      treatment: new FormControl({value: +this.data.TIPO_CODIGO, disabled: true}, Validators.required),
      sessions: new FormControl({value: 0, disabled: true}),
    });
    this.verifyDNI();
    this.changeQuantity();
    this.Form.get('treatment').disable();
  }

  numberOnly(event) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  searchPromotion(){

  }

  onlynumbers(event) {
    if ( event.keyCode == 46 || event.keyCode == 8 ) {
      // let it happen, don't do anything
    } else {
      // Ensure that it is a number and stop the keypress
      if (event.keyCode < 48 || event.keyCode > 57 ) {
          event.preventDefault(); 
      }
    }
  }

  clearFields() {
    this.Form.get('CANTIDAD').setValue('');
    this.Form.get('PROMO_MENSUAL').setValue('');
    this.Form.get('COD_PROMOCION').setValue('');
    this.Form.get('FORMA_PAGO').setValue('');
    this.Form.get('TIPO_COMPROBANTE').setValue('');
    this.Form.get('treatment').setValue('');
  }

  disableFields() {
    this.Form.get('CANTIDAD').disable();
    this.Form.get('PROMO_MENSUAL').disable();
    this.Form.get('COD_PROMOCION').disable();
    this.Form.get('FORMA_PAGO').disable();
    this.Form.get('TIPO_COMPROBANTE').disable();
    this.Form.get('treatment').disable();
  }

  enableFields() {
    this.Form.get('CANTIDAD').enable();
    this.Form.get('PROMO_MENSUAL').enable();
    this.Form.get('COD_PROMOCION').enable();
    this.Form.get('FORMA_PAGO').enable();
    this.Form.get('TIPO_COMPROBANTE').enable();
    // this.Form.get('treatment').enable();
  }

  async verifyDNI() {
    if (this.Form.get('DNI').value.length == 8) {
      if (this.patientList.length == 0) {
        await this.patientService.get().toPromise().then(data => {this.patientList = JSON.parse(JSON.stringify(data));}).catch();
      }
      if (this.servicesList.length == 0) {
        await this.servicesService.get().toPromise().then(data => {this.servicesList = JSON.parse(JSON.stringify(data));}).catch();
      }
      this.patient = this.patientList.find( x => x.DNI == this.Form.get('DNI').value);
      if (this.patient == null) {
        this.showToast(NbToastStatus.DANGER, 'DNI invalido', '');
        this.clearFields();
        this.disableFields();
      } else {
        this.loading = true;
        if ( this.alltreatments.length == 0 ){
          await this.treatmentService.get().toPromise().then(
            (data) => {
              this.alltreatments = JSON.parse(JSON.stringify(data));
            }
          ).catch()
        }

        this.Form.get('NOMBRE_APELLIDO').setValue(this.patient.P_NOMBRE + ' ' + this.patient.P_APELLIDO);

        const alltreatments_aux = JSON.parse(JSON.stringify(this.alltreatments));
        this.treatmentList = [];
        this.treatmentList = alltreatments_aux.filter( x => x.DNI == this.Form.get('DNI').value);
        this.treatmentList_parsed = [];
        for ( const xx of this.treatmentList ) {
          console.log(this.treatmentList.length);
          this.treatmentList_parsed.push({
            id: xx.id,
            SERVICIO: this.parseService(xx.SERVICIO),
            S_PROGRAMADAS: xx.S_PROGRAMADAS,
            S_PAGADAS: xx.S_PAGADAS,
          });
        }
        if (this.treatmentList_parsed.length == 0){
          this.showToast(NbToastStatus.DANGER, 'No tiene tratamientos o consultas activas', '');
          this.clearFields();
          this.disableFields();
        } else {
          // this.Form.get('treatment').setValue(this.treatmentList_parsed[0].id);
          const treatment = this.treatmentList_parsed.find( x => x.id == +this.Form.get('treatment').value);
          this.Form.get('sessions').setValue(treatment.S_PROGRAMADAS - treatment.S_PAGADAS);
          this.treatmentSelected = this.treatmentList.find( x => x.id == treatment.id );
          this.serviceSelected = this.servicesList.find( x => x.id == this.treatmentSelected.SERVICIO);
          if ( this.treatmentSelected.DOCTOR !== null) {
            this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
            this.promotions_parsed = this.promotions_parsed.filter( x => x.id == 4);
          } else {
            this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
            this.promotions_parsed = this.promotions_parsed.filter( x => x.id != 4);
          }
        }

        if (this.patient.TIPO_PACIENTE !== 'SEGURO') {
          if (this.patient.TIPO_SEGURO !== 'FULLPRICE') {
            this.showpromotions = false;
          }
        }

        this.loading = false;
        this.enableFields();
        this.changeQuantity();
      }
    }
  }

  parseService(idService) {
    return this.servicesList.find( x => x.id == idService).SERVICIO;
  }

  changeQuantity() {
    /*console.log(this.patient.TIPO_SEGURO);
    console.log(this.patient.TIPO_PACIENTE);
    console.log(this.patient.COPAGO_FIJO);
    console.log(this.patient.COPAGO_VARIABLE);*/
    console.log(this.treatmentSelected);

    const limit = +this.Form.get('sessions').value;
    if ( this.Form.get('PROMO_MENSUAL').value == 1 || this.Form.get('PROMO_MENSUAL').value == 2 || this.Form.get('PROMO_MENSUAL').value == 3 ) {
      if ( this.Form.get('PROMO_MENSUAL').value == 1 ) {
        if ( limit < 2) {
          this.showToast(NbToastStatus.DANGER, 'No puede ser mayor a las sesiones programadas', '');
          this.Form.get('CANTIDAD').setValue('');
          return;
        }
      }
      if ( this.Form.get('PROMO_MENSUAL').value == 2 ) {
        if ( limit < 10 ) {
          this.showToast(NbToastStatus.DANGER, 'No puede ser mayor a las sesiones programadas', '');
          this.Form.get('CANTIDAD').setValue('');
          return;
        }
      }
      if ( this.Form.get('PROMO_MENSUAL').value == 3 ) {
        if ( limit < 20 ) {
          this.showToast(NbToastStatus.DANGER, 'No puede ser mayor a las sesiones programadas', '');
          this.Form.get('CANTIDAD').setValue('');
          return;
        }
      }
    } else {
      if ( +this.Form.get('CANTIDAD').value > limit) {
        this.showToast(NbToastStatus.DANGER, 'No puede editar sesiones ya realizadas', '');
        this.disableFields();
        setTimeout(() => {
          this.router.navigate(['/pages/cash/income/list']);          
        }, 2000);
        return;
      }
    }
    
    const quantity = +this.Form.get('CANTIDAD').value;
    
    if ( this.patient.TIPO_PACIENTE == 'SEGURO') {
      // fullprice
      this.showpromotions = false;
      const cop_fijo = +this.patient.COPAGO_FIJO;
      const cop_var = +this.patient.COPAGO_VARIABLE / 100;

      
      let price = 0;
      if ( this.treatmentSelected.DOCTOR !== null ) {
        price = cop_fijo;
      } else {
        price = +this.serviceSelected.PRECIO - (+this.serviceSelected.PRECIO * cop_var);
      }
      const igv = (price * quantity) * 0.18;
      
      this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
      this.Form.get('SUBTOTAL').setValue( ( (quantity * price) - igv).toFixed(2) );
      this.Form.get('IGV').setValue( igv.toFixed(2) );
      this.Form.get('TOTAL').setValue( (quantity * price).toFixed(2) );
      this.Form.get('TOTAL_PAGADO').setValue( (quantity * price).toFixed(2) );

    } else {
      if ( this.treatmentSelected.DOCTOR !== null ) {
        let price = +this.serviceSelected.PRECIO;
        const igv = (price * quantity) * 0.18;
        
        this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
        this.Form.get('SUBTOTAL').setValue( ( (quantity * price) - igv).toFixed(2) );
        this.Form.get('IGV').setValue( igv.toFixed(2) );
        this.Form.get('TOTAL').setValue( (quantity * price).toFixed(2) );
        this.Form.get('TOTAL_PAGADO').setValue( (quantity * price).toFixed(2) );
        
        // this.Form.get('PROMO_MENSUAL').value;
        // this.Form.get('COD_PROMOCION').value;

        this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
        this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
      } else {
        if (this.patient.TIPO_SEGURO == 'FULLPRICE') {
          if (this.Form.get('PROMO_MENSUAL').value > 0) {
            this.changePromotion();
            return;
          }
          let price = +this.serviceSelected.PRECIO;
          const igv = (price * quantity) * 0.18;
          
          this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
          this.Form.get('SUBTOTAL').setValue( ( (quantity * price) - igv).toFixed(2) );
          this.Form.get('IGV').setValue( igv.toFixed(2) );
          this.Form.get('TOTAL').setValue( (quantity * price).toFixed(2) );
          this.Form.get('TOTAL_PAGADO').setValue( (quantity * price).toFixed(2) );
          
          // this.Form.get('PROMO_MENSUAL').value;
          // this.Form.get('COD_PROMOCION').value;
  
          this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
          this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
        } else {
          let price = +this.serviceSelected.PRECIO;
          let igv = 0;
          if (this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {
            igv = 0;
          } else {
            igv = (quantity * price) * 0.18;
          }
          
          this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
          this.Form.get('SUBTOTAL').setValue( (quantity * price).toFixed(2) );
          this.Form.get('IGV').setValue( igv.toFixed(2) );
          this.Form.get('TOTAL').setValue( ( (quantity * price) + igv ).toFixed(2) );
          this.Form.get('TOTAL_PAGADO').setValue( ( (quantity * price) + igv ).toFixed(2) );
          
          // this.Form.get('PROMO_MENSUAL').value;
          // this.Form.get('COD_PROMOCION').value;
  
          this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
          this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
        }
      }
    }
    
  }

  changePromotion() {
    const promo = +this.Form.get('PROMO_MENSUAL').value;
    switch(promo) {
      case -1: {
        this.Form.get('CANTIDAD').enable();
        this.changeQuantity();
        break;
      }
      case 0: {
        this.Form.get('CANTIDAD').enable();
        this.changeQuantity();
        break;
      }
      case 1: {
        this.Form.get('CANTIDAD').setValue(3);
        this.Form.get('CANTIDAD').disable();
        const quantity = 2;
        let price = +this.serviceSelected.PRECIO;
        let igv = 0;
        if (this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {
          igv = 0;
        } else {
          igv = (quantity * price) * 0.18;
        }
        this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
        this.Form.get('SUBTOTAL').setValue( (quantity * price).toFixed(2) );
        this.Form.get('IGV').setValue( igv.toFixed(2) );
        this.Form.get('TOTAL').setValue( ( (quantity * price) + igv ).toFixed(2) );
        this.Form.get('TOTAL_PAGADO').setValue( ( (quantity * price) + igv ).toFixed(2) );
        
        // this.Form.get('PROMO_MENSUAL').value;
        // this.Form.get('COD_PROMOCION').value;

        this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
        this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
        break;
      }
      case 2: {
        if ( +this.Form.get('sessions').value < 10) {
          this.showToast(NbToastStatus.DANGER, 'No tiene las suficientes sesiones', '');
          this.Form.get('CANTIDAD').setValue('');
          this.Form.get('PROMO_MENSUAL').setValue(0);
          return;
        }
        this.Form.get('CANTIDAD').setValue(11);
        this.Form.get('CANTIDAD').disable();
        const quantity = 10;
        let price = +this.serviceSelected.PRECIO;
        let igv = 0;
        if (this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {
          igv = 0;
        } else {
          igv = (quantity * price) * 0.18;
        }
        this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
        this.Form.get('SUBTOTAL').setValue( (quantity * price).toFixed(2) );
        this.Form.get('IGV').setValue( igv.toFixed(2) );
        this.Form.get('TOTAL').setValue( ( (quantity * price) + igv ).toFixed(2) );
        this.Form.get('TOTAL_PAGADO').setValue( ( (quantity * price) + igv ).toFixed(2) );
        
        // this.Form.get('PROMO_MENSUAL').value;
        // this.Form.get('COD_PROMOCION').value;

        this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
        this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
        break;
      }
      case 3: {
        if ( +this.Form.get('sessions').value < 20) {
          this.showToast(NbToastStatus.DANGER, 'No tiene las suficientes sesiones', '');
          this.Form.get('CANTIDAD').setValue('');
          this.Form.get('PROMO_MENSUAL').setValue(0);
          return;
        }
        this.Form.get('CANTIDAD').setValue(22);
        this.Form.get('CANTIDAD').disable();
        const quantity = 20;
        let price = +this.serviceSelected.PRECIO;
        let igv = 0;
        if (this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {
          igv = 0;
        } else {
          igv = (quantity * price) * 0.18;
        }
        this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
        this.Form.get('SUBTOTAL').setValue( (quantity * price).toFixed(2) );
        this.Form.get('IGV').setValue( igv.toFixed(2) );
        this.Form.get('TOTAL').setValue( ( (quantity * price) + igv ).toFixed(2) );
        this.Form.get('TOTAL_PAGADO').setValue( ( (quantity * price) + igv ).toFixed(2) );
        
        // this.Form.get('PROMO_MENSUAL').value;
        // this.Form.get('COD_PROMOCION').value;

        this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
        this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
        break;
      }
      case 4: {
        break;
      }
      case 5: {
        const quantity = this.Form.get('CANTIDAD').value;
        let price = +this.serviceSelected.PRECIO - 10;
        let igv = 0;
        if (this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {
          igv = 0;
        } else {
          igv = (quantity * price) * 0.18;
        }
        this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
        this.Form.get('SUBTOTAL').setValue( (quantity * price).toFixed(2) );
        this.Form.get('IGV').setValue( igv.toFixed(2) );
        this.Form.get('TOTAL').setValue( ( (quantity * price) + igv ).toFixed(2) );
        this.Form.get('TOTAL_PAGADO').setValue( ( (quantity * price) + igv ).toFixed(2) );
        
        // this.Form.get('PROMO_MENSUAL').value;
        // this.Form.get('COD_PROMOCION').value;

        this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
        this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
        break;
      }
    }
  }

  changeTreatment() {
    this.treatmentSelected = this.treatmentList.find( x => x.id == this.Form.get('treatment').value)
    this.serviceSelected = this.servicesList.find( x => x.id == this.treatmentSelected.SERVICIO);
    this.Form.get('sessions').setValue(this.treatmentSelected.S_PROGRAMADAS - this.treatmentSelected.S_PAGADAS);
    this.Form.get('CANTIDAD').setValue('');
    if ( this.treatmentSelected.DOCTOR !== null) {
      this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
      this.promotions_parsed = this.promotions_parsed.filter( x => x.id == 4);
    } else {
      this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
      this.promotions_parsed = this.promotions_parsed.filter( x => x.id != 4);
    }
    this.changeQuantity();
  }

  async save() {

    if (this.Form.valid) {
      const today = new Date();
      this.data.FECHA = today.toISOString().split('T')[0] + ' ' + today.getHours()+':'+today.getMinutes()+':00';
      this.data.CANTIDAD = this.Form.get('CANTIDAD').value;
      this.data.SUBTOTAL = this.Form.get('SUBTOTAL').value;
      this.data.IGV = this.Form.get('IGV').value;
      this.data.TOTAL = this.Form.get('TOTAL').value;
      this.data.TOTAL_DESCUENTO = this.Form.get('TOTAL_DESCUENTO').value;
      this.data.DESCUENTO = this.Form.get('DESCUENTO').value;
      this.data.TOTAL_PAGADO = this.Form.get('TOTAL_PAGADO').value;
      this.data.PROMO_MENSUAL = this.Form.get('PROMO_MENSUAL').value;
      this.data.COD_PROMOCION = this.Form.get('COD_PROMOCION').value; // tabla 2 de promociones
      this.data.FORMA_PAGO = this.Form.get('FORMA_PAGO').value;
      this.data.TIPO_COMPROBANTE = this.Form.get('TIPO_COMPROBANTE').value; // Boleta // factura // ticket
      this.data.RECEPTOR = this.user.NOMBRE + ' ' + this.user.APELLIDO;
      this.data.TURNO = this.user.TURNO;
      this.data.SEDE = this.user.SEDE;

      await this.service.update(this.data).toPromise().then(
        async res => {
          this.treatmentSelected.S_PAGADAS = this.treatmentSelected.S_PAGADAS - this.oldQuantity;
          this.treatmentSelected.S_PAGADAS = this.treatmentSelected.S_PAGADAS + +this.Form.get('CANTIDAD').value;
          await this.treatmentService.update(this.treatmentSelected).toPromise().then().catch();
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/cash/income/list']);
        }
      ).catch(
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );


    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/cash/income/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}