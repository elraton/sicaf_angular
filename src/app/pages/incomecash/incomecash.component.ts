import { Component } from '@angular/core';

@Component({
  selector: 'ngx-incomecash',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class IncomecashComponent {
}
