import { Component, NgZone, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { IncomecashService } from '../incomecash.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { IncomeCash } from '../../../models/incomecash';
import { Patient } from '../../../models/patient';
import { PatientsService } from '../../patients/patients.service';
import { TreatmentsService } from '../../treatments/treatments.service';
import { Treatment } from '../../../models/treatment';
import { RatesService } from '../../rates/rates.service';
import { Rate } from '../../../models/rate';
import { IpcService } from '../../ipcService.service';
import { PromosService } from '../../promos/promos.service';
import { Promos } from '../../../models/promos';
import { Agreement } from '../../../models/agreement';
import { AgreementsService } from '../../agreements/agreements.service';

interface Window {
  require: NodeRequire;
}

@Component({
  selector: 'ngx-incomecash-add',
  styleUrls: ['./addincomecash.component.scss'],
  templateUrl: './addincomecash.component.html',
  providers: [PatientsService, AgreementsService, TreatmentsService, RatesService, IpcService, PromosService]
})
export class AddIncomecashComponent implements AfterViewInit {
  hour = '';
  minutes = '';
  ampm = '';
  Form: FormGroup;
  data: IncomeCash;
  user;
  patient: Patient;
  patientList: Patient[] = [];
  loading = false;
  alltreatments: Treatment[] = [];
  treatmentList: Treatment[] = [];
  treatmentSelected: Treatment;
  serviceSelected: Rate;
  servicesList: Rate[] = [];
  treatmentList_parsed = [];

  statics = new Statics();

  payment_method = this.statics.PAYMENT_METHOD;
  comprobantes = this.statics.COMPROBANTES;

  showpromotions = true;
  disablePromotions = false;

  promotionFlag = true;

  promotions: Promos[] = [];

  promotions_parsed = [];
  promotion_selected: Promos;

  discount = 0;
  free_consults = 0;

  convenios_list: Agreement[] = [];

  @ViewChild("dni") dniInput: ElementRef;
  
  constructor(
    private service: IncomecashService,
    private toastrService: NbToastrService,
    private agreementService: AgreementsService,
    private treatmentService: TreatmentsService,
    private location: Location,
    private patientService: PatientsService,
    private router: Router,
    private servicesService: RatesService,
    private promoService: PromosService,
    private ipcService: IpcService,
    private _ngZone: NgZone
  ) {
    this.generateForm();

    this.agreementService.get().subscribe(
      data => {
        this.convenios_list = data;
      },
      error => {}
    );
    this.service.getUser().subscribe(
      data => {
        this.user = JSON.parse(JSON.stringify(data)).success;
      },
      error => {}
    );

    setInterval(() => {
      const today = new Date();
      const hour = today.getHours() > 12 ? today.getHours() - 12 : today.getHours();
      this.minutes = today.getMinutes() < 10 ? '0' + today.getMinutes() : '' + today.getMinutes();
      this.hour = hour < 10 ? '0' + hour : '' + hour;
      this.ampm = today.getHours() >= 12 ? 'pm' : 'am';
  }, 1000)

    
  }

  ngAfterViewInit() {
    this.dniInput.nativeElement.focus();
  }

  sendTicket() {
    const price: number = +this.Form.get('TOTAL_PAGADO').value;
    const treatment = this.treatmentList.find(x => x.id == this.Form.get('treatment').value);
    const service = this.servicesList.find(x => x.id == treatment.SERVICIO);
    const data = {
      title: 'TICKET DE PAGO',
      patient: this.Form.get('NOMBRE_APELLIDO').value,
      price: price.toFixed(2),
      service: service.SERVICIO,
      payment: this.Form.get('FORMA_PAGO').value,
      date: this.getDateTicket(),
      hour: this.hour + ':' + this.minutes + ' ' + this.ampm,
    }
    console.log('ticket')
    console.table(data);
    this.ipcService.ping(data).then(
      data => {
        console.log(data);
      }
    );
  }

  generateForm() {
    this.Form = new FormGroup({
      DNI: new FormControl('', Validators.required),
      NOMBRE_APELLIDO: new FormControl({value: '', disabled: true}, Validators.required),
      CANTIDAD: new FormControl({value: '', disabled: true}, Validators.required),
      PRECIO_UNIDAD: new FormControl({ value:0, disabled: true}, Validators.required),
      SUBTOTAL: new FormControl({ value:0, disabled: true}, Validators.required),
      IGV: new FormControl({ value:0, disabled: true}, Validators.required),
      TOTAL: new FormControl({ value:0, disabled: true}, Validators.required),
      DESCUENTO: new FormControl({ value:0, disabled: true}, Validators.required),
      TOTAL_DESCUENTO: new FormControl({ value:0, disabled: true}, Validators.required),
      TOTAL_PAGADO: new FormControl({ value:0, disabled: true}, Validators.required),
      PROMO_MENSUAL: new FormControl({value: 0, disabled: true}, Validators.required),
      COD_PROMOCION: new FormControl({value: '', disabled: true}),
      TIPO_CODIGO: new FormControl({ value: 0, disabled: true}, Validators.required), // de cual tabla escoge
      FORMA_PAGO: new FormControl({value: 'EFECTIVO', disabled: true}, Validators.required), // efectivo o tarjeta
      TIPO_COMPROBANTE: new FormControl({value: 'TICKET', disabled: true}, Validators.required), // Boleta // factura // ticket
      treatment: new FormControl({value: '', disabled: true}, Validators.required),
      sessions: new FormControl({value: 0, disabled: true}),
    });
  }

  numberOnly(event) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  searchPromotion(){

  }

  onlynumbers(event) {
    if ( event.keyCode == 46 || event.keyCode == 8 ) {
      // let it happen, don't do anything
    } else {
      // Ensure that it is a number and stop the keypress
      if (event.keyCode < 48 || event.keyCode > 57 ) {
          event.preventDefault(); 
      }
    }
  }

  clearFields() {
    this.Form.get('CANTIDAD').setValue('');
    this.Form.get('PROMO_MENSUAL').setValue('');
    this.Form.get('COD_PROMOCION').setValue('');
    this.Form.get('FORMA_PAGO').setValue('');
    this.Form.get('TIPO_COMPROBANTE').setValue('');
    this.Form.get('treatment').setValue('');
  }

  disableFields() {
    this.Form.get('CANTIDAD').disable();
    this.Form.get('PROMO_MENSUAL').disable();
    this.Form.get('COD_PROMOCION').disable();
    this.Form.get('FORMA_PAGO').disable();
    this.Form.get('TIPO_COMPROBANTE').disable();
    this.Form.get('treatment').disable();
    this.promotionFlag = true;
  }

  enableFields() {
    this.Form.get('CANTIDAD').enable();
    this.Form.get('PROMO_MENSUAL').enable();
    this.Form.get('COD_PROMOCION').enable();
    this.Form.get('FORMA_PAGO').enable();
    this.Form.get('TIPO_COMPROBANTE').enable();
    this.Form.get('treatment').enable();
    this.promotionFlag = false;
  }

  togglePromotion() {
    this.showpromotions = !this.showpromotions;
    this.changeQuantity();
  }

  async verifyDNI() {

    if (this.Form.get('DNI').value.length == 8) {
      if (this.patientList.length == 0) {
        await this.patientService.get().toPromise().then(data => {this.patientList = JSON.parse(JSON.stringify(data));}).catch();
      }
      if (this.servicesList.length == 0) {
        await this.servicesService.get().toPromise().then(data => {this.servicesList = JSON.parse(JSON.stringify(data));}).catch();
      }
      if (this.promotions.length == 0) {
        await this.promoService.get().toPromise().then( data => this.promotions = JSON.parse(JSON.stringify(data))).catch();
      }
      this.patient = this.patientList.find( x => x.DNI == this.Form.get('DNI').value);
      if (this.patient == null) {
        this.showToast(NbToastStatus.DANGER, 'DNI invalido', '');
        this.clearFields();
        this.disableFields();
        return;
      } else {
        this.loading = true;
        if ( this.alltreatments.length == 0 ){
          await this.treatmentService.get().toPromise().then(
            (data) => {
              this.alltreatments = JSON.parse(JSON.stringify(data));
            }
          ).catch()
        }

        this.Form.get('NOMBRE_APELLIDO').setValue(this.patient.P_NOMBRE + ' ' + this.patient.P_APELLIDO);

        const alltreatments_aux = JSON.parse(JSON.stringify(this.alltreatments));
        this.treatmentList = [];
        this.treatmentList = alltreatments_aux.filter( x => x.DNI == this.Form.get('DNI').value && x.ESTADO == 'ACTIVO');
        this.treatmentList_parsed = [];
        for ( const xx of this.treatmentList ) {
          this.treatmentList_parsed.push({
            id: xx.id,
            SERVICIO: this.parseService(xx.SERVICIO),
            S_PROGRAMADAS: xx.S_PROGRAMADAS,
            S_PAGADAS: xx.S_PAGADAS,
          });
        }
        
        if (this.treatmentList_parsed.length == 0){
          this.showToast(NbToastStatus.DANGER, 'No tiene tratamientos o consultas activas', '');
          this.clearFields();
          this.disableFields();
        } else {
          this.Form.get('treatment').setValue(this.treatmentList_parsed[0].id);
          this.Form.get('sessions').setValue(Number(this.treatmentList_parsed[0].S_PROGRAMADAS) - Number(this.treatmentList_parsed[0].S_PAGADAS));
          this.treatmentSelected = this.treatmentList[0];
          this.serviceSelected = this.servicesList.find( x => x.id == this.treatmentSelected.SERVICIO);
          this.promotions_parsed = [];
          for ( const promotion of JSON.parse(this.serviceSelected.promotions) ) {
            const promo = this.promotions.find(x => x.id == +promotion.promo);
            this.promotions_parsed.push(promo);
          }
          /*if ( this.treatmentSelected.DOCTOR !== null) {
            this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
            this.promotions_parsed = this.promotions_parsed.filter( x => x.id == 4);
          } else {
            this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
            this.promotions_parsed = this.promotions_parsed.filter( x => x.id != 4);
          }*/
        }

        if (this.patient.TIPO_PACIENTE.toUpperCase() !== 'SEGURO') {
          if (this.patient.TIPO_SEGURO.toUpperCase() !== 'FULLPRICE') {
            this.showpromotions = false;
            this.promotionFlag = true;
          }
        } else {
          this.showpromotions = false;
          this.promotionFlag = true;          
        }

        this.loading = false;
        this.enableFields();
        this.changeTreatment();
      }
    }
  }

  parseService(idService) {
    return this.servicesList.find( x => x.id == idService).SERVICIO;
  }

  getDateTicket() {
    const today = new Date();
    const day = today.getDate() < 10 ? '0' + today.getDate() : '' + today.getDate();
    const month = today.getMonth() + 1;
    const month2 = month < 10 ? '0' + month : '' + month;
    return day + '/' + month2 + '/' + today.getFullYear();
  }

  changeQuantity() {
    const limit = +this.Form.get('sessions').value;    
    let quantity = +this.Form.get('CANTIDAD').value;

    if ( quantity > limit && this.promotion_selected == undefined ) {
      this.showToast(NbToastStatus.DANGER, 'No puede ser mayor al limite de sesiones', '');
      this.Form.get('CANTIDAD').setValue('');
      return;
    }
    
    if ( this.patient.TIPO_PACIENTE.toUpperCase() == 'SEGURO') {
      // fullprice
      const cop_fijo = +this.patient.COPAGO_FIJO;
      const cop_var = +this.patient.COPAGO_VARIABLE / 100;
      
      let price = 0;
      if ( this.treatmentSelected.DOCTOR !== null ) {
        price = cop_fijo;
      } else {
        if ( this.serviceSelected.CODIGO.toUpperCase().includes('FISICA') ) {
          price = +this.serviceSelected.fullprice - (+this.serviceSelected.fullprice * cop_var);
        } else {
          price = +this.serviceSelected.fullprice;
          if ( this.discount > 0 ) {
            price = price - ( price * this.discount / 100 );
          }
        }
      }
      const igv = (price * quantity) * 0.18;
      
      this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
      this.Form.get('SUBTOTAL').setValue( ( (quantity * price) - igv).toFixed(2) );
      this.Form.get('IGV').setValue( igv.toFixed(2) );
      this.Form.get('TOTAL').setValue( (quantity * price).toFixed(2) );
      this.Form.get('TOTAL_PAGADO').setValue( (quantity * price).toFixed(2) );

    } else {
      if ( this.treatmentSelected.DOCTOR !== null ) {
        let price = 0;
        price = Number(this.serviceSelected.fullprice);

        if ( this.discount > 0 ) {
          price = price - ( price * this.discount / 100 );
        }

        const igv = (price * quantity) * 0.18;
        
        this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
        this.Form.get('SUBTOTAL').setValue( ( (quantity * price) - igv).toFixed(2) );
        this.Form.get('IGV').setValue( igv.toFixed(2) );
        this.Form.get('TOTAL').setValue( (quantity * price).toFixed(2) );
        this.Form.get('TOTAL_PAGADO').setValue( (quantity * price).toFixed(2) );

        this.Form.get('DESCUENTO').setValue( (0).toFixed(2) );
        this.Form.get('TOTAL_DESCUENTO').setValue( (0).toFixed(2) );
      } else {
        // tratamientos
        if (this.patient.TIPO_SEGURO == 'FULLPRICE') {
          let price = +this.serviceSelected.fullprice;
          if ( +this.free_consults > 0 ) {
            quantity = quantity - Number(this.free_consults);
          }
          if ( this.discount > 0 ) {
            price = price - ( price * this.discount / 100 );
          }

          



          if ( this.showpromotions ) {
            if (this.patient.CONVENIO) {
              console.log('convenio');
              const convenio = this.convenios_list.find(x => x.id == this.patient.CONVENIO);
              console.log(convenio);
              if (convenio) {
                price = price - (price * convenio.PORCENTAJE / 100);
                console.log('convenio');
                console.log(price);
              }
            } else {
              price = +this.serviceSelected.promocional;
            }
            const igv = (price * quantity) * 0.18;
            if (this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {          
              this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
              this.Form.get('SUBTOTAL').setValue( ( (quantity * price) - igv).toFixed(2) );
              this.Form.get('IGV').setValue( igv.toFixed(2) );
              this.Form.get('TOTAL').setValue( (quantity * price).toFixed(2) );
              this.Form.get('TOTAL_PAGADO').setValue( (quantity * price).toFixed(2) );
            } else {          
              this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
              this.Form.get('SUBTOTAL').setValue( (quantity * price).toFixed(2) );
              this.Form.get('IGV').setValue( igv.toFixed(2) );
              this.Form.get('TOTAL').setValue( ( (quantity * price) + igv ).toFixed(2) );
              this.Form.get('TOTAL_PAGADO').setValue( ( (quantity * price) + igv ).toFixed(2) );
            }
          } else {
            if (this.patient.CONVENIO) {
              console.log('convenio');
              const convenio = this.convenios_list.find(x => x.id == this.patient.CONVENIO);
              console.log(convenio);
              if (convenio) {
                price = price - (price * convenio.PORCENTAJE / 100);
                console.log('convenio');
                console.log(price);
              }
            }
            const igv = (price * quantity) * 0.18;
          
            this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
            this.Form.get('SUBTOTAL').setValue( ( (quantity * price) - igv).toFixed(2) );
            this.Form.get('IGV').setValue( igv.toFixed(2) );
            this.Form.get('TOTAL').setValue( (quantity * price).toFixed(2) );
            this.Form.get('TOTAL_PAGADO').setValue( (quantity * price).toFixed(2) );
          }

        } else {
          let price = 0;
          if (this.patient.TIPO_SEGURO == 'PROMOCIONAL') {
            price = +this.serviceSelected.promocional;
          }
          if (this.patient.TIPO_SEGURO == 'ESPECIAL') {
            price = +this.serviceSelected.especial;
          }
          if (this.patient.TIPO_SEGURO == 'FAMILIAR') {
            price = +this.serviceSelected.familiar;
          }

          if ( this.discount > 0 ) {
            price = +this.serviceSelected.fullprice;
            price = price - ( price * this.discount / 100 );
          }


          let igv = 0;
          if (this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {
            igv = 0;
          } else {
            igv = (quantity * price) * 0.18;
          }
          
          this.Form.get('PRECIO_UNIDAD').setValue( price.toFixed(2) );
          this.Form.get('SUBTOTAL').setValue( (quantity * price).toFixed(2) );
          this.Form.get('IGV').setValue( igv.toFixed(2) );
          this.Form.get('TOTAL').setValue( ( (quantity * price) + igv ).toFixed(2) );
          this.Form.get('TOTAL_PAGADO').setValue( ( (quantity * price) + igv ).toFixed(2) );
        }
      }
    }
    
  }

  changePayment() {
    if ( this.Form.get('FORMA_PAGO').value.toUpperCase() !== 'EFECTIVO') {
      if ( this.Form.get('TIPO_COMPROBANTE').value.toUpperCase() == 'TICKET' ) {
        this.Form.get('TIPO_COMPROBANTE').setValue('BOLETA');
      }
    }
    this.changeQuantity();
  }

  changePromotion() {
    const promo = +this.Form.get('PROMO_MENSUAL').value;
    if ( promo == 0) {
      this.promotion_selected = null;
      this.discount = 0;
      this.free_consults = 0;
      this.Form.get('CANTIDAD').enable();
      this.changeQuantity();
    } else {
      this.promotion_selected = this.promotions.find(x => x.id == +promo);
      let disableQuantity = false;
      
      if ( this.promotion_selected.CODIGO.includes('10') && +this.promotion_selected.CONSULTA > 0) {
        if ( +this.Form.get('sessions').value < 10) {
          this.showToast(NbToastStatus.DANGER, 'No tiene las suficientes sesiones', '');
          this.Form.get('CANTIDAD').setValue('');
          this.Form.get('PROMO_MENSUAL').setValue(0);
          return;
        }
        
        this.Form.get('CANTIDAD').setValue(10 + Number(this.promotion_selected.CONSULTA));
        disableQuantity = true;
      }

      if ( this.promotion_selected.CODIGO.includes('20') && +this.promotion_selected.CONSULTA > 0 ) {
        if ( +this.Form.get('sessions').value < 20) {
          this.showToast(NbToastStatus.DANGER, 'No tiene las suficientes sesiones', '');
          this.Form.get('CANTIDAD').setValue('');
          this.Form.get('PROMO_MENSUAL').setValue(0);
          return;
        }
        
        this.Form.get('CANTIDAD').setValue(20 + Number(this.promotion_selected.CONSULTA));
        disableQuantity = true;
      }
      
      if ( this.promotion_selected.CODIGO.includes('3') && +this.promotion_selected.CONSULTA > 0 ) {
        if ( +this.Form.get('sessions').value < 3) {
          this.showToast(NbToastStatus.DANGER, 'No tiene las suficientes sesiones', '');
          this.Form.get('CANTIDAD').setValue('');
          this.Form.get('PROMO_MENSUAL').setValue(0);
          return;
        }
        
        this.Form.get('CANTIDAD').setValue(3);
        disableQuantity = true;
      }

      if ( this.promotion_selected.CONSULTA == null ) { // calcula en % de descuento
        this.discount = +this.promotion_selected.DSCTO;
        this.free_consults = 0;
      } else { // calcula en consultas gratis
        this.discount = 0;
        this.free_consults = +this.promotion_selected.CONSULTA;
      }

      if ( disableQuantity ) {
        this.Form.get('CANTIDAD').disable();
      } else {
        this.Form.get('CANTIDAD').enable();
      }
      this.changeQuantity();
    }
  }

  changeTreatment() {
    this.treatmentSelected = this.treatmentList.find( x => x.id == this.Form.get('treatment').value)
    this.serviceSelected = this.servicesList.find( x => x.id == this.treatmentSelected.SERVICIO);
    this.Form.get('sessions').setValue(Number(this.treatmentSelected.S_PROGRAMADAS) - Number(this.treatmentSelected.S_PAGADAS));
    this.Form.get('CANTIDAD').setValue('');
    this.promotions_parsed = [];
    for ( const promotion of JSON.parse(this.serviceSelected.promotions) ) {
      const promo = this.promotions.find(x => x.id == +promotion.promo);
      this.promotions_parsed.push(promo);
    }
    if (this.patient.TIPO_PACIENTE.toUpperCase() !== 'SEGURO') {
      this.promotions_parsed = this.promotions_parsed.filter( x => x.CODIGO.toUpperCase().indexOf('SEGURO') == -1 );
    } else {
      this.promotions_parsed = this.promotions_parsed.filter( x => x.CODIGO.toUpperCase().indexOf('SEGURO') != -1 );
    }
    this.changeQuantity();
  }

  async save() {

    if (this.Form.valid) {
      const today = new Date();
      this.data = {
        id: 0,
        FECHA: today.toISOString().split('T')[0] + ' ' + today.getHours()+':'+today.getMinutes()+':00',
        DNI: this.Form.get('DNI').value,
        NOMBRE_APELLIDO: this.Form.get('NOMBRE_APELLIDO').value,
        TIPO_PACIENTE: this.patient.TIPO_PACIENTE,
        TIPO_SERVICIO: this.serviceSelected.SERVICIO,
        CANTIDAD: this.Form.get('CANTIDAD').value,
        PRECIO_UNIDAD: this.Form.get('PRECIO_UNIDAD').value,
        SUBTOTAL: this.Form.get('SUBTOTAL').value,
        IGV: this.Form.get('IGV').value,
        TOTAL: this.Form.get('TOTAL').value,
        TOTAL_DESCUENTO: this.Form.get('TOTAL_DESCUENTO').value,
        DESCUENTO: this.Form.get('DESCUENTO').value,
        TOTAL_PAGADO: this.Form.get('TOTAL_PAGADO').value,
        PROMO_MENSUAL: this.Form.get('PROMO_MENSUAL').value,
        COD_PROMOCION: this.Form.get('COD_PROMOCION').value, // tabla 2 de promociones
        TIPO_CODIGO: this.treatmentSelected.id.toString(),
        FORMA_PAGO: this.Form.get('FORMA_PAGO').value,
        TIPO_COMPROBANTE: this.Form.get('TIPO_COMPROBANTE').value, // Boleta // factura // ticket
        RECEPTOR: this.user.NOMBRE + ' ' + this.user.APELLIDO,
        TURNO: this.user.TURNO,
        SEDE: this.user.SEDE
      };

      await this.service.save(this.data).toPromise().then(
        async res => {
          this.treatmentSelected.S_PAGADAS = this.treatmentSelected.S_PAGADAS + +this.Form.get('CANTIDAD').value;
          await this.treatmentService.update(this.treatmentSelected).toPromise().then().catch();
          if ( this.Form.get('TIPO_COMPROBANTE').value == 'TICKET') {
            this.sendTicket();
          }
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/cash/income/list']);
        }
      ).catch(
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );


    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/cash/income/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}