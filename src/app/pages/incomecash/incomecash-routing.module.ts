import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncomecashComponent } from './incomecash.component';
import { AddIncomecashComponent } from './addincomecash/addincomecash.component';
import { EditIncomecashComponent } from './editincomecash/editincomecash.component';
import { ListIncomecashComponent } from './listincomecash/listincomecash.component';

const routes: Routes = [{
  path: '',
  component: IncomecashComponent,
  children: [
    {
      path: 'list',
      component: ListIncomecashComponent,
    },
    {
      path: 'add',
      component: AddIncomecashComponent,
    },
    {
      path: 'edit',
      component: EditIncomecashComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncomecashRoutingModule {
}
