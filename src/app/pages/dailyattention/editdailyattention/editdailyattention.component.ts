import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DailyattentionService } from '../dailyattention.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Rate } from '../../../models/rate';
import { Statics } from '../../../models/statics';

@Component({
  selector: 'ngx-dailyattention-edit',
  styleUrls: ['./editdailyattention.component.scss'],
  templateUrl: './editdailyattention.component.html',
})
export class EditDailyattentionComponent {
  Form: FormGroup;
  data: Rate;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;

  constructor(
    private service: DailyattentionService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.data = JSON.parse(localStorage.getItem('edit'));
    this.Form = new FormGroup({
      code: new FormControl(this.data.CODIGO, Validators.required),
      price: new FormControl(this.data.PRECIO, Validators.required),
      service: new FormControl(this.data.SERVICIO, Validators.required),
      patient_type: new FormControl(this.data.TIPO_PACIENTE, Validators.required),
    });
  }

  save() {
    if (this.Form.valid) {
      this.data.CODIGO = this.Form.controls.code.value;
      this.data.PRECIO = this.Form.controls.price.value;
      this.data.SERVICIO = this.Form.controls.service.value;
      this.data.TIPO_PACIENTE = this.Form.controls.patient_type.value;
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }      
  }

  cancel() {
    this.router.navigate(['/pages/services/rates/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}