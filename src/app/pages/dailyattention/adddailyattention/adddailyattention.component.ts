import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DailyattentionService } from '../dailyattention.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { Dailyattention } from '../../../models/dailyattention';
import { Patient } from '../../../models/patient';
import { PatientsService } from '../../patients/patients.service';
import { TreatmentsService } from '../../treatments/treatments.service';
import { Treatment } from '../../../models/treatment';
import { RatesService } from '../../rates/rates.service';
import { Rate } from '../../../models/rate';
import { PersonalService } from '../../personal/personal.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'ngx-dailyattention-add',
  styleUrls: ['./adddailyattention.component.scss'],
  templateUrl: './adddailyattention.component.html',
  providers: [PersonalService, PatientsService, RatesService, TreatmentsService]
})
export class AddDailyattentionComponent {

  Form: FormGroup;
  data: Dailyattention;
  user;
  patient: Patient;
  patientList: Patient[] = [];
  loading = false;
  alltreatments: Treatment[] = [];
  treatmentList: Treatment[] = [];
  treatmentSelected: Treatment;
  serviceSelected: Rate;
  servicesList: Rate[] = [];
  treatmentList_parsed = [];

  statics = new Statics();

  payment_method = this.statics.PAYMENT_METHOD;
  comprobantes = this.statics.COMPROBANTES;

  showpromotions = true;

  promotions = [
    {id: 1, value: 'Promoción 3 x 2'},
    {id: 2, value: 'Promoción 10 + 1'},
    {id: 3, value: 'Promoción 20 + 2'},
    {id: 4, value: 'Campaña Evaluación T.F 50%'},
    {id: 5, value: 'Tarifa Promocional'}
  ];

  promotions_parsed = [
    {id: 1, value: 'Promoción 3 x 2'},
    {id: 2, value: 'Promoción 10 + 1'},
    {id: 3, value: 'Promoción 20 + 2'},
    {id: 4, value: 'Campaña Evaluación T.F 50%'},
    {id: 5, value: 'Tarifa Promocional'}
  ];
  
  constructor(
    private service: DailyattentionService,
    private toastrService: NbToastrService,
    private treatmentService: TreatmentsService,
    private location: Location,
    private patientService: PatientsService,
    private router: Router,
    private servicesService: RatesService,
    private route: ActivatedRoute
  ) {
    this.generateForm();
    this.service.getUser().subscribe(
      data => {
        this.user = JSON.parse(JSON.stringify(data)).success;
      },
      error => {}
    )
  }

  generateForm() {
    let dni = this.route.snapshot.paramMap.get('dni');
    if ( dni == null ) {
      dni = '';
    }
    this.Form = new FormGroup({
      DNI: new FormControl(dni, Validators.required),
      NOMBRE_APELLIDO: new FormControl({value: '', disabled: true}, Validators.required),
      S_PAGADAS: new FormControl({value: 0, disabled: true}, Validators.required),
      S_PROGRAMADAS: new FormControl({value: 0, disabled: true}, Validators.required),
      S_REALIZADAS: new FormControl({value: 0, disabled: true}, Validators.required),
      treatment: new FormControl({value: '', disabled: true}, Validators.required),
      session: new FormControl({value: '', disabled: true}, Validators.required),
    });
    if ( dni != null ) {
      this.verifyDNI();
    }
  }

  numberOnly(event) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  searchPromotion(){

  }

  onlynumbers(event) {
    if ( event.keyCode == 46 || event.keyCode == 8 ) {
      // let it happen, don't do anything
    } else {
      // Ensure that it is a number and stop the keypress
      if (event.keyCode < 48 || event.keyCode > 57 ) {
          event.preventDefault(); 
      }
    }
  }

  clearFields() {
    this.Form.get('NOMBRE_APELLIDO').setValue('');
    this.Form.get('S_PAGADAS').setValue('');
    this.Form.get('S_PROGRAMADAS').setValue('');
    this.Form.get('S_REALIZADAS').setValue('');
    this.Form.get('treatment').setValue('');
    this.Form.get('session').setValue('');
  }

  disableFields() {
    this.Form.get('NOMBRE_APELLIDO').disable();
    this.Form.get('S_PAGADAS').disable();
    this.Form.get('S_PROGRAMADAS').disable();
    this.Form.get('S_REALIZADAS').disable();
    this.Form.get('treatment').disable();
    this.Form.get('session').disable();
  }

  enableFields() {
    this.Form.get('NOMBRE_APELLIDO').enable();
    this.Form.get('S_PAGADAS').enable();
    this.Form.get('S_PROGRAMADAS').enable();
    this.Form.get('S_REALIZADAS').enable();
    this.Form.get('treatment').enable();
    this.Form.get('session').enable();
  }

  async verifyDNI() {
    if (this.Form.get('DNI').value.length == 8) {
      if (this.patientList.length == 0) {
        await this.patientService.get().toPromise().then(data => {this.patientList = JSON.parse(JSON.stringify(data));}).catch();
      }
      if (this.servicesList.length == 0) {
        await this.servicesService.get().toPromise().then(data => {this.servicesList = JSON.parse(JSON.stringify(data));}).catch();
      }
      this.patient = this.patientList.find( x => x.DNI == this.Form.get('DNI').value);
      if (this.patient == null) {
        this.showToast(NbToastStatus.DANGER, 'DNI invalido', '');
        this.clearFields();
        this.disableFields();
      } else {
        this.loading = true;
        if ( this.alltreatments.length == 0 ){
          await this.treatmentService.get().toPromise().then(
            (data) => {
              this.alltreatments = JSON.parse(JSON.stringify(data));
            }
          ).catch()
        }

        this.Form.get('NOMBRE_APELLIDO').setValue(this.patient.P_NOMBRE + ' ' + this.patient.P_APELLIDO);

        const alltreatments_aux = JSON.parse(JSON.stringify(this.alltreatments));
        this.treatmentList = [];
        this.treatmentList = alltreatments_aux.filter( x => x.DNI == this.Form.get('DNI').value && x.ESTADO == 'ACTIVO');
        this.treatmentList_parsed = [];
        for ( const xx of this.treatmentList ) {
          console.log(this.treatmentList.length);
          this.treatmentList_parsed.push({
            id: xx.id,
            SERVICIO: this.parseService(xx.SERVICIO),
            S_PROGRAMADAS: xx.S_PROGRAMADAS,
            S_PAGADAS: xx.S_PAGADAS,
            S_REALIZADAS: xx.S_REALIZADAS
          });
        }
        if (this.treatmentList_parsed.length == 0){
          this.showToast(NbToastStatus.DANGER, 'No tiene tratamientos o consultas activas', '');
          this.clearFields();
          this.disableFields();
        } else {
          this.Form.get('treatment').enable();
          this.Form.get('treatment').setValue(this.treatmentList_parsed[0].id);
          this.Form.get('session').setValue(this.treatmentList_parsed[0].S_REALIZADAS + ' / ' + this.treatmentList_parsed[0].S_PROGRAMADAS);
          this.Form.get('S_PROGRAMADAS').setValue(this.treatmentList_parsed[0].S_PROGRAMADAS);
          this.Form.get('S_PAGADAS').setValue(this.treatmentList_parsed[0].S_PAGADAS);
          this.Form.get('S_REALIZADAS').setValue(this.treatmentList_parsed[0].S_REALIZADAS);
          this.treatmentSelected = this.treatmentList[0];
          this.serviceSelected = this.servicesList.find( x => x.id == this.treatmentSelected.SERVICIO);
          if ( this.treatmentSelected.DOCTOR !== null) {
            this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
            this.promotions_parsed = this.promotions_parsed.filter( x => x.id == 4);
          } else {
            this.promotions_parsed = JSON.parse(JSON.stringify(this.promotions));
            this.promotions_parsed = this.promotions_parsed.filter( x => x.id != 4);
          }
        }

        if (this.patient.TIPO_PACIENTE !== 'SEGURO') {
          if (this.patient.TIPO_SEGURO !== 'FULLPRICE') {
            this.showpromotions = false;
          }
        }

        this.loading = false;
        // this.enableFields();
      }
    }
  }

  parseService(idService) {
    return this.servicesList.find( x => x.id == idService).SERVICIO;
  }

  changeTreatment() {
    this.treatmentSelected = this.treatmentList.find( x => x.id == this.Form.get('treatment').value)
    this.serviceSelected = this.servicesList.find( x => x.id == this.treatmentSelected.SERVICIO);    
    this.Form.get('session').setValue(this.treatmentSelected.S_REALIZADAS + ' / ' + this.treatmentSelected.S_PROGRAMADAS);
    this.Form.get('S_PROGRAMADAS').setValue(this.treatmentSelected.S_PROGRAMADAS);
    this.Form.get('S_PAGADAS').setValue(this.treatmentSelected.S_PAGADAS);
    this.Form.get('S_REALIZADAS').setValue(this.treatmentSelected.S_REALIZADAS);
  }

  async save() {

    if (this.Form.valid) {
      if (this.treatmentSelected.S_PROGRAMADAS >= (+this.treatmentSelected.S_REALIZADAS + 1) ) {
        if (this.treatmentSelected.S_PAGADAS >= (+this.treatmentSelected.S_REALIZADAS + 1) ) {
          
          const today = new Date();
          this.data = {
            id: 0,
            FECHA: today.toISOString().split('T')[0] + ' ' + today.getHours()+':'+today.getMinutes()+':00',
            TIPO_SEGURO: this.patient.TIPO_SEGURO,
            ASEGURADORA: this.patient.ASEGURADORA,
            NOMBRE_APELLIDO: this.Form.get('NOMBRE_APELLIDO').value,
            TERAPEUTA: this.treatmentSelected.DOCTOR == null ? this.treatmentSelected.TERAPEUTA : this.treatmentSelected.DOCTOR,
            TERAPIAS: (+this.treatmentSelected.S_REALIZADAS + 1) + '/' + this.treatmentSelected.S_PROGRAMADAS,
          };

          await this.service.save(this.data).toPromise().then(
            async res => {
              this.treatmentSelected.S_REALIZADAS = +this.treatmentSelected.S_REALIZADAS + 1;
              if ( this.treatmentSelected.S_REALIZADAS == this.treatmentSelected.S_PROGRAMADAS ) {
                this.treatmentSelected.ESTADO = 'INACTIVO';
              }
              await this.treatmentService.update(this.treatmentSelected).toPromise().then().catch();
              this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
              this.sendTicket();
              this.router.navigate(['/pages/attentions/list']);
            }
          ).catch(
            error => {
              this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
            }
          );

        } else {
          this.showToast(NbToastStatus.DANGER, 'No tiene nuevas sesiones pagadas', '');
        }
      } else {
        this.showToast(NbToastStatus.DANGER, 'Ya completo las sesiones programadas', '');
      }
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  getDateTicket() : string {
    const today = new Date();
    const day = today.getDate() < 10 ? '0' + today.getDate() : '' + today.getDate();
    const month = today.getMonth() + 1;
    const month2 = month < 10 ? '0' + month : '' + month;
    return day + '/' + month2 + '/' + today.getFullYear();
  }

  getHourTicket() : string {
    const today = new Date();
    const hour = today.getHours() > 12 ? today.getHours() - 12 : today.getHours();
    const minutes = today.getMinutes() < 10 ? '0' + today.getMinutes() : '' + today.getMinutes();
    const hour2 = hour < 10 ? '0' + hour : '' + hour;
    const ampm = today.getHours() >= 12 ? 'pm' : 'am';

    return hour2 + ':' + minutes + ' ' + ampm; 
  }

  sendTicket() {
    const patient = this.patientList.find(x => x.DNI == this.treatmentSelected.DNI);
    console.log(this.treatmentSelected);
    let cita = '';
    for ( const xx of this.treatmentSelected.schedule) {
      if ( Number(xx.session) == (Number(this.treatmentSelected.S_REALIZADAS) + 1) ) {
        cita = xx.horario.split(' ')[1];
      }
    }

    let hour = Number(cita.split(':')[0]);
    let minute = Number(cita.split(':')[1]);
    let ampm = 'am';
    if ( hour > 12 ) {
      hour = hour - 12;
      ampm = 'pm';
    }



    const data = {
      title: 'TICKET DE ATENCION',
      patient: this.Form.get('NOMBRE_APELLIDO').value,
      sede: patient.SEDE,
      personal: this.treatmentSelected.DOCTOR == null ? this.treatmentSelected.TERAPEUTA : this.treatmentSelected.DOCTOR,
      cita: (hour < 10 ? ('0' + hour) : ('' + hour) ) + ':' + ( minute < 10 ? ( '0' + minute ) : ('' + minute) ) + ' ' + ampm,
      session: '' + (Number(this.treatmentSelected.S_REALIZADAS) + 1),
      date: this.getDateTicket(),
      hour: this.getHourTicket(),
      promotion: false
    }
    console.log(data);
    /*this.ipcService.ping(data).then(
      data => {
        console.log(data);
      }
    );*/
  }

  cancel() {
    this.router.navigate(['/pages/attentions/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}