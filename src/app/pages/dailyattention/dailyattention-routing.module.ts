import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DailyattentionComponent } from './dailyattention.component';
import { AddDailyattentionComponent } from './adddailyattention/adddailyattention.component';
import { EditDailyattentionComponent } from './editdailyattention/editdailyattention.component';
import { ListDailyattentionComponent } from './listdailyattention/listdailyattention.component';

const routes: Routes = [{
  path: '',
  component: DailyattentionComponent,
  children: [
    {
      path: 'list',
      component: ListDailyattentionComponent,
    },
    {
      path: 'add',
      component: AddDailyattentionComponent,
    },
    {
      path: 'add/:dni',
      component: AddDailyattentionComponent,
    },
    {
      path: 'edit',
      component: EditDailyattentionComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DailyattentionRoutingModule {
}
