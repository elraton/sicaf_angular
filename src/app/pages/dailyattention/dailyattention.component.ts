import { Component } from '@angular/core';

@Component({
  selector: 'ngx-dailyattention',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class DailyattentionComponent {
}
