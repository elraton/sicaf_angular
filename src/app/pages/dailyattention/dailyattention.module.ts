import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { DailyattentionRoutingModule } from './dailyattention-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

// components
import { DailyattentionComponent } from './dailyattention.component';
import { AddDailyattentionComponent } from './adddailyattention/adddailyattention.component';
import { EditDailyattentionComponent } from './editdailyattention/editdailyattention.component';
import { ListDailyattentionComponent } from './listdailyattention/listdailyattention.component';

// service
import { DailyattentionService } from './dailyattention.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbStepperModule } from '@nebular/theme';

const COMPONENTS = [
  DailyattentionComponent,
  AddDailyattentionComponent,
  EditDailyattentionComponent,
  ListDailyattentionComponent
];

const SERVICES = [
  DailyattentionService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  DailyattentionRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
  NbStepperModule,
  NgxMaterialTimepickerModule,
  AutocompleteModule.forRoot(),
  CalendarModule.forRoot({
    provide: DateAdapter,
    useFactory: adapterFactory
  })
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DailyattentionModule { }
