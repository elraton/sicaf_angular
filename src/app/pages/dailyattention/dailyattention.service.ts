import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class DailyattentionService {
    
    constructor(private http: HttpClient) {}

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'atenciondiaria/list ');
    }

    getUser() {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'getUser');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'atenciondiaria/add ', data);
    }

    getSchedule(data) {
        const url = localStorage.getItem('baseurl');
        return this.http.post( url + 'horarios/range', data);
    }
}