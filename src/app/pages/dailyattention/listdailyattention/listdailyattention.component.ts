import { Component, OnInit } from '@angular/core';
import { DailyattentionService } from '../dailyattention.service';
import { Personal_Category } from '../../../models/personal_category';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-dailyattention-list',
  styleUrls: ['./listdailyattention.component.scss'],
  templateUrl: './listdailyattention.component.html',
})
export class ListDailyattentionComponent implements OnInit{

  data: Personal_Category[];
  
  loading = true;
  constructor(
    private service: DailyattentionService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      FECHA: {
        title: 'Fecha',
        type: 'string',
      },
      NOMBRE_APELLIDO: {
        title: 'Paciente',
        type: 'string',
      },
      ASEGURADORA: {
        title: 'Aseguradora',
        type: 'string',
      },
      TERAPEUTA: {
        title: 'Terapeuta o Doctor',
        type: 'string'
      },
      TERAPIAS: {
        title: 'Sesion',
        type: 'string'
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.service.get().subscribe(
      response => {
        this.data = JSON.parse(JSON.stringify(response));
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  onAdd(event): void {
    this.router.navigate(['/pages/attentions/add']);
  }

  onEdit(event): void {
    const data = event.data;
    //localStorage.setItem('edit', JSON.stringify(data));
    //this.router.navigate(['/pages/services/rates/edit']);
    if (window.confirm('No se pueden Editar tickets')) {
      return;
    } else {
      return;
    }
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if (window.confirm('No se pueden eliminar tickets')) {
      return;
    } else {
      return;
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}