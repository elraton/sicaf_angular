import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { ProfileRoutingModule } from './profile-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { ProfileComponent } from './profile.component';
import { EditProfileComponent } from './editprofile/editprofile.component';

// service
import { ProfileService } from './profile.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ModuleWithProviders } from '@angular/compiler/src/core';

const COMPONENTS = [
  ProfileComponent,
  EditProfileComponent,
];

const SERVICES = [
  ProfileService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  ProfileRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class ProfileModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ProfileModule,
      providers: [...SERVICES],
    };
  }
}
