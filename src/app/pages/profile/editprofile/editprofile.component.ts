import { Component, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfileService } from '../profile.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService, NbUserModule } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router'; 
import { HeaderComponent } from '../../../@theme/components';
import { UserService } from '../../../@core/mock/users.service';
import { AvatarService } from '../../../@theme/components/header/avatarService';

@Component({
  selector: 'ngx-edit-profile',
  styleUrls: ['./editprofile.component.scss'],
  templateUrl: './editprofile.component.html',
  providers: [AvatarService]
})
export class EditProfileComponent {
  Form: FormGroup;
  showForm = false;
  user;

  baseImageurl = '/assets/images/avatar/';

  avatar_list = [
    {
      image: 'avatar1.jpg',
      active: false,
    },
    {
      image: 'avatar2.jpg',
      active: false,
    },
    {
      image: 'avatar3.jpg',
      active: false,
    },
    {
      image: 'avatar4.jpg',
      active: false,
    },
    {
      image: 'avatar5.jpg',
      active: false,
    },
    {
      image: 'avatar6.jpg',
      active: false,
    },
    {
      image: 'avatar7.jpg',
      active: false,
    },
    {
      image: 'avatar8.jpg',
      active: false,
    },
    {
      image: 'avatar9.jpg',
      active: false,
    },
    {
      image: 'avatar10.jpg',
      active: false,
    },
    {
      image: 'avatar11.jpg',
      active: false,
    },
    {
      image: 'avatar12.jpg',
      active: false,
    },
    {
      image: 'avatar13.jpg',
      active: false,
    },
    {
      image: 'avatar14.jpg',
      active: false,
    },
    {
      image: 'avatar15.jpg',
      active: false,
    },
    {
      image: 'avatar16.jpg',
      active: false,
    },
    {
      image: 'female_user.jpg',
      active: false,
    },
  ];

  constructor(
    private service: ProfileService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router,
    private avatarService: AvatarService
  ) {
    this.generateForm();
    this.service.getUser().subscribe(
      data => {
        this.user = JSON.parse(JSON.stringify(data));
        this.avatar_list.map(
          item => {
            if ( item.image == this.user.success.photo ) {
              item.active = true;
            }
          }
        )
      },
      error => {}
    );
  }

  generateForm() {
    this.Form = new FormGroup({
      newpassword: new FormControl('', Validators.required),
      renewpassword: new FormControl('', Validators.required),
    });
  }

  toggleShowForm() {
    this.showForm = !this.showForm;
  }

  setActive(avatar) {
    this.avatar_list.map( x => x.active = false);
    avatar.active = true;
  }

  save() {

    if ( this.showForm ) {
      if ( this.Form.valid ) {
        if ( this.Form.get('newpassword').value === this.Form.get('renewpassword') ) {
          const photoSelected = this.avatar_list.find( x => x.active ).image;
          const data = {
            id: this.user.success.id,
            password: this.Form.get('newpassword'),
            photo: photoSelected
          };
          this.service.update(data).subscribe(
            data => {
              this.service.changePhoto(photoSelected);
              this.showToast(NbToastStatus.SUCCESS, 'Se guardaron los datos', '');
              // this.router.navigate(['/schedule']);
            },
            error => {
              this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
            }
          );
        } else {
          this.showToast(NbToastStatus.DANGER, 'Los password no coinciden', '');
        }
      } else {
        this.showToast(NbToastStatus.DANGER, 'Debes completar los 2 campos', '');
      }
    } else {
      const photoSelected = this.avatar_list.find( x => x.active ).image;
      const data = {
        id: this.user.success.id,
        photo: photoSelected
      };
      this.service.update(data).subscribe(
        data => {
          this.service.changePhoto(photoSelected);
          this.showToast(NbToastStatus.SUCCESS, 'Se guardaron los datos', '');
          // this.router.navigate(['/schedule']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    }
  }

  cleanFormControl(control) {
    control.value = '';
  }

  cancel() {
    
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}