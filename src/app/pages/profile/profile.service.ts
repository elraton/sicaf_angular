import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of as observableOf,  Observable,  BehaviorSubject } from 'rxjs';

@Injectable()
export class ProfileService {
    currentPhoto = '';
    private photoSource = new BehaviorSubject(this.currentPhoto);
    
    constructor(private http: HttpClient) {}

    changePhoto(photo: string) {
        console.log('se envio el mensaje');
        this.photoSource.next(photo)
    }

    getPhoto(): Observable<string> {
        return observableOf(this.currentPhoto);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'users/edit ', data);
    }

    getUser() {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'getUser');
    }
}