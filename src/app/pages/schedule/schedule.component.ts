import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ScheduleService } from './schedule.service';
import { Personal_Category } from '../../models/personal_category';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { CalendarView, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { Subject } from 'rxjs';
import { PersonalService } from '../personal/personal.service';
import { PatientsService } from '../patients/patients.service';
import { RatesService } from '../rates/rates.service';
import { Statics } from '../../models/statics';
import { Personal } from '../../models/personal';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
import { Calendar_Day } from '../../models/calendar_day';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Treatment } from '../../models/treatment';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'ngx-schedule',
  styleUrls: ['./schedule.component.scss'],
  templateUrl: './schedule.component.html',
  providers: [PersonalService, PatientsService, RatesService]
})
export class ScheduleComponent implements OnInit{

  user;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;

  sede_list = new Statics().SEDES;

  selectedItem: any = '';
  inputChanged: any = '';

  week = [];
  period = 4;
  time = 15;

  startHour = 7;
  startMinute = 0;
  endHour = 13;
  endMinute = 0;
  lastStartHour;
  lastEndHour;

  config: any = {'placeholder': 'nombre terapeuta', 'sourceField': ['value']};

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        // this.handleEvent('Edited', event);
      }
    },
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  initialTime;

  terapist = 0;
  interval = 'lu-vi';
  excludedDays = [0];
  today = new Date();
  firstDay = new Date( new Date(new Date(this.today.setMonth(this.today.getMonth() - 2)).setDate(1)).setHours(0,0,0) );
  lastDay = new Date( new Date(new Date(this.today.setMonth(this.today.getMonth() + 5)).setDate(1)).setHours(0,0,0) );

  scheduleTreatments = [];
  scheduleConsults = [];
  treatments_list = [];
  doctorLegend = '';

  data: Personal_Category[];

  personalList: Personal[] = [];
  personalParsed = [];
  personalSelected: Personal;

  shift = 'm';
  sede = '';

  loading = true;

  constructor(
    private service: ScheduleService,
    private toastrService: NbToastrService,
    private personalService: PersonalService,
    private patientService: PatientsService,
    private serviceService: RatesService,
    private router: Router,
    private modal: NgbModal
  ) {}

  ngOnInit() {
    this.getServiceData();
    
  }

  async getServiceData() {
    await this.service.getConsults({from: this.firstDay, to: this.lastDay}).toPromise().then(
      data => {
        this.scheduleConsults = JSON.parse(JSON.stringify(data));
      }
    ).catch(
      error => {
        console.log(error)
      }
    );
    await this.service.get({from: this.firstDay, to: this.lastDay}).toPromise().then(
      data => {
        this.scheduleTreatments = JSON.parse(JSON.stringify(data));
      }
    ).catch(
      error => {
        console.log(error);
      }
    );

    await this.service.getUser().toPromise().then(
      data => {
        this.user = JSON.parse(JSON.stringify(data)).success;
        this.sede = this.user.SEDE;
      }
    ).catch(
      error => {}
    );

    await this.personalService.get().toPromise().then(
      data => {
        this.personalList = JSON.parse(JSON.stringify(data));
      }
    ).catch(
      error => {
        console.log(error);
      }
    );

    this.sedeChange();
    if ( this.personalParsed.length > 0 ) {
      this.personalSelected = this.personalList.find( x => x.id == this.personalParsed[0].id );
      this.terapist = this.personalSelected.id;
      this.generateSchedule();
    }
    this.loading = false;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  getpatientName(str: string) {
    return str.split('<br>')[0];
  }

  getpatientSession(str: string) {
    return str.split('<br>')[1];
  }

  getpatientHour(date) {
    const tm = new Date(date);
    let hour = tm.getHours() > 12 ? tm.getHours() - 12 : tm.getHours();
    let minutes = tm.getMinutes();

    if ( this.personalSelected.ES_NIÑOS ) {
      if ( minutes >= 30) {
        minutes = 30;
      } else {
        minutes = 0;
      }
    } else {
      minutes = 0;
    }

    return (hour < 10 ? '0' + hour : hour ) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ' ' + (tm.getHours() > 12 ? 'pm' : 'am' )
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  terapistChange() {
    this.personalSelected = this.personalList.find(x => x.id == +this.terapist);
    this.events = [];
    this.generateSchedule();
  }

  sedeChange() {
    let personal_aux = JSON.parse(JSON.stringify(this.personalList));
    this.personalParsed = [];
    personal_aux = personal_aux.filter(x => x.SEDE == this.sede );
    if ( this.shift == 'm' ) {
      personal_aux = personal_aux.filter(x => x.TURNO == 'MAÑANA' );
    } else {
      personal_aux = personal_aux.filter(x => x.TURNO == 'TARDE' );
    }
    for ( const xx of personal_aux ) {
      this.personalParsed.push(
        {
          id: xx.id,
          value: xx.NOMBRE + ' ' + xx.APELLIDO
        }
      );
    }
  }

  shiftChange() {
    if ( this.shift == 'm' ) {
      this.startHour = 7;
      this.startMinute = 0;
      this.endHour = 13;
      this.endMinute = 0;
    } else {
      this.startHour = 13;
      this.startMinute = 30;
      this.endHour = 19;
      this.endMinute = 30;
    }
    let personal_aux = JSON.parse(JSON.stringify(this.personalList));
    this.personalParsed = [];
    personal_aux = personal_aux.filter(x => x.SEDE == this.sede );
    if ( this.shift == 'm' ) {
      personal_aux = personal_aux.filter(x => x.TURNO == 'MAÑANA' );
    } else {
      personal_aux = personal_aux.filter(x => x.TURNO == 'TARDE' );
    }
    for ( const xx of personal_aux ) {
      this.personalParsed.push(
        {
          id: xx.id,
          value: xx.NOMBRE + ' ' + xx.APELLIDO
        }
      );
    }
    this.refresh.next();
  }

  parse_once(obj) {

    if ( this.shift == 'm' ) {
      const hour = new Date( JSON.parse(JSON.stringify(obj)).date ).getHours();
      const minute = new Date( JSON.parse(JSON.stringify(obj)).date ).getMinutes();
      if ( minute == 0 ) {
        return hour + ':0' + minute + (hour >= 12 ? ' pm' : ' am');
      } else {
        return '';
      }
    } else {
      const hour = new Date( JSON.parse(JSON.stringify(obj)).date ).getHours();
      const minute = new Date( JSON.parse(JSON.stringify(obj)).date ).getMinutes();
      if ( minute == 30 ) {
        return (hour - 12) + ':' + minute + (hour >= 12 ? ' pm' : ' am');
      } else {
        return '';
      }
    }

  }

  async generateSchedule() {
    console.log('generate');
    this.loading = true;
    this.events = [];

    if (+this.personalSelected.ES_DOCTOR > 0) {
      let schedule_aux = JSON.parse(JSON.stringify(this.scheduleConsults));
      schedule_aux = schedule_aux.filter(x => x.DOCTOR_ID == this.personalSelected.id)

      for ( const xx of schedule_aux ) {
        this.events.push({
          id: xx.id,
          start: new Date(xx.FECHA + ' ' + xx.HORARIO),
          end: new Date(new Date(xx.FECHA + ' ' + xx.HORARIO).setMinutes(new Date(xx.FECHA + ' ' + xx.HORARIO).getMinutes() + this.time)),
          title: xx.P_NOMBRE + ' ' + xx.P_APELLIDO,
          color: colors.blue,
          actions: this.actions,
          allDay: false,
          draggable: false,
          cssClass: 'active'
        });
      }
      this.refresh.next();
      
    } else {
      let schedule_aux = JSON.parse(JSON.stringify(this.scheduleTreatments));
      schedule_aux = schedule_aux.filter(x => x.personal_id == this.personalSelected.id)

      console.log(schedule_aux);

      for ( const xx of schedule_aux ) {
        let className = 'active';
        if ( xx.status == 1 ) {
          className = 'active';
        }
        if ( xx.status == 2 ) {
          className = 'reprogrammed';
        }
        if ( xx.status == 3 ) {
          className = 'canceled';
        }
        this.events.push({
          id: xx.id,
          start: new Date(xx.horario),
          end: new Date(new Date(xx.horario).setMinutes(new Date(xx.horario).getMinutes() + this.time)),
          title: xx.treatment[0].P_NOMBRE + ' ' + xx.treatment[0].P_APELLIDO + '<br>' + xx.session + '/' + xx.treatment[0].S_PROGRAMADAS,
          color: colors.blue,
          actions: this.actions,
          allDay: false,
          draggable: false,
          cssClass: className
        });
      }
      this.refresh.next();
    }
    this.loading = false;
  }

  handleEvent(action: string, event: CalendarEvent, option): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    // this.handleEvent('Dropped or resized', event);
  }

  emptyHourClicked(event, option) {
  }

  printTicket(data) {
    const schedule = this.scheduleTreatments.find( x => x.id == data.id);
    this.router.navigate(['/pages/attentions/add/'+schedule.treatment[0].DNI]);
    this.closeModal();
  }

  closeModal() {
    this.modal.dismissAll();
  }

  reprogramSchedule(data) {
    const schedule = this.scheduleTreatments.find( x => x.id == data.id);
    localStorage.setItem('edit', JSON.stringify(schedule.treatment[0]));
    this.closeModal();
    this.router.navigate(['/pages/treatments/edit/' + schedule.id]);    
  }

  async cancelSchedule(data) {

    let schedule1 = this.scheduleTreatments.find( x => x.id == data.id);
    schedule1.status = 3;

    await this.service.update(schedule1).toPromise().then(
      async data => {
        const event = this.events.find( x => x.id == schedule1.id);
        event.cssClass = 'canceled';

            let treatment: Treatment;
            await this.service.getTreatment({ id: schedule1.treatment[0].id }).toPromise().then(
              data => {
                treatment = JSON.parse(JSON.stringify(data));
              }
            ).catch();

            for (const xx of treatment.schedule) {
              if ( Number(xx.session) > Number(schedule1.session) ) {
                xx.session = Number(xx.session) - 1;
                this.events.find( x => x.id == xx.id).title = 
                  treatment.P_NOMBRE + ' ' + treatment.P_APELLIDO + '<br>' + xx.session + '/' + treatment.S_PROGRAMADAS;
                await this.service.update(xx).toPromise().then().catch();
              }
              this.refresh.next();
            }

            const schedule = treatment.schedule.pop();

            let date_sch = schedule.horario.split(' ')[0]
            let hour = schedule.horario.split(' ')[1]

            let days2sum = 0;
            if ( treatment.INTERVALO == 'lu-vi' || treatment.INTERVALO == 'lu-sa') {
              days2sum = 1;
            }
            if ( treatment.INTERVALO == 'lu-mi-vi' || treatment.INTERVALO == 'ma-ju-sa') {
              days2sum = 2;
            }
            let date = new Date(date_sch+'T10:00:00');
            date = new Date( new Date(date).setDate(date.getDate() + days2sum) );

            if ( treatment.INTERVALO == 'lu-vi') {
              if ( date.getDay() == 6 ) {
                date = new Date( new Date(date).setDate(date.getDate() + 2) );
              }
            }

            if ( treatment.INTERVALO == 'lu-sa') {
              if ( date.getDay() == 0 ) {
                date = new Date( new Date(date).setDate(date.getDate() + 1) );
              }
            }
            if ( treatment.INTERVALO == 'lu-mi-vi') {
              if ( date.getDay() == 0 ) {
                date = new Date( new Date(date).setDate(date.getDate() + 1) );
              }
            }
            if ( treatment.INTERVALO == 'ma-ju-sa') {
              if ( date.getDay() == 1 ) {
                date = new Date( new Date(date).setDate(date.getDate() + 1) );
              }
            }
            date = new Date(date.setHours(hour.split(':')[0], hour.split(':')[1], 0));
            const datt = new Date(date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0]);
            const horario = {
              horario: date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0],
              session: Number(schedule.session) + 1,
              status: 1,
              personal_id: schedule.personal_id,
              treatment: treatment.id
            };
            await this.service.saveOne(horario).toPromise().then(
              data => {
                this.events.push({
                  id: 0,
                  start: new Date(datt),
                  end: new Date(datt.setMinutes(datt.getMinutes() + this.time)),
                  title: treatment.P_NOMBRE + ' ' + treatment.P_APELLIDO + '<br>' + (Number(schedule.session) + 1) + '/' + treatment.S_PROGRAMADAS,
                  color: colors.blue,
                  actions: this.actions,
                  allDay: false,
                  draggable: false,
                  cssClass: 'active'
                });
                this.refresh.next();
              }
            ).catch();
        this.closeModal();
      }
    ).catch();

  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
