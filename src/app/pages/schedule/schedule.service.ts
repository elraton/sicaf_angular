import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class ScheduleService {
    
    constructor(private http: HttpClient) {}

    get(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'horarios/range', data);
    }

    saveOne(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'horarios/saveOne', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'horarios/edit ', data);
    }

    getConsults(data) {
        const url = localStorage.getItem('baseurl');
        return this.http.post( url + 'servpaciente/range', data);
    }

    getUser() {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'getUser');
    }

    getTreatment(data) {
        const url = localStorage.getItem('baseurl');
        return this.http.post( url + 'servpaciente/get', data);
    }

}