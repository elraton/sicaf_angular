import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PromosService } from '../promos.service';
import { Promos } from '../../../models/promos';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';

@Component({
  selector: 'ngx-promos-add',
  styleUrls: ['./addpromos.component.scss'],
  templateUrl: './addpromos.component.html',
})
export class AddPromosComponent {
  Form: FormGroup;
  data: Promos;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;
  status = new Statics().STATUS;

  constructor(
    private service: PromosService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.Form = new FormGroup({
      CODIGO: new FormControl('', Validators.required),
      ESTADO: new FormControl('ACTIVO', Validators.required),
      CONSULTA: new FormControl(''),
      DSCTO: new FormControl(''),
      IGV: new FormControl(1, Validators.required),
    });
  }

  btnIGV(value: number) {
    this.Form.get('IGV').setValue(value);
  }

  save() {
    if (this.Form.valid) {
      this.data = {
        id: 0,
        CODIGO: this.Form.get('CODIGO').value,
        CONSULTA: this.Form.get('CONSULTA').value,
        DSCTO: this.Form.get('DSCTO').value,
        ESTADO: this.Form.get('ESTADO').value,
        IGV: this.Form.get('IGV').value,
        TIPO: "-"
      };

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/services/promos/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/services/promos/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}