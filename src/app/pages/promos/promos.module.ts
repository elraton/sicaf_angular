import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { PromosRoutingModule } from './promos-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { PromosComponent } from './promos.component';
import { AddPromosComponent } from './addpromos/addpromos.component';
import { EditPromosComponent } from './editpromos/editpromos.component';
import { ListPromosComponent } from './listpromos/listpromos.component';

// service
import { PromosService } from './promos.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  PromosComponent,
  AddPromosComponent,
  EditPromosComponent,
  ListPromosComponent
];

const SERVICES = [
  PromosService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  PromosRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class PromosModule { }
