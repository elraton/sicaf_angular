import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PromosService } from '../promos.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Promos } from '../../../models/promos';
import { Statics } from '../../../models/statics';

@Component({
  selector: 'ngx-promos-edit',
  styleUrls: ['./editpromos.component.scss'],
  templateUrl: './editpromos.component.html',
})
export class EditPromosComponent {
  Form: FormGroup;
  data: Promos = JSON.parse(localStorage.getItem('edit'));

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;
  status = new Statics().STATUS;

  constructor(
    private service: PromosService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.Form = new FormGroup({
      CODIGO: new FormControl(this.data.CODIGO, Validators.required),
      ESTADO: new FormControl(this.data.ESTADO, Validators.required),
      CONSULTA: new FormControl(this.data.CONSULTA),
      DSCTO: new FormControl(this.data.DSCTO),
      IGV: new FormControl(this.data.IGV, Validators.required),
    });
  }

  btnIGV(value: number) {
    this.Form.get('IGV').setValue(value);
  }

  save() {
    if (this.Form.valid) {
      this.data.CODIGO = this.Form.get('CODIGO').value;
      this.data.CONSULTA = this.Form.get('CONSULTA').value;
      this.data.DSCTO = this.Form.get('DSCTO').value;
      this.data.ESTADO = this.Form.get('ESTADO').value;
      this.data.IGV = this.Form.get('IGV').value;

      this.service.update(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/services/promos/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/services/promos/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}