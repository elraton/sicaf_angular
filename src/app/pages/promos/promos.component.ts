import { Component } from '@angular/core';

@Component({
  selector: 'ngx-promos',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class PromosComponent {
}
