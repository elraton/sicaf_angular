import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromosComponent } from './promos.component';
import { AddPromosComponent } from './addpromos/addpromos.component';
import { EditPromosComponent } from './editpromos/editpromos.component';
import { ListPromosComponent } from './listpromos/listpromos.component';

const routes: Routes = [{
  path: '',
  component: PromosComponent,
  children: [
    {
      path: 'list',
      component: ListPromosComponent,
    },
    {
      path: 'add',
      component: AddPromosComponent,
    },
    {
      path: 'edit',
      component: EditPromosComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromosRoutingModule {
}
