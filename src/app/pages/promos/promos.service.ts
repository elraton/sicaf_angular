import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class PromosService {
    
    constructor(private http: HttpClient) {}

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'promociones/list ');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'promociones/add ', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'promociones/edit ', data);
    }

    remove(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'promociones/remove', data);
    }
}