import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { RatesService } from '../rates.service';
import { Rate } from '../../../models/rate';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { Promos } from '../../../models/promos';
import { PromosService } from '../../promos/promos.service';

@Component({
  selector: 'ngx-rates-add',
  styleUrls: ['./addrates.component.scss'],
  templateUrl: './addrates.component.html',
  providers: [PromosService]
})
export class AddRatesComponent {
  Form: FormGroup;
  data: Rate;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;
  promotionsList : Promos[] = [];
  promotionsSelected = [];

  constructor(
    private service: RatesService,
    private promoService: PromosService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
    this.promoService.get().subscribe(
      data => {
        this.promotionsList = data;
      },
    );
  }

  generateForm() {
    this.Form = new FormGroup({
      code: new FormControl('', Validators.required),
      service: new FormControl('', Validators.required),
      patient_type: new FormControl('', Validators.required),
      fullprice: new FormControl('', Validators.required),
      especial: new FormControl('', Validators.required),
      promocional: new FormControl('', Validators.required),
      familiar: new FormControl('', Validators.required),
    });
  }

  save() {

    this.promotionsSelected = this.promotionsSelected.filter( (x) => {
      if ( x.promo !== -1) {
        return x;
      }
    });

    if (this.Form.valid) {
      this.data = {
        id: 0,
        CODIGO: this.Form.get('code').value,
        PRECIO: "0",
        SERVICIO: this.Form.get('service').value,
        TIPO_PACIENTE: this.Form.get('patient_type').value,
        especial: this.Form.get('especial').value,
        familiar: this.Form.get('familiar').value,
        fullprice: this.Form.get('fullprice').value,
        promocional: this.Form.get('promocional').value,
        promotions: JSON.stringify(this.promotionsSelected)
      };

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/services/rates/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }      
  }

  addPromo() {
    this.promotionsSelected.push({
      promo: -1
    });
  }

  changePromo(event, promo) {
    promo.promo = event.srcElement.value;
  }

  deletePromo(promo) {
    this.promotionsSelected = this.promotionsSelected.filter(x => x !== promo);
  }

  cancel() {
    this.router.navigate(['/pages/services/rates/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}