import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { RatesRoutingModule } from './rates-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { RatesComponent } from './rates.component';
import { AddRatesComponent } from './addrates/addrates.component';
import { EditRatesComponent } from './editrates/editrates.component';
import { ListRatesComponent } from './listrates/listrates.component';

// service
import { RatesService } from './rates.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  RatesComponent,
  AddRatesComponent,
  EditRatesComponent,
  ListRatesComponent
];

const SERVICES = [
  RatesService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  RatesRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class RatesModule { }
