import { Component } from '@angular/core';

@Component({
  selector: 'ngx-rates',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class RatesComponent {
}
