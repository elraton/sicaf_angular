import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { RatesService } from '../rates.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Rate } from '../../../models/rate';
import { Statics } from '../../../models/statics';
import { Promos } from '../../../models/promos';
import { PromosService } from '../../promos/promos.service';

@Component({
  selector: 'ngx-rates-edit',
  styleUrls: ['./editrates.component.scss'],
  templateUrl: './editrates.component.html',
  providers: [PromosService]
})
export class EditRatesComponent {
  Form: FormGroup;
  data: Rate;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;
  promotionsList : Promos[] = [];
  promotionsSelected = [];

  constructor(
    private service: RatesService,
    private promoService: PromosService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
    this.promoService.get().subscribe(
      data => {
        this.promotionsList = data;
      },
    );
  }

  generateForm() {
    this.data = JSON.parse(localStorage.getItem('edit'));
    this.data.TIPO_PACIENTE = this.data.TIPO_PACIENTE.toUpperCase();
    this.promotionsSelected = JSON.parse(this.data.promotions);
    console.log(this.promotionsSelected);
    this.Form = new FormGroup({
      code: new FormControl(this.data.CODIGO, Validators.required),
      service: new FormControl(this.data.SERVICIO, Validators.required),
      patient_type: new FormControl(this.data.TIPO_PACIENTE, Validators.required),
      fullprice: new FormControl(this.data.fullprice, Validators.required),
      especial: new FormControl(this.data.especial, Validators.required),
      promocional: new FormControl(this.data.promocional, Validators.required),
      familiar: new FormControl(this.data.familiar, Validators.required),
    });
  }

  save() {

    this.promotionsSelected = this.promotionsSelected.filter( (x) => {
      if ( x.promo !== -1) {
        return x;
      }
    });

    if (this.Form.valid) {
      this.data.CODIGO = this.Form.controls.code.value;
      this.data.SERVICIO = this.Form.controls.service.value;
      this.data.TIPO_PACIENTE = this.Form.controls.patient_type.value;
      this.data.fullprice = this.Form.get('fullprice').value;
      this.data.especial = this.Form.get('especial').value;
      this.data.promocional = this.Form.get('promocional').value;
      this.data.familiar = this.Form.get('familiar').value;
      this.data.promotions = JSON.stringify(this.promotionsSelected);

      this.service.update(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/services/rates/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }      
  }

  addPromo() {
    this.promotionsSelected.push({
      promo: -1
    });
  }

  changePromo(event, promo) {
    promo.promo = event.srcElement.value;
  }

  deletePromo(promo) {
    this.promotionsSelected = this.promotionsSelected.filter(x => x !== promo);
  }

  cancel() {
    this.router.navigate(['/pages/services/rates/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}