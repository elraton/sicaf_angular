import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RatesComponent } from './rates.component';
import { AddRatesComponent } from './addrates/addrates.component';
import { EditRatesComponent } from './editrates/editrates.component';
import { ListRatesComponent } from './listrates/listrates.component';

const routes: Routes = [{
  path: '',
  component: RatesComponent,
  children: [
    {
      path: 'list',
      component: ListRatesComponent,
    },
    {
      path: 'add',
      component: AddRatesComponent,
    },
    {
      path: 'edit',
      component: EditRatesComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RatesRoutingModule {
}
