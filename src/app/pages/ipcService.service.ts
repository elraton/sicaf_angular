import { Injectable } from "@angular/core";
import { IpcRenderer } from "electron";

@Injectable({
  providedIn: "root"
})
export class IpcService {
  private ipc: IpcRenderer;

  constructor() {
    if (window.require) {
      try {
        this.ipc = window.require("electron").ipcRenderer;
      } catch (error) {
        throw error;
      }
    } else {
      console.warn("Could not load electron ipc");
    }
  }

  async ping(content) {
    if (!this.ipc) {
      return;
    }
    return new Promise<string[]>((resolve, reject) => {
      this.ipc.once("pong", (event, arg) => {        
        resolve(arg);
      });
      this.ipc.send("ping", content);
    });
  }
}