import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../users.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { User } from '../../../models/user';

@Component({
  selector: 'ngx-users-add',
  styleUrls: ['./addusers.component.scss'],
  templateUrl: './addusers.component.html',
})
export class AddUserComponent {
  Form: FormGroup;
  data: User;

  static = new Statics();

  constructor(
    private service: UsersService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.Form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      NOMBRE: new FormControl('', Validators.required),
      APELLIDO: new FormControl('', Validators.required),
      AREA: new FormControl('', Validators.required),
      SEDE: new FormControl('', Validators.required),
      TURNO: new FormControl('', Validators.required),
    });
  }

  save() {
    if (this.Form.valid) {
      this.data = {
        id: 0,
        APELLIDO: this.Form.get('APELLIDO').value,
        AREA: this.Form.get('AREA').value,
        NOMBRE: this.Form.get('NOMBRE').value,
        SEDE: this.Form.get('SEDE').value,
        TURNO: this.Form.get('TURNO').value,
        email: this.Form.get('email').value,
        password: this.Form.get('password').value,
        photo: 'female_user.jpg',
        username: this.Form.get('username').value,
      };

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/users/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/users/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}