import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../users.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Rate } from '../../../models/rate';
import { Statics } from '../../../models/statics';
import { User } from '../../../models/user';

@Component({
  selector: 'ngx-users-edit',
  styleUrls: ['./editusers.component.scss'],
  templateUrl: './editusers.component.html',
})
export class EditUserComponent implements OnInit, AfterViewInit{
  Form: FormGroup;
  data: User = JSON.parse(localStorage.getItem('edit'));

  static = new Statics();

  constructor(
    private service: UsersService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  ngOnInit() {
    this.Form.get('password').setValue('');
  }

  ngAfterViewInit() {
    this.Form.get('password').setValue('');
  }

  generateForm() {
    this.Form = new FormGroup({
      username: new FormControl({value: this.data.username, disabled: true}, Validators.required),
      password: new FormControl(''),
      email: new FormControl(this.data.email, Validators.required),
      NOMBRE: new FormControl(this.data.NOMBRE, Validators.required),
      APELLIDO: new FormControl(this.data.APELLIDO, Validators.required),
      AREA: new FormControl(this.data.AREA, Validators.required),
      SEDE: new FormControl(this.data.SEDE, Validators.required),
      TURNO: new FormControl(this.data.TURNO, Validators.required),
    });
  }

  save() {
    if (this.Form.valid) {
      this.data.APELLIDO = this.Form.get('APELLIDO').value;
      this.data.AREA = this.Form.get('AREA').value;
      this.data.NOMBRE = this.Form.get('NOMBRE').value;
      this.data.SEDE = this.Form.get('SEDE').value;
      this.data.TURNO = this.Form.get('TURNO').value;
      this.data.email = this.Form.get('email').value;
      this.data.password = this.Form.get('password').value == '' ? null : this.Form.get('password').value;
      console.log(this.data);
      this.service.update(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/users/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/users/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}