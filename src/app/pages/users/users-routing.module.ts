import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { AddUserComponent } from './addusers/addusers.component';
import { EditUserComponent } from './editusers/editusers.component';
import { ListUsersComponent } from './listusers/listusers.component';

const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [
    {
      path: 'list',
      component: ListUsersComponent,
    },
    {
      path: 'add',
      component: AddUserComponent,
    },
    {
      path: 'edit',
      component: EditUserComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {
}
