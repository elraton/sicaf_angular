import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { UsersRoutingModule } from './users-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { UsersComponent } from './users.component';
import { AddUserComponent } from './addusers/addusers.component';
import { EditUserComponent } from './editusers/editusers.component';
import { ListUsersComponent } from './listusers/listusers.component';

// service
import { UsersService } from './users.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  UsersComponent,
  AddUserComponent,
  EditUserComponent,
  ListUsersComponent
];

const SERVICES = [
  UsersService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  UsersRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class UsersModule { }
