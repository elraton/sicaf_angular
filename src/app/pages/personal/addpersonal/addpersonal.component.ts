import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PersonalService } from '../personal.service';
import { Personal } from '../../../models/personal';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { Personal_CategoryService } from '../../personal_category/personal_category.service';
import { Personal_Category } from '../../../models/personal_category';
import { Personal_SchedulesService } from '../../personal_schedules/personal_schedules.service';

@Component({
  selector: 'ngx-personal-add',
  styleUrls: ['./addpersonal.component.scss'],
  templateUrl: './addpersonal.component.html',
  providers: [Personal_CategoryService, Personal_SchedulesService],
})
export class AddPersonalComponent {
  Form: FormGroup;
  data: Personal;

  static = new Statics();

  areas = this.static.AREAS;
  civil_status = this.static.CIVIL_STATUS;
  sedes = this.static.SEDES;
  status = this.static.STATUS;
  conditions = this.static.CONDITIONS;
  shifts = this.static.SHIFTS;
  departments = [];
  cities;
  filtered_cities = [];
  districts;
  filtered_districts = [];

  categories: Personal_Category[] = [];

  schedulesList = [];
  dropdownSettings = {};

  rol = localStorage.getItem('rol');
  isadmin = (this.rol == 'administracion');

  constructor(
    private service: PersonalService,
    private categoryService: Personal_CategoryService,
    private scheduleService: Personal_SchedulesService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
    this.categoryService.get().subscribe(
      data => {
        this.categories = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
    this.service.getDepartments().subscribe(
      data => {
        this.departments = JSON.parse(JSON.stringify(data));
        this.service.getProvincias().subscribe(
          cities => {
            const arequipa = this.departments.find( x => x.id_ubigeo == '2900');
            this.cities = JSON.parse(JSON.stringify(cities));
            this.filtered_cities = this.cities[arequipa.id_ubigeo];
            this.service.getDistritos().subscribe(
              districts => { 
                this.districts = JSON.parse(JSON.stringify(districts));
              },
              error => { }
            );
          },
          error => { }
        );
      },
      error => { }
    );

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'NOMBRE',
      selectAllText: 'Seleccionar todos All',
      unSelectAllText: 'Deseleccionar todos',
      searchPlaceholderText: 'Buscar',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.scheduleService.get().subscribe(
      data => {
        this.schedulesList = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
  }

  generateForm() {
    this.Form = new FormGroup({
      name: new FormControl('', Validators.required),
      name2: new FormControl(''),
      surname: new FormControl('', Validators.required),
      surname2: new FormControl('', Validators.required),
      dni: new FormControl('', Validators.required),
      birth_date: new FormControl('', Validators.required),
      cellphone: new FormControl('', Validators.required),
      phone: new FormControl(''),
      email: new FormControl('', Validators.required),
      civil_status: new FormControl('', Validators.required),
      department: new FormControl('2900', Validators.required),
      city: new FormControl('', Validators.required),
      district: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      area: new FormControl('', Validators.required),
      sede: new FormControl('', Validators.required),
      shift: new FormControl('', Validators.required),
      is_doctor: new FormControl('0', Validators.required),
      is_kids: new FormControl('0', Validators.required),
      
      last_position: new FormControl(''),
      last_work: new FormControl(''),
      condition: new FormControl('CONTRATO', Validators.required),
      status: new FormControl('ACTIVO', Validators.required),
      enter_date: new FormControl(new Date().toISOString().split('T')[0], Validators.required),
      bank_account: new FormControl(''),
      category: new FormControl(''),
      schedules: new FormControl([]),

      password: new FormControl('', Validators.required),
    });
  }

  onChangeDepartment() {
    const city = this.departments.find( x => x.id_ubigeo == this.Form.controls.department.value);
    this.filtered_cities = this.cities[city.id_ubigeo];
  }

  onChangeProvince() {
    this.filtered_districts = this.districts[this.Form.controls.city.value];
  }

  getStatus(formcontrol) {
    if (formcontrol.touched) {
      if (formcontrol.errors) {
        if (formcontrol.errors.required) {
          return 'danger';
        }
      } else {
        return 'success';
      }
    } else {
      return '';
    }
  }

  save() {
    if (this.Form.valid) {
      let schedules = [];
      for ( const xx of this.Form.controls.schedules.value ) {
        schedules.push(xx.id);
      }
      this.data = {
        id: 0,
        NOMBRE: this.Form.controls.name.value,
        NOMBRE2: this.Form.controls.name2.value,
        APELLIDO: this.Form.controls.surname.value,
        APELLIDO2: this.Form.controls.surname2.value,
        DNI: this.Form.controls.dni.value,
        AREA: this.Form.controls.area.value,
        CELULAR: this.Form.controls.cellphone.value,
        DEPARTAMENTO: this.Form.controls.department.value,
        CIUDAD: this.Form.controls.city.value,
        DISTRITO: this.Form.controls.district.value,
        DIRECCION: this.Form.controls.address.value,
        CONDICION: this.Form.controls.condition.value,
        EMAIL: this.Form.controls.email.value,
        ESTADO: this.Form.controls.status.value,
        ESTADO_CIVIL: this.Form.controls.civil_status.value,
        ES_DOCTOR: this.Form.controls.is_doctor.value,
        ES_NIÑOS: this.Form.controls.is_kids.value,
        FECHA_INGRESO: this.Form.controls.enter_date.value,
        FECHA_NACIMIENTO: this.Form.controls.birth_date.value.toISOString().split('T')[0],
        NMRO_CUENTA: this.Form.controls.bank_account.value,
        SEDE: this.Form.controls.sede.value,
        TELEFONO: this.Form.controls.phone.value,
        TURNO: this.Form.controls.shift.value,
        ULTIMA_POSICION: this.Form.controls.last_position.value,
        ULTIMO_TRABAJO: this.Form.controls.last_work.value,
        CATEGORIA: this.Form.controls.category.value,
        horarios: schedules,
        PASSWORD: this.Form.controls.password.value,
      };

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/personal/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/personal/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}