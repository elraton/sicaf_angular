import { Component } from '@angular/core';

@Component({
  selector: 'ngx-personal',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class PersonalComponent {
}
