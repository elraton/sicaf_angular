import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class PersonalService {
    
    constructor(private http: HttpClient) {}

    getDepartments(): Observable<any> {
        return this.http.get('assets/data/departamentos.json');
    }
    getProvincias(): Observable<any> {
        return this.http.get('assets/data/provincias.json');
    }
    getDistritos(): Observable<any> {
        return this.http.get('assets/data/distritos.json');
    }

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'personal/list');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/add', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/edit', data);
    }

    remove(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/remove', data);
    }
}