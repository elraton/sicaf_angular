import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PersonalService } from '../personal.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Rate } from '../../../models/rate';
import { Statics } from '../../../models/statics';
import { Personal_Category } from '../../../models/personal_category';
import { Personal_CategoryService } from '../../personal_category/personal_category.service';
import { Personal } from '../../../models/personal';
import { Personal_SchedulesService } from '../../personal_schedules/personal_schedules.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'ngx-personal-edit',
  styleUrls: ['./editpersonal.component.scss'],
  templateUrl: './editpersonal.component.html',
  providers: [Personal_CategoryService, Personal_SchedulesService],
})
export class EditPersonalComponent {
  Form: FormGroup;
  data: Personal;

  static = new Statics();

  areas = this.static.AREAS;
  civil_status = this.static.CIVIL_STATUS;
  sedes = this.static.SEDES;
  status = this.static.STATUS;
  conditions = this.static.CONDITIONS;
  shifts = this.static.SHIFTS;
  departments = [];
  cities;
  filtered_cities = [];
  districts;
  filtered_districts = [];

  categories: Personal_Category[] = [];

  schedulesList = [];
  dropdownSettings = {};

  rol = localStorage.getItem('rol');
  isadmin = (this.rol == 'administracion');

  constructor(
    private service: PersonalService,
    private categoryService: Personal_CategoryService,
    private scheduleService: Personal_SchedulesService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
    this.categoryService.get().subscribe(
      data => {
        this.categories = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
    this.service.getDepartments().subscribe(
      data => {
        this.departments = JSON.parse(JSON.stringify(data));
        this.service.getProvincias().subscribe(
          cities => {
            const arequipa = this.departments.find( x => x.id_ubigeo == '2900');
            this.cities = JSON.parse(JSON.stringify(cities));
            this.filtered_cities = this.cities[arequipa.id_ubigeo];
            this.service.getDistritos().subscribe(
              districts => { 
                this.districts = JSON.parse(JSON.stringify(districts));
              },
              error => { }
            );
          },
          error => { }
        );
      },
      error => { }
    );

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'NOMBRE',
      selectAllText: 'Seleccionar todos All',
      unSelectAllText: 'Deseleccionar todos',
      searchPlaceholderText: 'Buscar',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    this.scheduleService.get().subscribe(
      data => {
        this.schedulesList = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
  }

  generateForm() {
    this.data = JSON.parse(localStorage.getItem('edit'));
    console.log(this.data);
    this.Form = new FormGroup({
      name: new FormControl(this.data.NOMBRE, Validators.required),
      name2: new FormControl(this.data.NOMBRE2),
      surname: new FormControl(this.data.APELLIDO, Validators.required),
      surname2: new FormControl(this.data.APELLIDO2, Validators.required),
      dni: new FormControl(this.data.DNI, Validators.required),
      birth_date: new FormControl(new Date(this.data.FECHA_NACIMIENTO), Validators.required),
      cellphone: new FormControl(this.data.CELULAR, Validators.required),
      phone: new FormControl(this.data.TELEFONO),
      email: new FormControl(this.data.EMAIL, Validators.required),
      civil_status: new FormControl(this.data.ESTADO_CIVIL, Validators.required),
      department: new FormControl(this.data.DEPARTAMENTO, Validators.required),
      city: new FormControl(this.data.CIUDAD, Validators.required),
      district: new FormControl(this.data.DISTRITO, Validators.required),
      address: new FormControl(this.data.DIRECCION, Validators.required),
      area: new FormControl(this.data.AREA, Validators.required),
      sede: new FormControl(this.data.SEDE, Validators.required),
      shift: new FormControl(this.data.TURNO, Validators.required),
      is_doctor: new FormControl(+this.data.ES_DOCTOR, Validators.required),
      is_kids: new FormControl(+this.data.ES_NIÑOS, Validators.required),
      
      last_position: new FormControl(this.data.ULTIMA_POSICION),
      last_work: new FormControl(this.data.ULTIMO_TRABAJO),
      condition: new FormControl(this.data.CONDICION, Validators.required),
      status: new FormControl(this.data.ESTADO, Validators.required),
      enter_date: new FormControl(this.data.FECHA_INGRESO, Validators.required),
      bank_account: new FormControl(this.data.NMRO_CUENTA),
      category: new FormControl(this.data.CATEGORIA),
      schedules: new FormControl(this.data.horarios),
      
    });
  }

  onChangeDepartment() {
    const city = this.departments.find( x => x.id_ubigeo == this.Form.controls.department.value);
    this.filtered_cities = this.cities[city.id_ubigeo];
  }

  onChangeProvince() {
    this.filtered_districts = this.districts[this.Form.controls.city.value];
  }

  save() {
    console.log(this.Form);
    if (this.Form.valid) {
      let schedules = [];
      for ( const xx of this.Form.controls.schedules.value ) {
        schedules.push(xx.id);
      }
      const data: Personal = {
        id: this.data.id,
        NOMBRE: this.Form.controls.name.value,
        NOMBRE2: this.Form.controls.name2.value,
        APELLIDO: this.Form.controls.surname.value,
        APELLIDO2: this.Form.controls.surname2.value,
        DNI: this.Form.controls.dni.value,
        AREA: this.Form.controls.area.value,
        CELULAR: this.Form.controls.cellphone.value,
        DEPARTAMENTO: this.Form.controls.department.value,
        CIUDAD: this.Form.controls.city.value,
        DISTRITO: this.Form.controls.district.value,
        DIRECCION: this.Form.controls.address.value,
        CONDICION: this.Form.controls.condition.value,
        EMAIL: this.Form.controls.email.value,
        ESTADO: this.Form.controls.status.value,
        ESTADO_CIVIL: this.Form.controls.civil_status.value,
        ES_DOCTOR: this.Form.controls.is_doctor.value,
        ES_NIÑOS: this.Form.controls.is_kids.value,
        FECHA_INGRESO: this.Form.controls.enter_date.value,
        FECHA_NACIMIENTO: this.Form.controls.birth_date.value.toISOString().split('T')[0],
        NMRO_CUENTA: this.Form.controls.bank_account.value,
        SEDE: this.Form.controls.sede.value,
        TELEFONO: this.Form.controls.phone.value,
        TURNO: this.Form.controls.shift.value,
        ULTIMA_POSICION: this.Form.controls.last_position.value,
        ULTIMO_TRABAJO: this.Form.controls.last_work.value,
        CATEGORIA: this.Form.controls.category.value,
        horarios: schedules,
        PASSWORD: ''
      };

      this.service.update(data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/personal/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }      
  }

  cancel() {
    this.router.navigate(['/pages/personal/list']);
  }

  getStatus(formcontrol) {
    if (formcontrol.touched) {
      if (formcontrol.errors) {
        if (formcontrol.errors.required) {
          return 'danger';
        }
      } else {
        return 'success';
      }
    } else {
      return '';
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}