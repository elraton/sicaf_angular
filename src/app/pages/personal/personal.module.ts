import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { PersonalRoutingModule } from './personal-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

// components
import { PersonalComponent } from './personal.component';
import { AddPersonalComponent } from './addpersonal/addpersonal.component';
import { EditPersonalComponent } from './editpersonal/editpersonal.component';
import { ListPersonalComponent } from './listpersonal/listpersonal.component';

// service
import { PersonalService } from './personal.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  PersonalComponent,
  AddPersonalComponent,
  EditPersonalComponent,
  ListPersonalComponent
];

const SERVICES = [
  PersonalService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  PersonalRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
  NgMultiSelectDropDownModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class PersonalModule { }
