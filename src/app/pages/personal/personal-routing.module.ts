import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonalComponent } from './personal.component';
import { AddPersonalComponent } from './addpersonal/addpersonal.component';
import { EditPersonalComponent } from './editpersonal/editpersonal.component';
import { ListPersonalComponent } from './listpersonal/listpersonal.component';

const routes: Routes = [{
  path: '',
  component: PersonalComponent,
  children: [
    {
      path: 'list',
      component: ListPersonalComponent,
    },
    {
      path: 'add',
      component: AddPersonalComponent,
    },
    {
      path: 'edit',
      component: EditPersonalComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonalRoutingModule {
}
