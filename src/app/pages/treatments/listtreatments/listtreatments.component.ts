import { Component, OnInit } from '@angular/core';
import { TreatmentsService } from '../treatments.service';
import { Personal_Category } from '../../../models/personal_category';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-treatments-list',
  styleUrls: ['./listtreatments.component.scss'],
  templateUrl: './listtreatments.component.html',
})
export class ListTreatmentsComponent implements OnInit{

  data: Personal_Category[];

  loading = true;

  constructor(
    private service: TreatmentsService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      P_NOMBRE: {
        title: 'Paciente',
        type: 'string',
      },
      P_APELLIDO: {
        title: 'Paciente',
        type: 'string',
      },
      FECHA: {
        title: 'Fecha Inicio',
        type: 'string',
      },
      S_PROGRAMADAS: {
        title: 'Ses. Programadas',
        type: 'number',
      },
      S_PAGADAS: {
        title: 'Ses. Pagadas',
        type: 'number',
      },
      S_REALIZADAS: {
        title: 'Ses. Realizadas',
        type: 'number',
      },
      ESTADO: {
        title: 'Estado',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.service.get().subscribe(
      response => {
        this.data = JSON.parse(JSON.stringify(response));
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  onAdd(event): void {
    this.router.navigate(['/pages/treatments/add']);
  }

  onEdit(event): void {
    const data = event.data;
    if ( event.data.ESTADO == 'INACTIVO') {
      this.showToast(NbToastStatus.DANGER, 'No se puede editar un tratamiento inactivo', '');
      return;
    }
    localStorage.setItem('edit', JSON.stringify(data));
    this.router.navigate(['/pages/treatments/edit']);
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if ( event.data.ESTADO == 'INACTIVO') {
      this.showToast(NbToastStatus.DANGER, 'No se puede eliminar un tratamiento inactivo', '');
      return;
    }
    if (window.confirm('¿Seguro que quieres eliminar?')) {
      this.service.remove(data).subscribe(
        success => {
          this.source.remove(data);
          this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          this.showToast(NbToastStatus.SUCCESS, 'Ocurrio un error', '');
        }
      );
    } else {
      return;
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}