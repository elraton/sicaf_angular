import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class TreatmentsService {
    
    constructor(private http: HttpClient) {}

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'servpaciente/list ');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'servpaciente/add ', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'servpaciente/edit ', data);
    }

    updateSchedule(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'horarios/edit ', data);
    }

    remove(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'servpaciente/remove', data);
    }

    getbyrange(data) {
        const url = localStorage.getItem('baseurl');
        return this.http.post( url + 'servpaciente/range', data);
    }

    getSchedule(data) {
        const url = localStorage.getItem('baseurl');
        return this.http.post( url + 'horarios/range', data);
    }
}