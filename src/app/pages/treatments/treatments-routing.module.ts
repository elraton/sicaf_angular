import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TreatmentsComponent } from './treatments.component';
import { AddTreatmentsComponent } from './addtreatments/addtreatments.component';
import { EditTreatmentsComponent } from './edittreatments/edittreatments.component';
import { ListTreatmentsComponent } from './listtreatments/listtreatments.component';

const routes: Routes = [{
  path: '',
  component: TreatmentsComponent,
  children: [
    {
      path: 'list',
      component: ListTreatmentsComponent,
    },
    {
      path: 'add',
      component: AddTreatmentsComponent,
    },
    {
      path: 'edit',
      component: EditTreatmentsComponent,
    },
    {
      path: 'edit/:id',
      component: EditTreatmentsComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TreatmentsRoutingModule {
}
