import { Component } from '@angular/core';

@Component({
  selector: 'ngx-treatments',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class TreatmentsComponent {
}
