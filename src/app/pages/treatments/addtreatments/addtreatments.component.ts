import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { TreatmentsService } from '../treatments.service';
import { Treatment } from '../../../models/treatment';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { Patient } from '../../../models/patient';
import { PersonalService } from '../../personal/personal.service';
import { Personal } from '../../../models/personal';
import { PatientsService } from '../../patients/patients.service';
import { RatesService } from '../../rates/rates.service';
import { Rate } from '../../../models/rate';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'ngx-treatments-add',
  styleUrls: ['./addtreatments.component.scss'],
  templateUrl: './addtreatments.component.html',
  providers: [PersonalService, PatientsService, RatesService]
})
export class AddTreatmentsComponent {

  actionForm: FormGroup;
  treatmentForm: FormGroup;
  consultForm: FormGroup;
  data: Treatment;

  personalList: Personal[] = [];
  personalParsed = [];
  personalSelected: Personal;

  serviceList: Rate[] = [];
  servicesParsed: Rate[] = [];

  patientList: Patient[] = [];
  patient: Patient;
  stepMovement = true;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;

  selectedItem: any = '';
  inputChanged: any = '';

  week = [];
  period = 4;
  time = 15;

  startHour = 7;
  startMinute = 0;
  endHour = 19;
  endMinute = 30;
  lastStartHour;
  lastEndHour;

  config: any = {'placeholder': 'nombre terapeuta', 'sourceField': ['value']};

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        // this.handleEvent('Edited', event);
      }
    },
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  initialTime;

  terapist = '';
  interval = 'lu-vi';
  excludedDays = [0];

  loading = false;
  today = new Date();
  firstDay = new Date( new Date(new Date(this.today.setMonth(this.today.getMonth() - 2)).setDate(1)).setHours(0,0,0) );
  lastDay = new Date( new Date(new Date(this.today.setMonth(this.today.getMonth() + 5)).setDate(1)).setHours(0,0,0) );

  scheduleTreatments = [];
  scheduleConsults = [];
  doctorLegend = '';

  active_treatments: Treatment[] = [];

  constructor(
    private service: TreatmentsService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router,
    private personalService: PersonalService,
    private patientService: PatientsService,
    private serviceService: RatesService,
    private modal: NgbModal
  ) {
    this.generateForm();
    this.serviceService.get().subscribe(
      data => {
        this.serviceList = JSON.parse(JSON.stringify(data));
        this.servicesParsed = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error)
      }
    );
    this.patientService.get().subscribe(
      data => {
        this.patientList = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error)
      }
    );
    this.personalService.get().subscribe(
      data => {
        this.personalList = JSON.parse(JSON.stringify(data));
        for (const xx of this.personalList) {
          this.personalParsed.push(
            {
              id: xx.id,
              value: xx.NOMBRE + ' ' + xx.APELLIDO
            }
          );
        }
      },
      error => {
        console.log(error);
      }
    );
    this.service.getSchedule({from: this.firstDay, to: this.lastDay}).subscribe(
      data => {
        this.scheduleTreatments = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error)
      }
    );
    this.service.getbyrange({from: this.firstDay, to: this.lastDay}).subscribe(
      data => {
        this.scheduleConsults = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error)
      }
    );
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    // this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent, option): void {
    //this.modalData = { event, action };
    //this.modal.open(this.modalContent, { size: 'lg' });
    if (action == 'Clicked') {
      if (event.title == 'Libre') {}    
    }
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: false,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  close() {}

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  generateForm() {
    let nullnumer: number;
    this.actionForm = new FormGroup({
      dni: new FormControl('', Validators.required),
      P_NOMBRE: new FormControl({value: '', disabled: true}),
      P_APELLIDO: new FormControl({value: '', disabled: true}),
      action: new FormControl({value: 0, disabled: true}, Validators.required)
    });
    this.consultForm = new FormGroup({
      FECHA: new FormControl(null),
      HORARIO: new FormControl(''),
      SERVICIO: new FormControl('', Validators.required),
      DOCTOR: new FormControl('', Validators.required),
      T_PROMOCION: new FormControl(''),
    });
    this.treatmentForm = new FormGroup({
      SERVICIO: new FormControl('', Validators.required),
      TERAPEUTA_ID: new FormControl(''),
      INTERVALO: new FormControl('lu-vi', Validators.required),
      FECHA: new FormControl(null),
      FECHA2: new FormControl(null),
      HORARIO: new FormControl(''),
      HORARIO2: new FormControl(''),
      sessions: new FormControl(nullnumer, Validators.required),
      T_PROMOCION: new FormControl(''),
    });
  }

  async generateSchedule() {
    console.log('schedule');
    this.loading = true;
    this.events = [];

    if (+this.personalSelected.ES_DOCTOR == 1) {
      let schedule_aux = JSON.parse(JSON.stringify(this.scheduleConsults));
      schedule_aux = schedule_aux.filter(x => x.DOCTOR_ID == this.personalSelected.id)

      for ( const xx of schedule_aux ) {
        this.events.push({
          id: xx.id,
          start: new Date(xx.FECHA + ' ' + xx.HORARIO),
          end: new Date(new Date(xx.FECHA + ' ' + xx.HORARIO).setMinutes(new Date(xx.FECHA + ' ' + xx.HORARIO).getMinutes() + this.time)),
          title: xx.P_NOMBRE + ' ' + xx.P_APELLIDO,
          color: colors.blue,
          actions: this.actions,
          allDay: false,
          draggable: false
        });
      }
      this.refresh.next();
      
    } else {
      let schedule_aux = JSON.parse(JSON.stringify(this.scheduleTreatments));
      schedule_aux = schedule_aux.filter(x => x.personal_id == this.personalSelected.id)

      for ( const xx of schedule_aux ) {
        this.events.push({
          id: xx.id,
          start: new Date(xx.horario),
          end: new Date(new Date(xx.horario).setMinutes(new Date(xx.horario).getMinutes() + this.time)),
          title: xx.treatment[0].P_NOMBRE + ' ' + xx.treatment[0].P_APELLIDO,
          color: colors.blue,
          actions: this.actions,
          allDay: false,
          draggable: false
        });
      }
      this.refresh.next();
    }
    this.loading = false;
  }

  async generateDay(horario, day_date: Date) {
    if (horario.TIEMPO_ATENCION == 15) {
      this.period = 4;
      this.time = 15;
    }
    if (horario.TIEMPO_ATENCION == 30) {
      this.period = 2;
      this.time = 30;
    }
    if (horario.TIEMPO_ATENCION == 60) {
      this.period = 3;
      this.time = 20;
    }

    let starttime = horario.HORA_INICIO;
    this.initialTime = starttime;
    let start_hour = +starttime.split(':')[0];
    let start_min = +starttime.split(':')[1];

    let endtime = horario.HORA_FIN;
    let end_hour = +endtime.split(':')[0];
    let end_min = +endtime.split(':')[1];

    let day = [];
    let hour = [];

    for( let i = start_hour; i < end_hour; i++) {
      for ( let j = 0; j < this.period; j++) {
        let hour = new Date(new Date(day_date).setHours(i, this.time * j,0));
        const event_aux = this.events.find( x =>  
           x.start.getFullYear() == hour.getFullYear() &&
             x.start.getMonth() == hour.getMonth() &&
              x.start.getDate() == hour.getDate() &&
                x.start.getHours() == hour.getHours() &&
                  x.start.getMinutes() == hour.getMinutes()
        );
        if (event_aux == undefined) {
          this.events.push({
            start: hour,
            end: new Date(new Date(hour).setMinutes(hour.getMinutes() + this.time)),
            title: 'Libre',
            color: colors.blue,
            actions: this.actions,
            allDay: false,
            draggable: false
          });
          this.refresh.next();
        }
      }
    }
    return;
  }

  async verifyDNI() {
    if (this.actionForm.get('dni').value.length == 8) {
      this.patient = this.patientList.find( x => x.DNI == this.actionForm.get('dni').value);
      if (this.patient == null) {
        this.showToast(NbToastStatus.DANGER, 'DNI invalido', '');
      } else {
        this.loading = true;
        await this.service.get().toPromise().then(
          (data) => {
            this.active_treatments = JSON.parse(JSON.stringify(data));
            this.loading = false;
            this.actionForm.get('P_NOMBRE').setValue(this.patient.P_NOMBRE);
            this.actionForm.get('P_APELLIDO').setValue(this.patient.P_APELLIDO);
            this.actionForm.get('action').enable();
          }
        ).catch()
      }
    }
  }

  calculateAge(date) {
    const birthDate = new Date(date);
    const today = new Date();
    let age = today.getFullYear() - birthDate.getFullYear();
    let months = today.getMonth() - birthDate.getMonth();
    if (months < 0 || (months === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  }

  setAction() {
    if ( +this.actionForm.get('action').value == 1 ) {

      this.servicesParsed = [];
      const auxServices = JSON.parse(JSON.stringify(this.serviceList));
      this.servicesParsed = auxServices.filter( x => x.SERVICIO.toUpperCase().indexOf('TERAPIA') == -1);
      const age = this.calculateAge(this.patient.F_NACIMIENTO);
      if (age <= 14) {
        this.servicesParsed = this.servicesParsed.filter( x => x.TIPO_PACIENTE.toUpperCase() == 'NIÑO');
      } else {
        this.servicesParsed = this.servicesParsed.filter( x => x.TIPO_PACIENTE.toUpperCase() == 'ADULTO');
      }
      const auxPersonal = JSON.parse(JSON.stringify(this.personalList));
      this.personalParsed = [];
      for (const xx of auxPersonal.filter( x => x.ES_DOCTOR == 1 )) {
        this.personalParsed.push({
          id: xx.id,
          value: xx.NOMBRE + ' ' + xx.APELLIDO
        });
      }
    } else {
      // Elige tratamientos
      const treatment = this.active_treatments.find( x => x.DNI == this.actionForm.get('dni').value && x.TERAPEUTA !== null && x.ESTADO !== 'INACTIVO');
      if (treatment != undefined) {
        this.showToast(NbToastStatus.DANGER, 'Ya tiene un tratamiento activo', '');
        //this.treatmentForm.get('sessions').disable();
        //return;
      }
      const auxPersonal = JSON.parse(JSON.stringify(this.personalList));
      this.personalParsed = [];
      for (const xx of auxPersonal.filter( x => x.ES_DOCTOR == 0 )) {
        this.personalParsed.push({
          id: xx.id,
          value: xx.NOMBRE + ' ' + xx.APELLIDO
        });
      }

      this.servicesParsed = [];
      const auxServices = JSON.parse(JSON.stringify(this.serviceList));
      this.servicesParsed = auxServices.filter( x => x.SERVICIO.toUpperCase().indexOf('TERAPIA') != -1);

      const age = this.calculateAge(this.patient.F_NACIMIENTO);
      if (age <= 14) {
        this.servicesParsed = this.servicesParsed.filter( x => x.TIPO_PACIENTE.toUpperCase() == 'NIÑO');
      } else {
        this.servicesParsed = this.servicesParsed.filter( x => x.TIPO_PACIENTE.toUpperCase() == 'ADULTO');
      }

    }
  }

  getDoctorSchedule(stepper) {
    if (this.consultForm.valid) {
      this.personalSelected = this.personalList.find(x => x.id == this.selectedItem.id);
      let startHour = 19;
      let endHour = 7;
      this.doctorLegend = 'El doctor viene los dias: ';
      const weekdays = [
        {id: 'lu', value: 'Lunes'},
        {id: 'ma', value: 'Martes'},
        {id: 'mi', value: 'Miercoles'},
        {id: 'ju', value: 'Jueves'},
        {id: 'vi', value: 'Viernes'},
      ];
      for (const xx of this.personalSelected.horarios) {
        if ( +xx.HORA_INICIO.split(':')[0] < startHour ) {
          startHour = +xx.HORA_INICIO.split(':')[0]
        }
        if ( +xx.HORA_FIN.split(':')[0] > endHour ) {
          endHour = +xx.HORA_FIN.split(':')[0]
        }
        const day = weekdays.find( x => x.id == xx.DIA);
        if (day !== undefined) {
          this.doctorLegend = this.doctorLegend + day.value + ', ';
        }
      }
      this.doctorLegend = this.doctorLegend.slice(0, this.doctorLegend.length - 2);
      this.startHour = startHour;
      this.endHour = (endHour + 1) > 19 ? 19 : endHour + 1;
      this.excludedDays = [0,6];
      this.generateSchedule();
      stepper.next();
    }
  }

  getTerapistSchedule(stepper) {
    if (this.treatmentForm.valid && +this.treatmentForm.get('sessions').value > 0) {
      // this.data.SERVICIO = this.servicesParsed[0].id;
      this.personalSelected = this.personalList.find(x => x.id == this.personalParsed[0].id);
      this.terapist = this.personalParsed[0].id;
      //this.generateSchedule();
      this.intervalChange();
      if (this.personalSelected.TURNO == 'TARDE') {
        this.startHour = 13;
        this.startMinute = 30;
        this.endHour = 19;
        this.endMinute = 30;
      } else {
        this.startHour = 7;
        this.startMinute = 0;
        this.endHour = 13;
        this.endMinute = 0;
      }
      stepper.next();
    } else {
      this.showToast(NbToastStatus.DANGER, 'Ya tiene un tratamiento activo', '');
    }
  }

  terapistChange() {
    this.personalSelected = this.personalList.find(x => x.id == +this.terapist);
    this.events = [];
    this.generateSchedule();
  }

  onSelect(item: any) {
    this.selectedItem = item;
    this.consultForm.get('DOCTOR').setValue(item);
  }
 
  onInputChangedEvent(val: string) {
    this.inputChanged = val;
  }

  save(option: number) {
    if (option == 1) {
      if ( this.actionForm.get('action').value == 1 ) {
        if (this.consultForm.valid && this.consultForm.get('FECHA').value !== null) {
          this.data = {
            id: 0,
            DNI: this.actionForm.get('dni').value,
            P_NOMBRE: this.actionForm.get('P_NOMBRE').value,
            P_APELLIDO: this.actionForm.get('P_APELLIDO').value,
            TERAPEUTA: null,
            TERAPEUTA_ID: null,
            ESTADO: 'ACTIVO',
            INTERVALO: null,
            FECHA: this.consultForm.get('FECHA').value,
            HORARIO: this.consultForm.get('HORARIO').value,
            HORARIO2: null,
            S_PROGRAMADAS: 1,
            S_REALIZADAS: 0,
            S_PAGADAS: 0,
            CONSULTA_GRATIS: null,
            SERVICIO: this.consultForm.get('SERVICIO').value,
            ABONO_TERAPIA: null,
            T_PROMOCION: null,
            DOCTOR: this.consultForm.get('DOCTOR').value.value,
            DOCTOR_ID: this.consultForm.get('DOCTOR').value.id,
            HORARIOS: [],
            schedule: [],
          };
          this.service.save(this.data)
          .subscribe(
            res => {
              this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
              this.router.navigate(['/pages/treatments/list']);
            },
            error => {
              this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
            }
          );
        } else {
          this.showToast(NbToastStatus.DANGER, 'Debe elegir un horario', '');
        }
      } else {
        if (this.treatmentForm.valid  && this.treatmentForm.get('FECHA').value !== null) {
          let fecha_init;

          if (this.treatmentForm.get('INTERVALO').value == 'lu-sa' || this.treatmentForm.get('INTERVALO').value == 'ma-ju-sa') {
            if (this.treatmentForm.get('FECHA2').value == null) {
              this.showToast(NbToastStatus.DANGER, 'Debe elegir un horario para sabado', '');
              return;
            }
            if (new Date(this.treatmentForm.get('FECHA').value) < new Date(this.treatmentForm.get('FECHA2').value)) {
              fecha_init = new Date(new Date(this.treatmentForm.get('FECHA').value).toISOString().split('T')[0]+'T11:00:00');
            } else {
              fecha_init = new Date(new Date(this.treatmentForm.get('FECHA2').value).toISOString().split('T')[0]+'T11:00:00');
            }
          } else {
            fecha_init = new Date(new Date(this.treatmentForm.get('FECHA').value).toISOString().split('T')[0]+'T11:00:00');
          }
          this.data = {
            id: 0,
            DNI: this.actionForm.get('dni').value,
            P_NOMBRE: this.actionForm.get('P_NOMBRE').value,
            P_APELLIDO: this.actionForm.get('P_APELLIDO').value,
            TERAPEUTA: this.personalSelected.NOMBRE + ' ' + this.personalSelected.APELLIDO,
            TERAPEUTA_ID: this.personalSelected.id,
            ESTADO: 'ACTIVO',
            INTERVALO: this.treatmentForm.get('INTERVALO').value,
            FECHA: fecha_init.toISOString().split('T')[0],
            HORARIO: this.treatmentForm.get('HORARIO').value,
            HORARIO2: this.treatmentForm.get('HORARIO2').value,
            S_PROGRAMADAS: this.treatmentForm.get('sessions').value,
            S_REALIZADAS: 0,
            S_PAGADAS: 0,
            CONSULTA_GRATIS: null,
            SERVICIO: this.servicesParsed[0].id,
            ABONO_TERAPIA: null,
            T_PROMOCION: null,
            DOCTOR: null,
            DOCTOR_ID: null,
            schedule: [],
            HORARIOS: this.generateTerapistDays(fecha_init, this.treatmentForm.get('sessions').value, this.treatmentForm.get('INTERVALO').value),
          };

          this.service.save(this.data)
          .subscribe(
            res => {
              this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
              this.router.navigate(['/pages/treatments/list']);
            },
            error => {
              this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
            }
          );
        } else {
          this.showToast(NbToastStatus.DANGER, 'Debe elegir un horario', '');
        }
      }
    }
  }

  generateTerapistDays(initDate, sessions, interval) {
    const days = [];
    let days2sum = 0;
    if ( interval == 'lu-vi' || interval == 'lu-sa') {
      days2sum = 1;
    }
    if ( interval == 'lu-mi-vi' || interval == 'ma-ju-sa') {
      days2sum = 2;
    }
    let date = new Date(initDate);
    if ( date.getDay() == 6) {
      date = new Date(date.setHours(this.treatmentForm.get('HORARIO2').value.split(':')[0], this.treatmentForm.get('HORARIO2').value.split(':')[1], 0));
      //date = new Date(date.setHours(9, 0, 0));
    } else {
      date = new Date(date.setHours(this.treatmentForm.get('HORARIO').value.split(':')[0], this.treatmentForm.get('HORARIO').value.split(':')[1], 0));
      //date = new Date(date.setHours(10, 0, 0));
    }
    days.push({
      horario: date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0],
      session: 1,
      status: 1,
      personal_id: this.personalSelected.id,
    });
    const staticDate = new Date(initDate);

    date = new Date(initDate);


    for ( let i = 1; i < sessions; i++) {
      date = new Date( new Date(date).setDate(date.getDate() + days2sum) );

      if ( interval == 'lu-vi') {
        if ( date.getDay() == 6 ) {
          date = new Date( new Date(date).setDate(date.getDate() + 2) );
        }
      }

      if ( interval == 'lu-sa') {
        if ( date.getDay() == 0 ) {
          date = new Date( new Date(date).setDate(date.getDate() + 1) );
        }
      }
      if ( interval == 'lu-mi-vi') {
        if ( date.getDay() == 0 ) {
          date = new Date( new Date(date).setDate(date.getDate() + 1) );
        }
      }
      if ( interval == 'ma-ju-sa') {
        if ( date.getDay() == 1 ) {
          date = new Date( new Date(date).setDate(date.getDate() + 1) );
        }
      }

      if ( date.getDay() == 6) {
        date = new Date(date.setHours(this.treatmentForm.get('HORARIO2').value.split(':')[0], this.treatmentForm.get('HORARIO2').value.split(':')[1], 0));
        //date = new Date(date.setHours(9, 0, 0));
      } else {
        date = new Date(date.setHours(this.treatmentForm.get('HORARIO').value.split(':')[0], this.treatmentForm.get('HORARIO').value.split(':')[1], 0));
        //date = new Date(date.setHours(10, 0, 0));
      }

      days.push({
        horario: date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0],
        session: i + 1,
        status: 1,
        personal_id: this.personalSelected.id,
      });


    }

    return days;
  }

  cancel() {
    this.router.navigate(['/pages/personal/categories/list']);
  }

  emptyHourClicked(event, option) {
    if (new Date(event.date) < new Date( new Date().setHours(6,0,0) )) {
      this.showToast(NbToastStatus.DANGER, 'Fecha invalida, no puede ser antes de hoy', '');
      return;
    }
    if (+this.actionForm.get('action').value == 1) {
      this.consultForm.get('FECHA').setValue(new Date(event.date).toISOString().split('T')[0]);
      this.consultForm.get('HORARIO').setValue(new Date(event.date).toTimeString().split(' ')[0]);
    } else {
      if (new Date(event.date).getDay() == 6) {
        this.treatmentForm.get('HORARIO2').setValue(new Date(event.date).toTimeString().split(' ')[0]);
        this.treatmentForm.get('FECHA2').setValue(new Date(event.date).toISOString().split('T')[0]);
      } else {
        this.treatmentForm.get('HORARIO').setValue(new Date(event.date).toTimeString().split(' ')[0]);
        this.treatmentForm.get('FECHA').setValue(new Date(event.date).toISOString().split('T')[0]);
      }
    }
    if (new Date(event.date).getDay() == 6) {
      this.events = this.events.filter(x => x.id != 101);
      this.events.push({
        id: 101,
        start: new Date(event.date),
        end: new Date(new Date(event.date).setMinutes(new Date(event.date).getMinutes() + this.time)),
        title: this.actionForm.get('P_NOMBRE').value + ' ' + this.actionForm.get('P_APELLIDO').value,
        color: colors.yellow,
        actions: this.actions,
        allDay: false,
        draggable: false
      });
    } else {
      this.events = this.events.filter(x => x.id != 100);
      this.events.push({
        id: 100,
        start: new Date(event.date),
        end: new Date(new Date(event.date).setMinutes(new Date(event.date).getMinutes() + this.time)),
        title: this.actionForm.get('P_NOMBRE').value + ' ' + this.actionForm.get('P_APELLIDO').value,
        color: colors.yellow,
        actions: this.actions,
        allDay: false,
        draggable: false
      });
    }
    this.refresh.next();
  }

  intervalChange() {
    switch(this.interval) {
      case 'lu-vi': {
        this.excludedDays = [0, 6];
        this.refresh.next();
        break;
      }
      case 'lu-sa': {
        this.excludedDays = [0];
        this.refresh.next();
        break;
      }
      case 'lu-mi-vi': {
        this.excludedDays = [0, 6];
        this.refresh.next();
        break;
      }
      case 'ma-ju-sa': {
        this.excludedDays = [0];
        this.refresh.next();
        break;
      }
    }

    this.events = [];
    this.generateSchedule();
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}