import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { TreatmentsRoutingModule } from './treatments-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

// components
import { TreatmentsComponent } from './treatments.component';
import { AddTreatmentsComponent } from './addtreatments/addtreatments.component';
import { EditTreatmentsComponent } from './edittreatments/edittreatments.component';
import { ListTreatmentsComponent } from './listtreatments/listtreatments.component';

// service
import { TreatmentsService } from './treatments.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbStepperModule } from '@nebular/theme';

const COMPONENTS = [
  TreatmentsComponent,
  AddTreatmentsComponent,
  EditTreatmentsComponent,
  ListTreatmentsComponent
];

const SERVICES = [
  TreatmentsService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  TreatmentsRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
  NbStepperModule,
  NgxMaterialTimepickerModule,
  AutocompleteModule.forRoot(),
  CalendarModule.forRoot({
    provide: DateAdapter,
    useFactory: adapterFactory
  })
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class TreatmentsModule { }
