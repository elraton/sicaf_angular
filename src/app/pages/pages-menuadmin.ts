import { NbMenuItem } from '@nebular/theme';

export const MENU_ADMIN: NbMenuItem[] = [
  {
    title: 'Personal',
    icon: 'fas fa-user-md',
    children: [
      {
        title: 'Listar Personal',
        link: '/pages/personal/list',
      },
      {
        title: 'Agregar nuevo',
        link: '/pages/personal/add',
      }
    ]
  },
  {
    title: 'Marcacion',
    icon: 'far fa-clock',
    children: [
      {
        title: 'Ingreso',
        link: '/pages/assistance/in',
      },
      {
        title: 'Salida',
        link: '/pages/assistance/out',
      },
    ]
  },
  {
    title: 'Caja',
    icon: 'fas fa-dollar-sign',
    children: [
      {
        title: 'Ingreso Caja',
        icon: 'fas fa-sign-in-alt',
        children: [
          {
            title: 'Listar ingresos',
            link: '/pages/cash/income/list',
          },
          {
            title: 'Agregar ingresos',
            link: '/pages/cash/income/add',
          },
        ]
      },
      {
        title: 'Egreso Caja',
        icon: 'fas fa-sign-out-alt',
        children: [
          {
            title: 'Listar egresos',
            link: '/pages/cash/outcome/list',
          },
          {
            title: 'Agregar egreso',
            link: '/pages/cash/outcome/add',
          },
        ]
      },
      /*{
        title: 'Ventas',
        icon: 'fas fa-user-md',
        children: [
          {
            title: 'Listar ventas',
            link: '/pages/cash/sales/list',
          },
          {
            title: 'Agregar ventas',
            link: '/pages/cash/sales/add',
          },
        ]
      },*/
    ]
  },
  {
    title: 'Pacientes',
    icon: 'fas fa-users',
    children: [
      {
        title: 'Listar pacientes',
        link: '/pages/patients/list',
      },
      {
        title: 'Agregar paciente',
        link: '/pages/patients/add',
      },
    ]
  },
  {
    title: 'Tratamientos',
    icon: 'fas fa-file-medical',
    children: [
      {
        title: 'Listar tratamientos',
        link: '/pages/treatments/list',
      },
      {
        title: 'Agregar tratamiento',
        link: '/pages/treatments/add',
      },
    ]
  },
  {
    title: 'Atención diaria',
    icon: 'far fa-check-square',
    children: [
      {
        title: 'Listar atenciones',
        link: '/pages/attentions/list',
      },
      {
        title: 'Agregar atencion',
        link: '/pages/attentions/add',
      },
    ]
  },
  {
    title: 'Horario',
    icon: 'far fa-calendar-alt',
    link: '/pages/schedule',
  },
  {
    title: 'Administración',
    group: true,
  },
  {
    title: 'Reportes',
    icon: 'fas fa-user-md',
    children: [
      {
        title: 'Reporte Asistencias',
        link: '/pages/reports/assistance',
      },
      {
        title: 'Reporte Atenciones',
        link: '/pages/reports/atentions',
      },
      {
        title: 'Reporte Caja',
        link: '/pages/reports/cash',
      }
    ]
  },
  {
    title: 'Usuarios',
    icon: 'fas fa-user-md',
    children: [
      {
        title: 'Listar Usuarios',
        link: '/pages/users/list',
      },
      {
        title: 'Agregar nuevo',
        link: '/pages/users/add',
      }
    ]
  },
  {
    title: 'Categoria Personal',
    icon: 'fas fa-user-cog',
    children: [
      {
        title: 'Listar categorias',
        link: '/pages/personal/categories/list',
      },
      {
        title: 'Agregar nueva',
        link: '/pages/personal/categories/add',
      },
    ]
  },
  {
    title: 'Horario Personal',
    icon: 'fas fa-user-cog',
    children: [
      {
        title: 'Listar horarios',
        link: '/pages/personal/schedules/list',
      },
      {
        title: 'Agregar nuevo',
        link: '/pages/personal/schedules/add',
      },
    ]
  },
  {
    title: 'Servicios',
    icon: 'fas fa-user-cog',
    children: [
      {
        title: 'Listar servcios',
        link: '/pages/services/rates/list',
      },
      {
        title: 'Agregar servicio',
        link: '/pages/services/rates/add',
      },
    ]
  },
  {
    title: 'Convenios',
    icon: 'fas fa-user-cog',
    children: [
      {
        title: 'Listar convenios',
        link: '/pages/agreements/list',
      },
      {
        title: 'Agregar convenio',
        link: '/pages/agreements/add',
      },
    ]
  },
  {
    title: 'Promociones',
    icon: 'fas fa-user-cog',
    children: [
      {
        title: 'Listar promociones',
        link: '/pages/services/promos/list',
      },
      {
        title: 'Agregar promoción',
        link: '/pages/services/promos/add',
      },
    ]
  },
];
