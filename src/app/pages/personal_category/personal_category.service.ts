import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { Personal_Category } from '../../models/personal_category';

@Injectable()
export class Personal_CategoryService {
    
    constructor(private http: HttpClient) {}

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'personal/category/list ');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/category/add ', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/category/edit ', data);
    }

    remove(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/category/remove', data);
    }
}