import { Component } from '@angular/core';

@Component({
  selector: 'ngx-personal_category',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class Personal_CategoryComponent {
}
