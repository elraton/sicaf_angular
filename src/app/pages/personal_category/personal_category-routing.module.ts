import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Personal_CategoryComponent } from './personal_category.component';
import { AddPersonal_CategoryComponent } from './addpersonal_category/addpersonal_category.component';
import { EditPersonal_CategoryComponent } from './editpersonal_category/editpersonal_category.component';
import { ListPersonal_CategoryComponent } from './listpersonal_category/listpersonal_category.component';

const routes: Routes = [{
  path: '',
  component: Personal_CategoryComponent,
  children: [
    {
      path: 'list',
      component: ListPersonal_CategoryComponent,
    },
    {
      path: 'add',
      component: AddPersonal_CategoryComponent,
    },
    {
      path: 'edit',
      component: EditPersonal_CategoryComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Personal_CategoryRoutingModule {
}
