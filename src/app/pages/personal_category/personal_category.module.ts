import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { Personal_CategoryRoutingModule } from './personal_category-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { Personal_CategoryComponent } from './personal_category.component';
import { AddPersonal_CategoryComponent } from './addpersonal_category/addpersonal_category.component';
import { EditPersonal_CategoryComponent } from './editpersonal_category/editpersonal_category.component';
import { ListPersonal_CategoryComponent } from './listpersonal_category/listpersonal_category.component';

// service
import { Personal_CategoryService } from './personal_category.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  Personal_CategoryComponent,
  AddPersonal_CategoryComponent,
  EditPersonal_CategoryComponent,
  ListPersonal_CategoryComponent
];

const SERVICES = [
  Personal_CategoryService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  Personal_CategoryRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class Personal_CategoryModule { }
