import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Personal_CategoryService } from '../personal_category.service';
import { Personal_Category } from '../../../models/personal_category';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-personal_category-edit',
  styleUrls: ['./editpersonal_category.component.scss'],
  templateUrl: './editpersonal_category.component.html',
})
export class EditPersonal_CategoryComponent {
  Form: FormGroup;
  data: Personal_Category;

  constructor(
    private service: Personal_CategoryService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.data = JSON.parse(localStorage.getItem('edit'));
    this.Form = new FormGroup({
      name: new FormControl(this.data.NOMBRE, Validators.required),
      salary: new FormControl(this.data.SALARIO, Validators.required),
    });
  }

  save() {
    if (this.Form.valid) {
      this.data.NOMBRE = this.Form.controls.name.value;
      this.data.SALARIO = this.Form.controls.salary.value;

      this.service.update(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/personal/categories/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    }            
  }

  cancel() {
    this.router.navigate(['/pages/personal/categories/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}