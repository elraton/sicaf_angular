import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { AgreementsRoutingModule } from './agreements-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { AgreementsComponent } from './agreements.component';
import { AddAgreementsComponent } from './addagreements/addagreements.component';
import { EditAgreementsComponent } from './editagreements/editagreements.component';
import { ListAgreementsComponent } from './listagreements/listagreements.component';

// service
import { AgreementsService } from './agreements.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  AgreementsComponent,
  AddAgreementsComponent,
  EditAgreementsComponent,
  ListAgreementsComponent
];

const SERVICES = [
  AgreementsService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  AgreementsRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class AgreementsModule { }
