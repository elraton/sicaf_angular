import { Component, OnInit } from '@angular/core';
import { AgreementsService } from '../agreements.service';
import { Personal_Category } from '../../../models/personal_category';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Rate } from '../../../models/rate';

@Component({
  selector: 'ngx-agreements-list',
  styleUrls: ['./listagreements.component.scss'],
  templateUrl: './listagreements.component.html',
})
export class ListAgreementsComponent implements OnInit{

  data: Rate[];
  loading = true;

  constructor(
    private service: AgreementsService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      NOMBRE: {
        title: 'Nombre',
        type: 'string',
      },
      PORCENTAJE: {
        title: 'Porcentaje',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.service.get().subscribe(
      response => {
        this.data = JSON.parse(JSON.stringify(response));
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  onAdd(event): void {
    this.router.navigate(['/pages/agreements/add']);
  }

  onEdit(event): void {
    const data = event.data;
    localStorage.setItem('edit', JSON.stringify(data));
    this.router.navigate(['/pages/agreements/edit']);
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.remove(data).subscribe(
        success => {
          this.source.remove(data);
          this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          this.showToast(NbToastStatus.SUCCESS, 'Ocurrio un error', '');
        }
      );
    } else {
      return;
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}