import { Component } from '@angular/core';

@Component({
  selector: 'ngx-agreements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AgreementsComponent {
}
