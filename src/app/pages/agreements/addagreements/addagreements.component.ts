import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AgreementsService } from '../agreements.service';
import { Rate } from '../../../models/rate';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { Promos } from '../../../models/promos';
import { PromosService } from '../../promos/promos.service';
import { Agreement } from '../../../models/agreement';

@Component({
  selector: 'ngx-agreements-add',
  styleUrls: ['./addagreements.component.scss'],
  templateUrl: './addagreements.component.html',
  providers: [PromosService]
})
export class AddAgreementsComponent {
  Form: FormGroup;
  data: Agreement;

  constructor(
    private service: AgreementsService,
    private promoService: PromosService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.Form = new FormGroup({
      NOMBRE: new FormControl('', Validators.required),
      PORCENTAJE: new FormControl('', Validators.required),
    });
  }

  save() {

    if (this.Form.valid) {
      this.data = {
        NOMBRE: this.Form.get('NOMBRE').value,
        PORCENTAJE: this.Form.get('PORCENTAJE').value
      };

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/agreements/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }      
  }

  cancel() {
    this.router.navigate(['/pages/agreements/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}