import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgreementsComponent } from './agreements.component';
import { AddAgreementsComponent } from './addagreements/addagreements.component';
import { EditAgreementsComponent } from './editagreements/editagreements.component';
import { ListAgreementsComponent } from './listagreements/listagreements.component';

const routes: Routes = [{
  path: '',
  component: AgreementsComponent,
  children: [
    {
      path: 'list',
      component: ListAgreementsComponent,
    },
    {
      path: 'add',
      component: AddAgreementsComponent,
    },
    {
      path: 'edit',
      component: EditAgreementsComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgreementsRoutingModule {
}
