import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AgreementsService } from '../agreements.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Rate } from '../../../models/rate';
import { Statics } from '../../../models/statics';
import { Promos } from '../../../models/promos';
import { PromosService } from '../../promos/promos.service';
import { Agreement } from '../../../models/agreement';

@Component({
  selector: 'ngx-agreements-edit',
  styleUrls: ['./editagreements.component.scss'],
  templateUrl: './editagreements.component.html',
  providers: [PromosService]
})
export class EditAgreementsComponent {
  Form: FormGroup;
  data: Agreement;

  constructor(
    private service: AgreementsService,
    private promoService: PromosService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.data = JSON.parse(localStorage.getItem('edit'));
    this.Form = new FormGroup({
      NOMBRE: new FormControl(this.data.NOMBRE, Validators.required),
      PORCENTAJE: new FormControl(this.data.PORCENTAJE, Validators.required),
    });
  }

  save() {

    if (this.Form.valid) {
      this.data.NOMBRE = this.Form.get('NOMBRE').value;
      this.data.PORCENTAJE = this.Form.get('PORCENTAJE').value;

      this.service.update(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/agreements/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }      
  }
  
  cancel() {
    this.router.navigate(['/pages/agreements/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}