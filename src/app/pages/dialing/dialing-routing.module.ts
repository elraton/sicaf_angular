import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DialingComponent } from './dialing.component';
import { InDialingComponent } from './indialing/indialing.component';
import { OutDialingComponent } from './outdialing/outdialing.component';
import { ListDialingComponent } from './listdialing/listdialing.component';

const routes: Routes = [{
  path: '',
  component: DialingComponent,
  children: [
    {
      path: 'list',
      component: ListDialingComponent,
    },
    {
      path: 'in',
      component: InDialingComponent,
    },
    {
      path: 'out',
      component: OutDialingComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DialingRoutingModule {
}
