import { Component, ViewChildren, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DialingService } from '../dialing.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { Dialing } from '../../../models/dialing';
import { Personal } from '../../../models/personal';
import { PersonalService } from '../../personal/personal.service';

@Component({
  selector: 'ngx-dialing-out',
  styleUrls: ['./outdialing.component.scss'],
  templateUrl: './outdialing.component.html',
  providers: [PersonalService]
})
export class OutDialingComponent {
  Form: FormGroup;
  data: Dialing;
  personalList: Personal[];
  personalSelected: Personal;
  today = new Date();
  time;

  morning_outhour = 13;
  morning_outminute = 0;
  afternoon_outhour = 19;
  afternoon_outminute = 30;

  @ViewChild('dni') DNI: ElementRef;

  constructor(
    private service: DialingService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router,
    private personalService: PersonalService,
  ) {
    this.generateForm();
    this.personalService.get().subscribe(
      data => {
        this.personalList = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
  }

  searchPersonal(control) {
    if (this.Form.controls.dni.value.length == 8) {
      const personal = this.personalList.find(x => x.DNI == this.Form.controls.dni.value);
      if (personal !== undefined) {
        this.personalSelected = personal;
        this.showToast(NbToastStatus.SUCCESS, 'DNI encontrado', '');
        control.focus();
        const now = new Date();
        this.time = now.getHours() + ':' + now.getMinutes() + ':00';
      } else {
        this.showToast(NbToastStatus.DANGER, 'DNI mal ingresado', '');
        this.Form.controls.dni.setValue('');
      }
    }
  }

  verifyPassword(control) {
    if (this.Form.controls.pass.value.length == 4) {
      if (this.personalSelected && this.personalSelected.PASSWORD == this.Form.get('pass').value) {
        this.showToast(NbToastStatus.SUCCESS, 'Password correcto', '');
        control.focus();
      } else {
        this.showToast(NbToastStatus.DANGER, 'Password incorrecto', '');
        this.Form.controls.pass.setValue('');
      }
    } else {
      this.showToast(NbToastStatus.DANGER, 'Password incorrecto', '');
      this.Form.controls.pass.setValue('');
    }
  }

  generateForm() {
    this.Form = new FormGroup({
      dni: new FormControl('Ingresa tu DNI', Validators.required),
      pass: new FormControl('Ingresa tu contraseña', Validators.required),
      attentions: new FormControl(0, Validators.required),
    });
  }

  tabPressed(event) {
    event.preventDefault();
  }

  save() {
    if (this.Form.valid) {
      let status = '';
      if (this.personalSelected.TURNO == 'TARDE') {
        const hour = +this.time.split(':')[0];
        const minute = +this.time.split(':')[1];
        if (hour == this.afternoon_outhour) {
          if (minute >= this.afternoon_outminute) {
            status = 'SALIDA';
          } else {
            status = 'SALIO ANTES';
          }
        } else {
          if (hour > this.afternoon_outhour) {
            status = 'SALIDA';
          } else {
            status = 'SALIO ANTES';
          }
        }
      } else {
        const hour = +this.time.split(':')[0];
        const minute = +this.time.split(':')[1];
        if (hour == this.morning_outhour) {
          if (minute >= this.morning_outminute) {
            status = 'SALIDA';
          } else {
            status = 'SALIO ANTES';
          }
        } else {
          if (hour > this.morning_outhour) {
            status = 'SALIDA';
          } else {
            status = 'SALIO ANTES';
          }
        }
      }
      this.data = {
        id: 0,
        FECHA: this.today.toISOString().split('T')[0],
        CODIGO: this.Form.controls.dni.value,
        NOMBRE_APELLIDO: this.personalSelected.NOMBRE + ' ' + this.personalSelected.APELLIDO,
        TIPO_REGISTRO: 'SALIDA',
        TURNO: this.personalSelected.TURNO,
        SEDE: this.personalSelected.SEDE,
        HORA_PERSONAL: this.personalSelected.TURNO == 'TARDE' ? '19:30:00' : '13:00:00',
        HORA_REGISTRO: this.time,
        ESTADO: status,
        ATENCIONES: this.Form.controls.attentions.value,
      };

      /*OK -> TEMPRANO
      LLEGO TARDE -> TARDE
      REINGRESO -> REINGRESO
      SALIO ANTES -> SALIO ANTES
      FALTA -> FALTA*/
      // finalizar ingresos,

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.Form.controls.dni.setValue('');
          this.Form.controls.pass.setValue('');
          this.Form.controls.attentions.setValue('');
          this.DNI.nativeElement.focus();
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cleanFormControl(control) {
    control.value = '';
  }

  cancel() {
    
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}