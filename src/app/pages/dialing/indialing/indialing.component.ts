import { Component, ViewChildren, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DialingService } from '../dialing.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { Dialing } from '../../../models/dialing';
import { Personal } from '../../../models/personal';
import { PersonalService } from '../../personal/personal.service';

@Component({
  selector: 'ngx-dialing-in',
  styleUrls: ['./indialing.component.scss'],
  templateUrl: './indialing.component.html',
  providers: [PersonalService]
})
export class InDialingComponent implements AfterViewInit{
  Form: FormGroup;
  data: Dialing;
  personalList: Personal[];
  personalSelected: Personal;
  today = new Date();
  time;

  morning_inhour = 7;
  morning_inminute = 0;
  afternoon_inhour = 13;
  afternoon_inminute = 30;

  //@ViewChildren('dni') DNI: ElementRef;
  @ViewChild('dni') DNI: ElementRef;

  constructor(
    private service: DialingService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router,
    private personalService: PersonalService,
  ) {
    this.generateForm();
    this.personalService.get().subscribe(
      data => {
        this.personalList = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
  }

  ngAfterViewInit(): void {
    this.DNI.nativeElement.focus();    
  }

  searchPersonal(control) {
    if (this.Form.controls.dni.value.length == 8) {
      const personal = this.personalList.find(x => x.DNI == this.Form.controls.dni.value);
      if (personal !== undefined) {
        this.personalSelected = personal;
        this.showToast(NbToastStatus.SUCCESS, 'DNI encontrado', '');
        control.focus();
        const now = new Date();
        this.time = now.getHours() + ':' + now.getMinutes() + ':00';
      } else {
        this.showToast(NbToastStatus.DANGER, 'DNI mal ingresado', '');
        this.Form.controls.dni.setValue('');
        control.focus();
      }
    }
  }

  tabPressed(event) {
    event.preventDefault();
  }

  generateForm() {
    this.Form = new FormGroup({
      dni: new FormControl('Ingresa tu DNI', Validators.required),
      pass: new FormControl('Ingresa tu contraseña', Validators.required),
    });
  }

  save() {
    if (this.Form.valid) {
      if (this.Form.get('pass').value == this.personalSelected.PASSWORD) {
        let status = '';
        if (this.personalSelected.TURNO == 'TARDE') {
          const hour = +this.time.split(':')[0];
          const minute = +this.time.split(':')[1];
          if (hour == this.afternoon_inhour) {
            if (minute > this.afternoon_inminute) {
              status = 'LLEGO TARDE';
            } else {
              status = 'OK';
            }
          } else {
            if (hour > this.afternoon_inhour) {
              status = 'LLEGO TARDE';
            } else {
              status = 'OK';
            }
          }
        } else {
          const hour = +this.time.split(':')[0];
          const minute = +this.time.split(':')[1];
          if (hour == this.morning_inhour) {
            if (minute > this.morning_inminute) {
              status = 'LLEGO TARDE';
            } else {
              status = 'OK';
            }
          } else {
            if (hour > this.morning_inhour) {
              status = 'LLEGO TARDE';
            } else {
              status = 'OK';
            }
          }
        }
        this.data = {
          id: 0,
          FECHA: this.today.toISOString().split('T')[0],
          CODIGO: this.Form.controls.dni.value,
          NOMBRE_APELLIDO: this.personalSelected.NOMBRE + ' ' + this.personalSelected.APELLIDO,
          TIPO_REGISTRO: 'INGRESO',
          TURNO: this.personalSelected.TURNO,
          SEDE: this.personalSelected.SEDE,
          HORA_PERSONAL: this.personalSelected.TURNO == 'TARDE' ? '13:30:00' : '07:00:00',
          HORA_REGISTRO: this.time,
          ESTADO: status,
          ATENCIONES: 0,
        };

        /*OK -> TEMPRANO
        LLEGO TARDE -> TARDE
        REINGRESO -> REINGRESO
        SALIO ANTES -> SALIO ANTES
        FALTA -> FALTA*/
        // finalizar ingresos,

        this.service.save(this.data)
          .subscribe(
            res => {
              this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
              this.Form.controls.dni.setValue('');
              this.Form.controls.pass.setValue('');
              this.DNI.nativeElement.focus();
            },
            error => {
              this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
            }
          );
      } else {
        this.showToast(NbToastStatus.DANGER, 'Password Incorrecto', '');
        this.Form.get('pass').setValue('');
      }
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cleanFormControl(control) {
    control.value = '';
  }

  cancel() {
    
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}