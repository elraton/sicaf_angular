import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable()
export class DialingService {
    
    constructor(private http: HttpClient) {}

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'iopersonal/list ');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'iopersonal/add ', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'iopersonal/edit ', data);
    }

    remove(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'iopersonal/remove', data);
    }
}