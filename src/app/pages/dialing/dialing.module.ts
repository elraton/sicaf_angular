import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { DialingRoutingModule } from './dialing-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { DialingComponent } from './dialing.component';
import { InDialingComponent } from './indialing/indialing.component';
import { OutDialingComponent } from './outdialing/outdialing.component';
import { ListDialingComponent } from './listdialing/listdialing.component';

// service
import { DialingService } from './dialing.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  DialingComponent,
  InDialingComponent,
  OutDialingComponent,
  ListDialingComponent
];

const SERVICES = [
  DialingService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  DialingRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DialingModule { }
