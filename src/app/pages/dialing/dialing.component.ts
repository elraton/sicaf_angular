import { Component } from '@angular/core';

@Component({
  selector: 'ngx-dialing',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class DialingComponent {
}
