import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { MENU_ADMIN } from './pages-menuadmin';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu *ngIf="rol == 'administracion'" [items]="admin"></nb-menu>
      <nb-menu *ngIf="rol != 'administracion'" [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;
  admin = MENU_ADMIN;
  rol = localStorage.getItem('rol');

}
