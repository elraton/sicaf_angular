import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { OutcomecashRoutingModule } from './outcomecash-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { OutcomecashComponent } from './outcomecash.component';
import { AddOutcomecashComponent } from './addoutcomecash/addoutcomecash.component';
import { EditOutcomecashComponent } from './editoutcomecash/editoutcomecash.component';
import { ListOutcomecashComponent } from './listoutcomecash/listoutcomecash.component';

// service
import { OutcomecashService } from './outcomecash.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  OutcomecashComponent,
  AddOutcomecashComponent,
  EditOutcomecashComponent,
  ListOutcomecashComponent
];

const SERVICES = [
  OutcomecashService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  OutcomecashRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class OutcomecashModule { }
