import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { OutcomecashService } from '../outcomecash.service';
import { OutcomeCash } from '../../../models/outcomecash';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';

@Component({
  selector: 'ngx-outcomecash-edit',
  styleUrls: ['./editoutcomecash.component.scss'],
  templateUrl: './editoutcomecash.component.html',
})
export class EditOutcomecashComponent {
  Form: FormGroup;
  data: OutcomeCash = JSON.parse(localStorage.getItem('edit'));
  user;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;
  status = new Statics().STATUS;

  constructor(
    private service: OutcomecashService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
    this.service.getUser().subscribe(
      data => {
        this.user = JSON.parse(JSON.stringify(data)).success;
      },
      error => {}
    )
  }

  generateForm() {
    this.Form = new FormGroup({
      RECEPTOR: new FormControl(this.data.RECEPTOR, Validators.required),
      IMPORTE: new FormControl(this.data.IMPORTE, Validators.required),
      DESCRIPCION: new FormControl(this.data.DESCRIPCION, Validators.required),
    });
  }

  onlynumbers(event) {
    if ( event.keyCode == 46 || event.keyCode == 8 ) {
      // let it happen, don't do anything
    } else {
      // Ensure that it is a number and stop the keypress
      if (event.keyCode < 48 || event.keyCode > 57 ) {
          event.preventDefault(); 
      }
    }
  }

  save() {
    if (this.Form.valid) {
      const today = new Date();
      this.data.DESCRIPCION = this.Form.get('DESCRIPCION').value;
      this.data.EMISOR = this.user.NOMBRE + ' ' + this.user.APELLIDO;
      this.data.FECHA = today.toISOString().split('T')[0] + ' ' + today.getHours() +':'+today.getMinutes()+':00';
      this.data.IMPORTE = this.Form.get('IMPORTE').value;
      this.data.RECEPTOR = this.Form.get('RECEPTOR').value;
      this.data.SEDE = this.user.SEDE;
      this.data.TURNO =  this.user.TURNO;

      this.service.update(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/cash/outcome/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/cash/outcome/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}