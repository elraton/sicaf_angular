import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OutcomecashComponent } from './outcomecash.component';
import { AddOutcomecashComponent } from './addoutcomecash/addoutcomecash.component';
import { EditOutcomecashComponent } from './editoutcomecash/editoutcomecash.component';
import { ListOutcomecashComponent } from './listoutcomecash/listoutcomecash.component';

const routes: Routes = [{
  path: '',
  component: OutcomecashComponent,
  children: [
    {
      path: 'list',
      component: ListOutcomecashComponent,
    },
    {
      path: 'add',
      component: AddOutcomecashComponent,
    },
    {
      path: 'edit',
      component: EditOutcomecashComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OutcomecashRoutingModule {
}
