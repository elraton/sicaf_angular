import { Component } from '@angular/core';

@Component({
  selector: 'ngx-outcomecash',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class OutcomecashComponent {
}
