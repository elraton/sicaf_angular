import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { OutcomecashService } from '../outcomecash.service';
import { OutcomeCash } from '../../../models/outcomecash';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';

@Component({
  selector: 'ngx-outcomecash-add',
  styleUrls: ['./addoutcomecash.component.scss'],
  templateUrl: './addoutcomecash.component.html',
})
export class AddOutcomecashComponent {
  Form: FormGroup;
  data: OutcomeCash;
  user;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;
  status = new Statics().STATUS;

  constructor(
    private service: OutcomecashService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
    this.service.getUser().subscribe(
      data => {
        this.user = JSON.parse(JSON.stringify(data)).success;
      },
      error => {}
    )
  }

  generateForm() {
    this.Form = new FormGroup({
      RECEPTOR: new FormControl('', Validators.required),
      IMPORTE: new FormControl('', Validators.required),
      DESCRIPCION: new FormControl('', Validators.required),
    });
  }

  onlynumbers(event) {
    if ( event.keyCode == 46 || event.keyCode == 8 ) {
      // let it happen, don't do anything
    } else {
      // Ensure that it is a number and stop the keypress
      if (event.keyCode < 48 || event.keyCode > 57 ) {
          event.preventDefault(); 
      }
    }
  }

  save() {
    if (this.Form.valid) {
      const today = new Date();
      this.data = {
        id: 0,
        DESCRIPCION: this.Form.get('DESCRIPCION').value,
        EMISOR: this.user.NOMBRE + ' ' + this.user.APELLIDO,
        FECHA: today.toISOString().split('T')[0] + ' ' + today.getHours() +':'+today.getMinutes()+':00',
        IMPORTE: this.Form.get('IMPORTE').value,
        RECEPTOR: this.Form.get('RECEPTOR').value,
        SEDE: this.user.SEDE,
        TURNO:  this.user.TURNO
      };

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/cash/outcome/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/cash/outcome/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}