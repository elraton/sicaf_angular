import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PatientsComponent } from './patients.component';
import { AddPatientsComponent } from './addpatients/addpatients.component';
import { EditPatientsComponent } from './editpatients/editpatients.component';
import { ListPatientsComponent } from './listpatients/listpatients.component';

const routes: Routes = [{
  path: '',
  component: PatientsComponent,
  children: [
    {
      path: 'list',
      component: ListPatientsComponent,
    },
    {
      path: 'add',
      component: AddPatientsComponent,
    },
    {
      path: 'edit',
      component: EditPatientsComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientsRoutingModule {
}
