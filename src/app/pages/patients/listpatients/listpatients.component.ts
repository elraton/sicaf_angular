import { Component, OnInit } from '@angular/core';
import { PatientsService } from '../patients.service';
import { Patient } from '../../../models/patient';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-patients-list',
  styleUrls: ['./listpatients.component.scss'],
  templateUrl: './listpatients.component.html',
})
export class ListPatientsComponent implements OnInit{

  data: Patient[];
  data_copy: any[] = [];
  loading = true;

  constructor(
    private service: PatientsService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      hhcc: {
        title: 'Historia Clinica',
        type: 'number',
      },
      surnames: {
        title: 'Apellidos',
        type: 'string',
      },
      names: {
        title: 'Nombres',
        type: 'string',
      },
      dni: {
        title: 'DNI',
        type: 'string',
      },
      cellphone: {
        title: 'Celular',
        type: 'string',
      },
      patient_type: {
        title: 'Tpo paciente',
        type: 'string',
      },
      emergency_contact: {
        title: 'Contacto Emergencia',
        type: 'string',
      },
      emergency_cellphone: {
        title: 'Celular Contacto Em.',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.service.get().subscribe(
      response => {
        this.data = JSON.parse(JSON.stringify(response));
        this.generateData();
        this.source.load(this.data_copy);
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  generateData() {

    for (const xx of this.data) {
      this.data_copy.push({
        id: xx.id,
        hhcc: xx.HHCC,
        surnames: xx.P_APELLIDO + ' ' + xx.S_APELLIDO,
        names: xx.P_NOMBRE + ' ' + (xx.S_NOMBRE == null ? '' : xx.S_NOMBRE),
        dni: xx.DNI,
        cellphone: xx.CELULAR,
        patient_type: xx.TIPO_PACIENTE,
        emergency_contact: xx.NOMBRE_TUTOR == null ? '' : xx.NOMBRE_TUTOR,
        emergency_cellphone:  xx.CELULAR_TUTOR == null ? '' : xx.CELULAR_TUTOR,
      });
    }
  }

  onAdd(event): void {
    this.router.navigate(['/pages/patients/add']);
  }

  onEdit(event): void {
    const data = event.data;
    const data_real = this.data.find( x => x.id == data.id);
    localStorage.setItem('edit', JSON.stringify(data_real));
    this.router.navigate(['/pages/patients/edit']);
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.remove(data).subscribe(
        success => {
          this.source.remove(data);
          this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          this.showToast(NbToastStatus.SUCCESS, 'Ocurrio un error', '');
        }
      );
    } else {
      return;
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}