import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { PatientsRoutingModule } from './patients-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxSoapModule } from 'ngx-soap';

// components
import { PatientsComponent } from './patients.component';
import { AddPatientsComponent } from './addpatients/addpatients.component';
import { EditPatientsComponent } from './editpatients/editpatients.component';
import { ListPatientsComponent } from './listpatients/listpatients.component';

// service
import { PatientsService } from './patients.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  PatientsComponent,
  AddPatientsComponent,
  EditPatientsComponent,
  ListPatientsComponent
];

const SERVICES = [
  PatientsService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  PatientsRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
  NgxSoapModule,
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class PatientsModule { }
