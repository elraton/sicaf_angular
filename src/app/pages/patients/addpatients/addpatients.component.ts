import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PatientsService } from '../patients.service';
import { Patient } from '../../../models/patient';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { NgxSoapService, Client, ISoapMethodResponse } from 'ngx-soap';
import { HttpClient } from '@angular/common/http';
import { isNumber } from 'util';
import { Agreement } from '../../../models/agreement';
import { AgreementsService } from '../../agreements/agreements.service';

@Component({
  selector: 'ngx-patients-add',
  styleUrls: ['./addpatients.component.scss'],
  templateUrl: './addpatients.component.html',
  providers: [AgreementsService]
})
export class AddPatientsComponent implements OnInit {
  Form: FormGroup;
  data: Patient;
  error = 0;

  statics = new Statics();

  agreementsList: Agreement[] = [];

  patient_types = this.statics.TIPO_PACIENTE;
  rate_types = this.statics.TIPO_TARIFA;
  sedes = this.statics.SEDES;
  districts;
  filtered_districts = [];

  insurance_type = [];

  insurance_list = [];

  client: Client;

  age = 18;
  dnivalid = '';

  constructor(
    private service: PatientsService,
    private toastrService: NbToastrService,
    private agreementService: AgreementsService,
    private location: Location,
    private router: Router,
    private soap: NgxSoapService,
    private http: HttpClient
  ) {
    this.generateForm();
  }

  ngOnInit() {
    this.service.getDistritos().subscribe(
      districts => { 
        this.districts = JSON.parse(JSON.stringify(districts));
        this.filtered_districts = this.districts[2901];
      },
      error => { }
    );
    this.agreementService.get().subscribe(
      data => {
        this.agreementsList = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  soapCall() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', 'http://app26.susalud.gob.pe:27801/path/to/ServiceAcreditacionWs?wsdl', false);
    //xmlhttp.open('POST', 'http://app26.susalud.gob.pe:27801/ServicePasarela', true);

    var sr = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsp="http://www.susalud.gob.pe/acreditacion/WSPasarelaSuSalud/">
    <soapenv:Header>
      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="WSU_ID">
          <wsse:Username>44847251</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">123456jo</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
    </soapenv:Header>
    <soapenv:Body>
       <wsp:getConsultaAsegNomRequest>
          <coExcepcion>0000</coExcepcion>
          <txNombre>270_CON_ASE</txNombre>
          <coIafa>coIafa</coIafa>
          <txPeticion>ISA*00*          *00*          *ZZ*980001C        *ZZ*20001          *171025*1210*|*00501*000000001*0*P*: ~GS*HS*980001C*20001*20171025*121037*1*X*005010~ST*270*0001~BHT*0022*13~HL*1**20*1~NM1*PR*2******PI*20100054184~PRV*OR**CN~HL*2*1*21*1~NM1*1P*2******FI*20001~HL*3*2*22*0~NM1*IL*1*DELGADO*ME****MI*  ***EDUARDO~REF*DD*1~REF*4A* ~REF*PRT* * *ZZ: ~REF*D7* * *  : :: ~REF*D7* **ZZ: ~REF*8X* ~REF*S2* ~REF*ZZ* ~REF*18* **ZZ: ~REF*PRT* ~DTP*447*D8* ~NM1*P5* * * ******** ~REF*DD* **4A: ~SE*5*0001~GE*1*1~IEA*1*000000001~</txPeticion>
       </wsp:getConsultaAsegNomRequest>
    </soapenv:Body>
 </soapenv:Envelope>`;
  }

  generateForm() {
    this.Form = new FormGroup({
      dni: new FormControl('', [Validators.required, Validators.minLength(4)]),
      dni_code: new FormControl('', Validators.required),
      f_name: new FormControl('', Validators.required),
      s_name: new FormControl(''),
      f_surname: new FormControl('', Validators.required),
      s_surname: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      cellphone: new FormControl('', Validators.required),
      phone: new FormControl(''),
      birth_date: new FormControl('', Validators.required),
      district: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      patient_type: new FormControl('', Validators.required),
      tutor_name: new FormControl('', Validators.required),
      tutor_phone: new FormControl('', Validators.required),
      insurance: new FormControl('', Validators.required),
      insurance_type: new FormControl('', Validators.required),
      copago: new FormControl('', Validators.required),
      copago_variable: new FormControl('', Validators.required),
      sede: new FormControl('', Validators.required),
      convenio: new FormControl(0),
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 46 || charCode == 190 || charCode == 110) {
      return true;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57) ) {
      return false;
    }
    return true;

  }

  calculateAge(date) {
    const birthDate = new Date(date);
    const today = new Date();
    let age = today.getFullYear() - birthDate.getFullYear();
    let months = today.getMonth() - birthDate.getMonth();
    if (months < 0 || (months === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    this.age = age;
  }

  updatePatientType() {
    if (this.Form.get('patient_type').value == 'SEGURO') {
      this.insurance_list = this.statics.ASEGURADORAS_SEGURO;
      this.Form.get('copago').setValidators(Validators.required);
      this.Form.get('copago_variable').setValidators(Validators.required);
    } else {
      this.insurance_list = this.statics.ASEGURADORAS_PARTICULAR;
      this.insurance_type = this.statics.TIPO_SEGURO_PARTIULAR;
      this.Form.get('insurance').setValue(this.age < 18 ? 'NIÑO' : 'ADULTO');
      this.Form.get('insurance').disable();
      this.Form.get('insurance_type').setValue('FULLPRICE');
      this.Form.get('insurance_type').disable();
      this.Form.get('copago').setValidators(null);
      this.Form.get('copago_variable').setValidators(null);
    }
    this.Form.get('copago').updateValueAndValidity();
    this.Form.get('copago_variable').updateValueAndValidity();
  }

  updateInsurance() {
    if (this.Form.get('insurance').value == 'POSITIVA') {
      this.insurance_type = this.statics.TIPO_SEGURO_POSITIVA;
    }
    if (this.Form.get('insurance').value == 'RIMAC' || this.Form.get('insurance').value == 'MAPFRE') {
      this.insurance_type = this.statics.TIPO_SEGURO_RIMAC_MAPFRE;
    }
    if (this.Form.get('insurance').value == 'PACIFICO') {
      this.insurance_type = this.statics.TIPO_SEGURO_PACIFICO;
    }
    if (this.Form.get('insurance').value == 'SANNA') {
      this.insurance_type = this.statics.TIPO_SEGURO_SANNA;
    }
  }

  updateInsuranceType() {
    if (this.Form.get('insurance_type').value == 'SOAT') {
      this.Form.get('copago').setValue(0);
      this.Form.get('copago').disable();
      this.Form.get('copago_variable').setValue(100);
      this.Form.get('copago_variable').disable();
    }
  }

  is_dni(godni: string) {

		let wpdb;
		let error = 0;
				
    let i = 0;
    let suma = 0;
    let multiplos = [3,2,7,6,5,4,3,2];
    
    let array_number = [6, 7, 8, 9, 0, 1, 1, 2, 3, 4, 5];
    let array_letters = ['K', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
    
    let numdni = godni.slice(0, 8); // 8 digits
    let dcontrol = godni.slice(8,9); //1 digito
    
    for (const xx of numdni) {
      suma += +xx * multiplos[i];
      i = i + 1;
    }
    
    let key = 11 - (suma%11);
    key = key == 11 ? 0 : key;
    if(array_number[key] != +dcontrol) {
      error++;
      return false;
    } else {
      return true;
    }
		
  }
  
  getStatus( control ) {
    if (control.touched) {
      if ( control.valid ) {
        return 'success'
      } else {
        return 'danger'
      }
    } else {
      return ''
    }
  }

  verifyDNI() {
    this.service.searchDNI({DNI: this.Form.get('dni').value}).subscribe(
      data => {
        const dataarr = JSON.parse(JSON.stringify(data));
        if (dataarr.length > 0) {
          this.dnivalid = 'danger';
          this.error = this.error + 1;
          this.showToast(NbToastStatus.DANGER, 'DNI ya existe', '');
        }
      },
      error => {}
    );
    if (this.Form.get('dni').valid && this.Form.get('dni').touched ) {
      
      

      if (this.Form.get('dni_code').valid && this.Form.get('dni_code').touched ) {
        if ( this.is_dni(this.Form.get('dni').value + this.Form.controls.dni_code.value) ) {
          this.dnivalid = 'success';
        } else {
          this.error = this.error + 1;
          this.dnivalid = 'danger';
        }        
      }
    }
  }

  save() {
    if (this.Form.valid) {
      this.data = {
        id: 0,
        DNI: this.Form.controls.dni.value,
        P_NOMBRE: this.Form.controls.f_name.value,
        S_NOMBRE: this.Form.controls.s_name.value,
        P_APELLIDO: this.Form.controls.f_surname.value,
        S_APELLIDO: this.Form.controls.s_surname.value,
        DIRECCION: this.Form.controls.address.value,
        CELULAR: this.Form.controls.cellphone.value,
        TELEFONO: this.Form.controls.phone.value,
        F_NACIMIENTO: new Date(this.Form.controls.birth_date.value).toISOString().split('T')[0],
        DISTRITO: this.Form.controls.district.value,
        EMAIL: this.Form.controls.email.value,
        TIPO_PACIENTE: this.Form.controls.patient_type.value,
        NOMBRE_TUTOR: this.Form.controls.tutor_name.value,
        CELULAR_TUTOR: this.Form.controls.tutor_phone.value,
        ASEGURADORA: this.Form.controls.insurance.value,
        TIPO_SEGURO: this.Form.controls.insurance_type.value,
        COPAGO_FIJO: this.Form.controls.copago.value,
        COPAGO_VARIABLE: this.Form.controls.copago_variable.value,
        SEDE: this.Form.controls.sede.value,
        F_REGISTRO: new Date().toISOString().split('T')[0],
        HHCC: 0,
        CONVENIO: this.Form.get('convenio').value
      };


      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/patients/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa los datos marcados', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/patients/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}