import { Component, OnInit } from '@angular/core';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { IncomeCash } from '../../../models/incomecash';
import { OutcomeCash } from '../../../models/outcomecash';
import { LocalDataSource } from 'ng2-smart-table';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { DialingService } from '../../dialing/dialing.service';
import { PersonalService } from '../../personal/personal.service';
import { Personal } from '../../../models/personal';

@Component({
  selector: 'ngx-reports-assitance',
  styleUrls: ['./asistencia.component.scss'],
  templateUrl: './asistencia.component.html',
  providers: [DialingService, PersonalService]
})
export class AssistanceComponent implements OnInit {
  personal_list : Personal[] = [];
  personal_parsed : Personal[] = [];
  personal_selected : Personal;
  personal = 0;
  data_parsed = [];
  loading = true;

  dialing_data = [];

  time = 0;
  sede = 'LA PERLA';
  type = 1;
  shift = 1;
  custom_init;
  custom_end;

  constructor(
    private service: DialingService,
    private personalService: PersonalService,
    private router: Router,
    private toastrService: NbToastrService,
  ) { }

  settings = {
    noDataMessage: 'No hay datos',
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: false,
    columns: {
      NOMBRE_APELLIDO: {
        title: 'Personal',
        type: 'string',
      },
      CODIGO: {
        title: 'DNI',
        type: 'string',
      },
      FECHA: {
        title: 'Fecha',
        type: 'string',
      },
      HORA_PERSONAL: {
        title: 'Hora Ingreso',
        type: 'string',
      },
      HORA_REGISTRO: {
        title: 'Hora Registrada',
        type: 'string',
      },
      ESTADO: {
        title: 'Estado',
        type: 'string',
      },
      SEDE: {
        title: 'Sede',
        type: 'string',
      },
      TURNO: {
        title: 'Turno',
        type: 'string',
      },
      ATENCIONES: {
        title: 'Atenciones',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.source.load([]);
    this.loading = false;
    this.personalService.get().subscribe(
      data => {
        this.personal_list = data;
        this.personal_parsed = this.personal_list.filter( x => x.SEDE == this.sede && x.TURNO == 'MAÑANA');
      },
      error => {
        console.log(error);
      }
    );
    this.service.get().subscribe(
      data => {
        this.dialing_data = data;
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  changeSede(){
    const shift = this.shift == 1 ? 'MAÑANA' : 'TARDE'
    this.personal_parsed = this.personal_list.filter(x => x.SEDE == this.sede && x.TURNO == shift);
    this.personal = 0;
    this.filterData();
  }

  customDateInit(event) {
    this.custom_init = new Date(event);
    if (this.custom_end != null) {
      this.filterData();
    } else {
      this.showToast(NbToastStatus.WARNING, 'Debe ingresar la fecha de fin', '')
    }
  }
  customDateEnd(event) {
    this.custom_end = new Date(event);
    if (this.custom_init != null) {
      this.filterData();
    } else {
      this.showToast(NbToastStatus.WARNING, 'Debe ingresar la fecha de inicio', '')
    }
  }

  changePersonal(){
    this.personal_selected = this.personal_list.find( x => x.id == +this.personal);
    this.filterData();
  }

  filterData() {
    this.data_parsed = [];
    this.loading = true;

    const shift = this.shift == 1 ? 'MAÑANA' : 'TARDE';

    if (+this.time > 0) {
      switch (+this.time) {
        case 1: {
          const today = new Date().toISOString().split('T')[0];
          if (this.type == 1) {
            if (this.personal == 0) {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(today) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO');              
            } else {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(today) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO' && x.CODIGO == this.personal_selected.DNI);
            }
          } else {
            if (this.personal == 0) {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(today) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA');
            } else {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(today) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA' && x.CODIGO == this.personal_selected.DNI);
            }
          }
          break;
        }
        case 2: {
          const today = new Date();
          const yesterday = new Date(today.setDate(today.getDate() - 1)).toISOString().split('T')[0];
          if (this.type == 1) {
            if (this.personal == 0) {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(yesterday) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO');
            } else {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(yesterday) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO' && x.CODIGO == this.personal_selected.DNI);
            }
          } else {
            if (this.personal == 0) {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(yesterday) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA');
            } else {
              this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(yesterday) && x.SEDE == this.sede && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA' && x.CODIGO == this.personal_selected.DNI);
            }
          }
          break;
        }
        case 3: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth(), 1);
          const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 1);
          if (this.type == 1) {
            if (this.personal == 0) {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO') {
                    this.data_parsed.push(x);
                  }
                }
              });
            } else {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO' && x.CODIGO == this.personal_selected.DNI) {
                    this.data_parsed.push(x);
                  }
                }
              });
            }
          } else {
            if (this.personal == 0) {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA') {
                    this.data_parsed.push(x);
                  }
                }
              });
            } else {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA' && x.CODIGO == this.personal_selected.DNI) {
                    this.data_parsed.push(x);
                  }
                }
              });
            }
          }
          break;
        }
        case 4: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth() - 1, 1);
          const last_day = new Date(today.getFullYear(), today.getMonth(), 1);
          if (this.type == 1) {
            if (this.personal == 0) {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO') {
                    this.data_parsed.push(x);
                  }
                }
              });
            } else {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO' && x.CODIGO == this.personal_selected.DNI) {
                    this.data_parsed.push(x);
                  }
                }
              });
            }
          } else {
            if (this.personal == 0) {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA') {
                    this.data_parsed.push(x);
                  }
                }
              });
            } else {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > first_day && data_date < last_day) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA' && x.CODIGO == this.personal_selected.DNI) {
                    this.data_parsed.push(x);
                  }
                }
              });
            }
          }
          break;
        }
        case 5: {
          if (this.type == 1) {
            if (this.personal == 0) {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > this.custom_init && data_date < this.custom_end) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO') {
                    this.data_parsed.push(x);
                  }
                }
              });
            } else {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > this.custom_init && data_date < this.custom_end) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'INGRESO' && x.CODIGO == this.personal_selected.DNI) {
                    this.data_parsed.push(x);
                  }
                }
              });
            }
          } else {
            if (this.personal == 0) {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > this.custom_init && data_date < this.custom_end) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA') {
                    this.data_parsed.push(x);
                  }
                }
              });
            } else {
              this.dialing_data.map(x => {
                const data_date = new Date(x.FECHA);
                if (data_date > this.custom_init && data_date < this.custom_end) {
                  if (this.sede == x.SEDE && x.TURNO == shift && x.TIPO_REGISTRO == 'SALIDA' && x.CODIGO == this.personal_selected.DNI) {
                    this.data_parsed.push(x);
                  }
                }
              });
            }
          }
          break;
        }
      }


    }    

    this.source.load(this.data_parsed);
    this.loading = false;
  }

  export() {
    this.source.getAll().then(
      data => {
        const table_2_parse = data;
        const table_2_export = [];

        table_2_parse.map(x => {
          table_2_export.push({
            "Personal": x.NOMBRE_APELLIDO,
            "DNI": x.CODIGO,
            "Fecha": x.FECHA,
            "Hora de Ingreso": x.HORA_PERSONAL,
            "Hora Registrada": x.HORA_REGISTRO,
            "Estado": x.ESTADO,
            "Sede": x.SEDE,
            "Turno": x.TURNO,
            "Num de Atenciones": x.ATENCIONES
          });
        });

        if ( this.type == 1) {
          this.exportAsExcelFile(table_2_export, 'Reporte_Asistencia_Entrada_' + new Date().toISOString().split('T')[0]);
        } else {
          this.exportAsExcelFile(table_2_export, 'Reporte_Asistencia_Salidas_' + new Date().toISOString().split('T')[0]);
        }
      }
    );
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + '.xlsx');
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}