import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../reports.service';
import { Personal_Category } from '../../../models/personal_category';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Rate } from '../../../models/rate';
import { IncomecashService } from '../../incomecash/incomecash.service';
import { IncomeCash } from '../../../models/incomecash';
import { OutcomecashService } from '../../outcomecash/outcomecash.service';
import { OutcomeCash } from '../../../models/outcomecash';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

@Component({
  selector: 'ngx-reports-cash',
  styleUrls: ['./caja.component.scss'],
  templateUrl: './caja.component.html',
  providers: [IncomecashService, OutcomecashService]
})
export class CashComponent implements OnInit{

  data_income: IncomeCash[];
  data_outcome: OutcomeCash[];
  data_parsed = [];
  loading = true;

  time = 0;
  sede = 'LA PERLA';
  type = 1;
  custom_init;
  custom_end;

  constructor(
    private serviceIncome: IncomecashService,
    private serviceOutcome: OutcomecashService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings_income = {
    noDataMessage: 'No hay datos',
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: false,
    columns: {
      NOMBRE_APELLIDO: {
        title: 'Paciente',
        type: 'string',
      },
      DNI: {
        title: 'DNI',
        type: 'string',
      },
      FECHA: {
        title: 'Fecha',
        type: 'string',
      },
      TIPO_PACIENTE: {
        title: 'Tipo de paciente',
        type: 'string',
      },
      TIPO_SERVICIO: {
        title: 'Servicio',
        type: 'string',
      },
      CANTIDAD: {
        title: 'Nmro Sesiones',
        type: 'string',
      },
      FORMA_PAGO: {
        title: 'Forma de pago',
        type: 'string',
      },
      TIPO_COMPROBANTE: {
        title: 'Tipo Comprobante',
        type: 'string',
      },
      TOTAL: {
        title: 'Total',
        type: 'string',
      },
      SEDE: {
        title: 'Sede',
        type: 'string',
      },
      TURNO: {
        title: 'Turno',
        type: 'string',
      }
    },
  };


  settings_outcome = {
    noDataMessage: 'No hay datos',
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: false,
    columns: {
      DESCRIPCION: {
        title: 'Descripcion',
        type: 'string',
      },
      FECHA: {
        title: 'Fecha',
        type: 'string',
      },
      IMPORTE: {
        title: 'Importe',
        type: 'string',
      },
      EMISOR: {
        title: 'Emisor',
        type: 'string',
      },
      RECEPTOR: {
        title: 'Receptor',
        type: 'string',
      },
      SEDE: {
        title: 'Sede',
        type: 'string',
      },
      TURNO: {
        title: 'Turno',
        type: 'string',
      },
    },
  };
  settings_sells = {};

  settings;

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.settings = JSON.parse(JSON.stringify(this.settings_income));
    this.source.load([]);
    this.loading = false;
    this.serviceIncome.get().subscribe(
      data => {
        this.data_income = data;
        console.log(this.data_income);
      },
      error => {
        console.log(error);
      }
    );

    this.serviceOutcome.get().subscribe(
      data => {
        this.data_outcome = data;
      },
      error => {
        console.log(error);
      }
    );
  }

  customDateInit(event) {
    this.custom_init = new Date(event);
    if ( this.custom_end != null) {
      this.filterData();
    } else {
      this.showToast(NbToastStatus.WARNING, 'Debe ingresar la fecha de fin', '')
    }
  }
  customDateEnd(event) {
    this.custom_end = new Date(event);
    if (this.custom_init != null) {
      this.filterData();
    } else {
      this.showToast(NbToastStatus.WARNING, 'Debe ingresar la fecha de inicio', '')
    }
  }

  filterData() {
    this.data_parsed = [];
    this.loading = true;
    if ( +this.type == 1 ) {
      this.settings = JSON.parse(JSON.stringify(this.settings_income));
      // es ingresos
      if ( +this.time > 0 ) {
        switch(+this.time) {
          case 1: {
            const today = new Date().toISOString().split('T')[0];
            this.data_parsed = this.data_income.filter( x => x.FECHA.includes(today) && x.SEDE == this.sede );
            break;
          }
          case 2: {
            const today = new Date();
            const yesterday = new Date(today.setDate( today.getDate() - 1 )).toISOString().split('T')[0];
            this.data_parsed = this.data_income.filter(x => x.FECHA.includes(yesterday) && x.SEDE == this.sede );
            break;
          }
          case 3: {
            const today = new Date();
            const first_day = new Date( today.getFullYear(), today.getMonth(), 1 );
            const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 1);
            this.data_income.map( x => {
              const data_date = new Date(x.FECHA);
              if ( data_date > first_day && data_date < last_day ) {
                if ( this.sede == x.SEDE ){
                  this.data_parsed.push(x);
                }
              }
            });
            break;
          }
          case 4: {
            const today = new Date();
            const first_day = new Date(today.getFullYear(), today.getMonth() - 1, 1);
            const last_day = new Date(today.getFullYear(), today.getMonth(), 1);
            this.data_income.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > first_day && data_date < last_day) {
                if (this.sede == x.SEDE) {
                  this.data_parsed.push(x);
                }
              }
            });
            break;
          }
          case 5: {
            this.data_income.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > this.custom_init && data_date < this.custom_end) {
                if (this.sede == x.SEDE) {
                  this.data_parsed.push(x);
                }
              }
            });
            break;
          }
        }
      }
    }

    if (+this.type == 2) {
      this.settings = JSON.parse(JSON.stringify(this.settings_outcome));
      if (+this.time > 0) {
        switch (+this.time) {
          case 1: {
            const today = new Date().toISOString().split('T')[0];
            this.data_parsed = this.data_outcome.filter(x => x.FECHA.includes(today) && x.SEDE == this.sede);
            break;
          }
          case 2: {
            const today = new Date();
            const yesterday = new Date(today.setDate(today.getDate() - 1)).toISOString().split('T')[0];
            this.data_parsed = this.data_outcome.filter(x => x.FECHA.includes(yesterday) && x.SEDE == this.sede);
            break;
          }
          case 3: {
            const today = new Date();
            const first_day = new Date(today.getFullYear(), today.getMonth(), 1);
            const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 1);
            this.data_outcome.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > first_day && data_date < last_day) {
                if (this.sede == x.SEDE) {
                  this.data_parsed.push(x);
                }
              }
            });
            break;
          }
          case 4: {
            const today = new Date();
            const first_day = new Date(today.getFullYear(), today.getMonth() - 1, 1);
            const last_day = new Date(today.getFullYear(), today.getMonth(), 1);
            this.data_outcome.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > first_day && data_date < last_day) {
                if (this.sede == x.SEDE) {
                  this.data_parsed.push(x);
                }
              }
            });
            break;
          }
          case 5: {
            this.data_outcome.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > this.custom_init && data_date < this.custom_end) {
                if (this.sede == x.SEDE) {
                  this.data_parsed.push(x);
                }
              }
            });
            break;
          }
        }
      }
    }

    this.source.load(this.data_parsed);
    this.loading = false;
  }

  onAdd(event): void {
    this.router.navigate(['/pages/services/rates/add']);
  }

  onEdit(event): void {
    const data = event.data;
    localStorage.setItem('edit', JSON.stringify(data));
    this.router.navigate(['/pages/services/rates/edit']);
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if (window.confirm('Are you sure you want to delete?')) {
    } else {
      return;
    }
  }

  export() {
    this.source.getAll().then(
      data => {
        const table_2_parse = data;
        const table_2_export = [];

        if (+this.type == 1) {
          table_2_parse.map(x => {
            table_2_export.push({
              "Paciente": x.NOMBRE_APELLIDO,
              "DNI": x.DNI,
              "Fecha": x.FECHA,
              "Tipo de paciente": x.TIPO_PACIENTE,
              "Servicio": x.TIPO_SERVICIO,
              "Nmro de sesiones": x.CANTIDAD,
              "Forma de pago": x.FORMA_PAGO,
              "Comprobante": x.TIPO_COMPROBANTE,
              "Precio unitario": x.PRECIO_UNIDAD,
              "IGV": x.IGV,
              "Subtotal": x.SUBTOTAL,
              "Total": x.TOTAL,
              "Total Pagado": x.TOTAL_PAGADO,
              "Receptor": x.RECEPTOR,
              "Sede": x.SEDE,
              "Turno": x.TURNO
            });
          });
          this.exportAsExcelFile(table_2_export, 'Reporte_Ingresos_' + new Date().toISOString().split('T')[0]);
        }

        if (+this.type == 2) {
          table_2_parse.map(x => {
            table_2_export.push({
              "Descripcion": x.DESCRIPCION,
              "Fecha": x.FECHA,
              "Importe": x.IMPORTE,
              "Emisor": x.EMISOR,
              "Receptor": x.RECEPTOR,
              "Sede": x.SEDE,
              "Turno": x.TURNO
            });
          });

          this.exportAsExcelFile(table_2_export, 'Reporte_Egresos_' + new Date().toISOString().split('T')[0]);

        }

        
      }
    );
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + '.xlsx');
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}