import { Component, OnInit } from '@angular/core';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';
import { IncomeCash } from '../../../models/incomecash';
import { OutcomeCash } from '../../../models/outcomecash';
import { LocalDataSource } from 'ng2-smart-table';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { DialingService } from '../../dialing/dialing.service';
import { PersonalService } from '../../personal/personal.service';
import { Personal } from '../../../models/personal';
import { DailyattentionService } from '../../dailyattention/dailyattention.service';

@Component({
  selector: 'ngx-reports-attentions',
  styleUrls: ['./atencion.component.scss'],
  templateUrl: './atencion.component.html',
  providers: [DailyattentionService, PersonalService]
})
export class AttentionsComponent {
  personal_list: Personal[] = [];
  personal_parsed: Personal[] = [];
  personal_selected: Personal;
  personal = 0;
  data_parsed = [];
  loading = true;

  dialing_data = [];

  time = 0;
  sede = 'LA PERLA';
  type = 1;
  shift = 1;
  custom_init;
  custom_end;

  constructor(
    private service: DailyattentionService,
    private personalService: PersonalService,
    private router: Router,
    private toastrService: NbToastrService,
  ) { }

  settings = {
    noDataMessage: 'No hay datos',
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    actions: false,
    columns: {
      TERAPEUTA: {
        title: 'Terapeuta',
        type: 'string',
      },
      FECHA: {
        title: 'Fecha',
        type: 'string',
      },
      TERAPIA: {
        title: 'Terapia',
        type: 'string',
      },
      PACIENTE: {
        title: 'Paciente',
        type: 'string',
      },
      SEGURO: {
        title: 'Seguro',
        type: 'string',
      },
      ASEGURADORA: {
        title: 'Aseguradora',
        type: 'string',
      },
      TIPO_SEGURO: {
        title: 'Tipo de seguro',
        type: 'string',
      },
      
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.source.load([]);
    this.loading = false;
    this.personalService.get().subscribe(
      data => {
        this.personal_list = data;
        this.personal_parsed = this.personal_list.filter(x => x.SEDE == this.sede);
      },
      error => {
        console.log(error);
      }
    );
    this.service.get().subscribe(
      data => {
        this.dialing_data = data;
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }

  changeSede() {
    this.personal_parsed = this.personal_list.filter(x => x.SEDE == this.sede);
    this.personal = 0;
    this.filterData();
  }

  customDateInit(event) {
    this.custom_init = new Date(event);
    if (this.custom_end != null) {
      this.filterData();
    } else {
      this.showToast(NbToastStatus.WARNING, 'Debe ingresar la fecha de fin', '')
    }
  }
  customDateEnd(event) {
    this.custom_end = new Date(event);
    if (this.custom_init != null) {
      this.filterData();
    } else {
      this.showToast(NbToastStatus.WARNING, 'Debe ingresar la fecha de inicio', '')
    }
  }

  changePersonal() {
    this.personal_selected = this.personal_list.find(x => x.id == +this.personal);
    this.filterData();
  }

  filterData() {
    this.data_parsed = [];
    this.loading = true;

    if (+this.time > 0) {
      switch (+this.time) {
        case 1: {
          const today = new Date().toISOString().split('T')[0];
          if (this.personal > 0) {
            const name = this.personal_selected.NOMBRE + ' ' + this.personal_selected.APELLIDO;
            this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(today) && x.TERAPEUTA == name);
          }
          break;
        }
        case 2: {
          const today = new Date();
          const yesterday = new Date(today.setDate(today.getDate() - 1)).toISOString().split('T')[0];
          if (this.personal > 0) {
            const name = this.personal_selected.NOMBRE + ' ' + this.personal_selected.APELLIDO;
            this.data_parsed = this.dialing_data.filter(x => x.FECHA.includes(yesterday) && x.TERAPEUTA == name);
          }
          break;
        }
        case 3: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth(), 1);
          const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 1);
          if (this.personal > 0) {
            const name = this.personal_selected.NOMBRE + ' ' + this.personal_selected.APELLIDO;
            this.dialing_data.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > first_day && data_date < last_day) {
                if (x.TERAPEUTA == name) {
                  this.data_parsed.push(x);
                }
              }
            });
          }
          break;
        }
        case 4: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth() - 1, 1);
          const last_day = new Date(today.getFullYear(), today.getMonth(), 1);
          if (this.personal > 0) {
            const name = this.personal_selected.NOMBRE + ' ' + this.personal_selected.APELLIDO;
            this.dialing_data.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > first_day && data_date < last_day) {
                if (x.TERAPEUTA == name) {
                  this.data_parsed.push(x);
                }
              }
            });
          }
          break;
        }
        case 5: {
          if (this.personal > 0) {
            const name = this.personal_selected.NOMBRE + ' ' + this.personal_selected.APELLIDO;
            this.dialing_data.map(x => {
              const data_date = new Date(x.FECHA);
              if (data_date > this.custom_init && data_date < this.custom_end) {
                if (x.TERAPEUTA == name) {
                  this.data_parsed.push(x);
                }
              }
            });
          }
          break;
        }
      }


    }

    const data2parse = [];
    this.data_parsed.map( x => {
      let seguro = '';
      if ( x.ASEGURADORA == 'ADULTO' || x.ASEGURADORA == 'NIÑO' ) {
        seguro = 'PARTICULAR';
      } else {
        seguro = 'SEGURO'
      }
      data2parse.push({
        SEGURO: seguro,
        ASEGURADORA: x.ASEGURADORA,
        FECHA: x.FECHA,
        PACIENTE: x.NOMBRE_APELLIDO,
        TERAPEUTA: x.TERAPEUTA,
        TIPO_SEGURO: x.TIPO_SEGURO,
        TERAPIA: x.TERAPIAS
      });
    });

    this.data_parsed = data2parse;

    this.source.load(this.data_parsed);
    this.loading = false;
  }

  export() {
    this.source.getAll().then(
      data => {
        const table_2_parse = data;
        const table_2_export = [];

        table_2_parse.map(x => {
          table_2_export.push({
            "Terapeuta": x.TERAPEUTA,
            "Fecha": x.FECHA,
            "Sesion o Terapia": x.TERAPIA,
            "Paciente": x.PACIENTE,
            "Seguro": x.SEGURO,
            "Aseguradora": x.ASEGURADORA,
            "Tipo de seguro": x.TIPO_SEGURO
          });
        });

        this.exportAsExcelFile(table_2_export, 'Reporte_Atenciones_' + this.personal_selected.NOMBRE + '_' + new Date().toISOString().split('T')[0]);
      }
    );
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + '.xlsx');
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}