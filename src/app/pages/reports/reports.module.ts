import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { ReportsRoutingModule } from './reports-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { ReportsComponent } from './reports.component';
import { AssistanceComponent } from './asistencia/asistencia.component';
import { AttentionsComponent } from './atencion/atencion.component';
import { CashComponent } from './caja/caja.component';

// service
import { ReportsService } from './reports.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  ReportsComponent,
  AssistanceComponent,
  AttentionsComponent,
  CashComponent
];

const SERVICES = [
  ReportsService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  ReportsRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class ReportsModule { }
