import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportsComponent } from './reports.component';
import { AssistanceComponent } from './asistencia/asistencia.component';
import { AttentionsComponent } from './atencion/atencion.component';
import { CashComponent } from './caja/caja.component';

const routes: Routes = [{
  path: '',
  component: ReportsComponent,
  children: [
    {
      path: 'assistance',
      component: AssistanceComponent,
    },
    {
      path: 'atentions',
      component: AttentionsComponent,
    },
    {
      path: 'cash',
      component: CashComponent,
    },
    {
      path: '',
      redirectTo: 'assistance',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule {
}
