import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Personal',
    icon: 'fas fa-user-md',
    children: [
      {
        title: 'Listar Personal',
        link: '/pages/personal/list',
      },
      {
        title: 'Agregar nuevo',
        link: '/pages/personal/add',
      }
    ]
  },
  {
    title: 'Marcacion',
    icon: 'far fa-clock',
    children: [
      {
        title: 'Ingreso',
        link: '/pages/assistance/in',
      },
      {
        title: 'Salida',
        link: '/pages/assistance/out',
      },
    ]
  },
  {
    title: 'Caja',
    icon: 'fas fa-dollar-sign',
    children: [
      {
        title: 'Ingreso Caja',
        icon: 'fas fa-sign-in-alt',
        children: [
          {
            title: 'Listar ingresos',
            link: '/pages/cash/income/list',
          },
          {
            title: 'Agregar ingresos',
            link: '/pages/cash/income/add',
          },
        ]
      },
      {
        title: 'Egreso Caja',
        icon: 'fas fa-sign-out-alt',
        children: [
          {
            title: 'Listar egresos',
            link: '/pages/cash/outcome/list',
          },
          {
            title: 'Agregar egreso',
            link: '/pages/cash/outcome/add',
          },
        ]
      },
      /*{
        title: 'Ventas',
        icon: 'fas fa-user-md',
        children: [
          {
            title: 'Listar ventas',
            link: '/pages/cash/sales/list',
          },
          {
            title: 'Agregar ventas',
            link: '/pages/cash/sales/add',
          },
        ]
      },*/
    ]
  },
  {
    title: 'Pacientes',
    icon: 'fas fa-users',
    children: [
      {
        title: 'Listar pacientes',
        link: '/pages/patients/list',
      },
      {
        title: 'Agregar paciente',
        link: '/pages/patients/add',
      },
    ]
  },
  {
    title: 'Tratamientos',
    icon: 'fas fa-file-medical',
    children: [
      {
        title: 'Listar tratamientos',
        link: '/pages/treatments/list',
      },
      {
        title: 'Agregar tratamiento',
        link: '/pages/treatments/add',
      },
    ]
  },
  {
    title: 'Atención diaria',
    icon: 'far fa-check-square',
    children: [
      {
        title: 'Listar atenciones',
        link: '/pages/attentions/list',
      },
      {
        title: 'Agregar atencion',
        link: '/pages/attentions/add',
      },
    ]
  },
  {
    title: 'Horario',
    icon: 'far fa-calendar-alt',
    link: '/pages/schedule',
  },
];
