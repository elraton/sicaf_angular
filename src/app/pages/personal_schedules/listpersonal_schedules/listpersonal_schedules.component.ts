import { Component, OnInit } from '@angular/core';
import { Personal_SchedulesService } from '../personal_schedules.service';
import { Personal_Schedule } from '../../../models/personal_schedule';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-personal_schedules-list',
  styleUrls: ['./listpersonal_schedules.component.scss'],
  templateUrl: './listpersonal_schedules.component.html',
})
export class ListPersonal_SchedulesComponent implements OnInit{

  data: Personal_Schedule[];

  loading = true;

  constructor(
    private service: Personal_SchedulesService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      NOMBRE: {
        title: 'Nombre',
        type: 'string',
      },
      HORA_INICIO: {
        title: 'Hora de inicio',
        type: 'string',
      },
      HORA_FIN: {
        title: 'Hora de fin',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.service.get().subscribe(
      response => {
        this.data = JSON.parse(JSON.stringify(response));
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  onAdd(event): void {
    this.router.navigate(['/pages/personal/schedules/add']);
  }

  onEdit(event): void {
    const data = event.data;
    localStorage.setItem('edit', JSON.stringify(data));
    this.router.navigate(['/pages/personal/schedules/edit']);
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if (window.confirm('Are you sure you want to delete?')) {
      this.service.remove(data).subscribe(
        success => {
          this.source.remove(data);
          this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          this.showToast(NbToastStatus.SUCCESS, 'Ocurrio un error', '');
        }
      );
    } else {
      return;
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}