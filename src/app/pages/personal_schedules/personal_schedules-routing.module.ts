import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Personal_SchedulesComponent } from './personal_schedules.component';
import { AddPersonal_SchedulesComponent } from './addpersonal_schedules/addpersonal_schedules.component';
import { EditPersonal_SchedulesComponent } from './editpersonal_schedules/editpersonal_schedules.component';
import { ListPersonal_SchedulesComponent } from './listpersonal_schedules/listpersonal_schedules.component';

const routes: Routes = [{
  path: '',
  component: Personal_SchedulesComponent,
  children: [
    {
      path: 'list',
      component: ListPersonal_SchedulesComponent,
    },
    {
      path: 'add',
      component: AddPersonal_SchedulesComponent,
    },
    {
      path: 'edit',
      component: EditPersonal_SchedulesComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Personal_SchedulesRoutingModule {
}
