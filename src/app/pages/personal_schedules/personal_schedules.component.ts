import { Component } from '@angular/core';

@Component({
  selector: 'ngx-personal_schedules',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class Personal_SchedulesComponent {
}
