import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Personal_SchedulesService } from '../personal_schedules.service';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Personal_Schedule } from '../../../models/personal_schedule';
import { Statics } from '../../../models/statics';

@Component({
  selector: 'ngx-personal_schedules-add',
  styleUrls: ['./addpersonal_schedules.component.scss'],
  templateUrl: './addpersonal_schedules.component.html',
})
export class AddPersonal_SchedulesComponent {
  Form: FormGroup;
  data: Personal_Schedule;

  hours = new Statics().HOURS;
  minutes = new Statics().MINUTES;
  sedes = new Statics().SEDES;
  days = new Statics().DAYS;
  durations = new Statics().DURATIONS;

  constructor(
    private service: Personal_SchedulesService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
  }

  generateForm() {
    this.Form = new FormGroup({
      day: new FormControl('lu', Validators.required),
      duration: new FormControl('15', Validators.required),
      start_hour_h: new FormControl(7, Validators.required),
      start_hour_m: new FormControl(0, Validators.required),
      start_hour_t: new FormControl('am', Validators.required),
      end_hour_h: new FormControl(7, Validators.required),
      end_hour_m: new FormControl(0, Validators.required),
      end_hour_t: new FormControl('am', Validators.required),
      name: new FormControl('', Validators.required),
    });
  }

  save() {
    if (this.Form.valid) {
      let start_hour = this.Form.controls.start_hour_h.value;
      let end_hour = this.Form.controls.end_hour_h.value;
      let start_min = this.Form.controls.start_hour_m.value;
      let end_min = this.Form.controls.end_hour_m.value;

      start_min = start_min == 0 ? '00' : '30';
      end_min = end_min == 0 ? '00' : '30';

      if (this.Form.controls.start_hour_t.value == 'pm') {
        start_hour = Number(start_hour) + 12;
      }
      if (this.Form.controls.end_hour_t.value == 'pm') {
        end_hour = Number(end_hour) + 12;
      }
      this.data = {
        id: 0,
        DIA: this.Form.controls.day.value,
        TIEMPO_ATENCION: this.Form.controls.duration.value,
        HORA_INICIO: start_hour + ':' + start_min + ':00',
        HORA_FIN: end_hour + ':' + end_min + ':00',
        NOMBRE: this.Form.controls.name.value
      };

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/personal/schedules/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    }            
  }

  cancel() {
    this.router.navigate(['/pages/personal/schedules/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}