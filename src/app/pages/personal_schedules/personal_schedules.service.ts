import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { Personal_Schedule } from '../../models/personal_schedule';

@Injectable()
export class Personal_SchedulesService {
    
    constructor(private http: HttpClient) {}

    get(): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.get(url + 'personal/schedule/list ');
    }
    
    save(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/schedule/add ', data);
    }

    update(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/schedule/edit ', data);
    }

    remove(data): Observable<any> {
        const url = localStorage.getItem('baseurl');
        return this.http.post(url + 'personal/schedule/remove', data);
    }
}