import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { Personal_SchedulesRoutingModule } from './personal_schedules-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { Personal_SchedulesComponent } from './personal_schedules.component';
import { AddPersonal_SchedulesComponent } from './addpersonal_schedules/addpersonal_schedules.component';
import { EditPersonal_SchedulesComponent } from './editpersonal_schedules/editpersonal_schedules.component';
import { ListPersonal_SchedulesComponent } from './listpersonal_schedules/listpersonal_schedules.component';

// service
import { Personal_SchedulesService } from './personal_schedules.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  Personal_SchedulesComponent,
  AddPersonal_SchedulesComponent,
  EditPersonal_SchedulesComponent,
  ListPersonal_SchedulesComponent
];

const SERVICES = [
  Personal_SchedulesService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  Personal_SchedulesRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class Personal_SchedulesModule { }
