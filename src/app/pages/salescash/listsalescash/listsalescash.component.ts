import { Component, OnInit } from '@angular/core';
import { SalescashService } from '../salescash.service';
import { Personal_Category } from '../../../models/personal_category';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-salescash-list',
  styleUrls: ['./listsalescash.component.scss'],
  templateUrl: './listsalescash.component.html',
})
export class ListSalescashComponent implements OnInit{

  data: Personal_Category[];

  loading = true;

  constructor(
    private service: SalescashService,
    private router: Router,
    private toastrService: NbToastrService,
  ) {}

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      FECHA: {
        title: 'Fecha',
        type: 'string'
      },
      IMPORTE: {
        title: 'Importe',
        type: 'number'
      },
      DESCRIPCION: {
        title: 'Descripción',
        type: 'string'
      },
      EMISOR: {
        title: 'Emisor',
        type: 'string'
      },
      RECEPTOR: {
        title: 'Receptor',
        type: 'string'
      },
      SEDE: {
        title: 'Sede',
        type: 'string'
      },
      TURNO: {
        title: 'Turno',
        type: 'string'
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  ngOnInit() {
    this.service.get().subscribe(
      response => {
        this.data = JSON.parse(JSON.stringify(response));
        this.source.load(this.data);
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  onAdd(event): void {
    this.router.navigate(['/pages/cash/outcome/add']);
  }

  onEdit(event): void {
    const data = event.data;
    localStorage.setItem('edit', JSON.stringify(data));
    this.router.navigate(['/pages/cash/outcome/edit']);
    return;
  }

  onDeleteConfirm(event): void {
    const data = event.data;
    if (window.confirm('No se pueden eliminar egresos')) {
      /*this.service.remove(data).subscribe(
        success => {
          this.source.remove(data);
          this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          this.showToast(NbToastStatus.SUCCESS, 'Ocurrio un error', '');
        }
      );*/
      return;
    } else {
      return;
    }
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}