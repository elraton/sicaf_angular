import { NgModule } from '@angular/core';

import { TreeModule } from 'angular-tree-component';
import { ToasterModule } from 'angular2-toaster';

import { ThemeModule } from '../../@theme/theme.module';
import { SalescashRoutingModule } from './salescash-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// components
import { SalescashComponent } from './salescash.component';
import { AddSalescashComponent } from './addsalescash/addsalescash.component';
import { EditSalescashComponent } from './editsalescash/editsalescash.component';
import { ListSalescashComponent } from './listsalescash/listsalescash.component';

// service
import { SalescashService } from './salescash.service';
import { TokenInterceptor } from '../token.interceptor';
import { Ng2SmartTableModule } from 'ng2-smart-table';

const COMPONENTS = [
  SalescashComponent,
  AddSalescashComponent,
  EditSalescashComponent,
  ListSalescashComponent
];

const SERVICES = [
  SalescashService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

const MODULES = [
  ThemeModule,
  SalescashRoutingModule,
  TreeModule,
  HttpClientModule,
  Ng2SmartTableModule,
  ToasterModule.forRoot(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class SalescashModule { }
