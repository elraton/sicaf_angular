import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SalescashService } from '../salescash.service';
import { SalesCash } from '../../../models/salescash';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Statics } from '../../../models/statics';

@Component({
  selector: 'ngx-salescash-add',
  styleUrls: ['./addsalescash.component.scss'],
  templateUrl: './addsalescash.component.html',
})
export class AddSalescashComponent {
  Form: FormGroup;
  data: SalesCash;
  user;

  patient_types = new Statics().TIPO_PERSONA;
  rate_types = new Statics().TIPO_TARIFA;
  status = new Statics().STATUS;

  item_list_parsed = [];
  marca_list_parsed = [];
  tipo_list_parsed = [];
  modelo_list_parsed = [];

  item_list = [];
  marca_list = [];
  tipo_list = [];
  modelo_list = [];

  constructor(
    private service: SalescashService,
    private toastrService: NbToastrService,
    private location: Location,
    private router: Router
  ) {
    this.generateForm();
    this.service.getUser().subscribe(
      data => {
        this.user = JSON.parse(JSON.stringify(data)).success;
      },
      error => {}
    )
  }

  generateForm() {
    this.Form = new FormGroup({
      ITEM: new FormControl('', Validators.required),
      MARCA: new FormControl('', Validators.required),
      TIPO: new FormControl('', Validators.required),
      MODELO: new FormControl('', Validators.required),
      CANTIDAD: new FormControl('', Validators.required),
      PRECIO_UNIDAD: new FormControl('', Validators.required),
      SUBTOTAL: new FormControl('', Validators.required),
      IGV: new FormControl('', Validators.required),
      TOTAL: new FormControl('', Validators.required),
      PERSONAL_FISIOVIDA: new FormControl('', Validators.required),
    });
  }

  onlynumbers(event) {
    if ( event.keyCode == 46 || event.keyCode == 8 ) {
      // let it happen, don't do anything
    } else {
      // Ensure that it is a number and stop the keypress
      if (event.keyCode < 48 || event.keyCode > 57 ) {
          event.preventDefault(); 
      }
    }
  }

  save() {
    if (this.Form.valid) {
      const today = new Date();
      this.data;

      this.service.save(this.data)
      .subscribe(
        res => {
          this.showToast(NbToastStatus.SUCCESS, 'Se guardo correctamente', '');
          this.router.navigate(['/pages/cash/outcome/list']);
        },
        error => {
          this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      this.showToast(NbToastStatus.DANGER, 'Completa todos los datos', '');
    }
  }

  cancel() {
    this.router.navigate(['/pages/cash/outcome/list']);
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 2000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? title : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}