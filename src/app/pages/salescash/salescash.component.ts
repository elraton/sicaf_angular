import { Component } from '@angular/core';

@Component({
  selector: 'ngx-salescash',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class SalescashComponent {
}
