import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalescashComponent } from './salescash.component';
import { AddSalescashComponent } from './addsalescash/addsalescash.component';
import { EditSalescashComponent } from './editsalescash/editsalescash.component';
import { ListSalescashComponent } from './listsalescash/listsalescash.component';

const routes: Routes = [{
  path: '',
  component: SalescashComponent,
  children: [
    {
      path: 'list',
      component: ListSalescashComponent,
    },
    {
      path: 'add',
      component: AddSalescashComponent,
    },
    {
      path: 'edit',
      component: EditSalescashComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalescashRoutingModule {
}
