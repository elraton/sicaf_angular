export interface OutcomeCash {
    id: number;
    FECHA: string;
    SEDE: string;
    EMISOR: string;
    RECEPTOR: string;
    IMPORTE: number;
    TURNO: string;
    DESCRIPCION: string;
}
