export interface Dialing {
    id: number;
    FECHA: string;
    CODIGO: string;
    NOMBRE_APELLIDO: string;
    TIPO_REGISTRO: string;
    TURNO: string;
    SEDE: string;
    HORA_PERSONAL: string;
    HORA_REGISTRO: string;
    ESTADO: string;
    ATENCIONES: number;
}