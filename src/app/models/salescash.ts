export interface SalesCash {
    id: number;
    FECHA: string;
    ITEM: string;
    MARCA: string;
    TIPO: string;
    MODELO: string;
    CANTIDAD: string;
    PRECIO_UNIDAD: number;
    SUBTOTAL: number;
    IGV: number;
    TOTAL: number;
    PERSONAL_FISIOVIDA: string; //cliente
    CODIGO_VENTA: number;
    SEDE: string;
    USUARIO: string;
    TURNO: string;
}