import { Patient } from './patient';
import { Treatment } from './treatment';
import { User } from './user';

export interface Pending_Board {
    id: number;
    patient: Patient;
    treatment: Treatment;
    message: string;
    level: number;
    user: User;
}