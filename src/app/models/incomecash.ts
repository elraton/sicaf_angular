export interface IncomeCash {
    id:                 number;
    FECHA:              string;
    DNI:                string;
    NOMBRE_APELLIDO:    string;
    TIPO_PACIENTE:      string;
    TIPO_SERVICIO:      string;
    CANTIDAD:           number; // 1
    PRECIO_UNIDAD:      number; // 30
    SUBTOTAL:           number; // 30
    IGV:                number; // 5.1
    TOTAL:              number; // 35.1
    TOTAL_DESCUENTO:    number; // 
    DESCUENTO:          number; // 
    TOTAL_PAGADO:       number; // total de totales
    PROMO_MENSUAL:      string; // tabla 1 de promociones
    COD_PROMOCION:      string; // tabla 2 de promociones
    TIPO_CODIGO:        string; // de cual tabla escoge
    FORMA_PAGO:         string; // efectivo o tarjeta
    TIPO_COMPROBANTE:   string; // Boleta // factura // ticket
    RECEPTOR:           string; // usuario
    TURNO:              string; // turno
    SEDE:               string; 
}
