export interface Rate {
    id: number;
    CODIGO: string;
    PRECIO: string;
    SERVICIO: string;
    TIPO_PACIENTE: string;
    especial: string;
    familiar: string;
    fullprice: string;
    promocional: string;
    promotions: string;
}