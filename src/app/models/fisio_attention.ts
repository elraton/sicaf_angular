import { Calendar_Day } from './calendar_day'; 
import { Patient } from './patient';
import { Personal } from './personal';

export interface Fisio_Attention {
    id: number;
    patient: Patient;
    sessions: number;
    startdate: Date;
    interval: string;
    hour: string;
    hour_s: string;
    personal: Personal;
    calendar_day: Calendar_Day[];
}