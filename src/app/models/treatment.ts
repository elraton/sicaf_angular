export interface Treatment {
    id: number;
    DNI: string;
    P_NOMBRE: string;
    P_APELLIDO: string;
    TERAPEUTA: string;
    TERAPEUTA_ID: number;
    DOCTOR: string;
    DOCTOR_ID: number;
    ESTADO: string;
    INTERVALO: string;
    FECHA: Date;
    HORARIO: string;
    HORARIO2: string;
    S_PROGRAMADAS: number;
    S_REALIZADAS: number;
    S_PAGADAS: number;
    CONSULTA_GRATIS: number;
    SERVICIO: number;
    ABONO_TERAPIA: number;
    T_PROMOCION: number;
    HORARIOS: any;
    schedule: any;
}