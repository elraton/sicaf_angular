import { Personal_Schedule } from './personal_schedule';
import { Personal_Category } from './personal_category';

export interface Personal {
    id: number;
    DNI: string;
    NOMBRE: string;
    NOMBRE2: string;
    APELLIDO: string;
    APELLIDO2: string;
    CELULAR: string;
    TELEFONO: string;
    SEDE: string;
    TURNO: string;
    AREA: string;
    FECHA_INGRESO: Date;
    FECHA_NACIMIENTO: Date;
    DEPARTAMENTO: string;
    CIUDAD: string;
    DIRECCION: string;
    DISTRITO: string;
    ESTADO: string;
    CONDICION: string;
    EMAIL: string;
    ULTIMO_TRABAJO: string;
    ULTIMA_POSICION: string;
    ESTADO_CIVIL: string;
    NMRO_CUENTA: string;
    ES_DOCTOR: boolean;
    ES_NIÑOS: boolean;
    CATEGORIA: number;
    PASSWORD: string;
    horarios: any;
}
