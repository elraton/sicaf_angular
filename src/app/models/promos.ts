export interface Promos {
    id: number;
    CODIGO: string;
    DSCTO: number;
    CONSULTA: string;
    TIPO: string;
    IGV: number;
    ESTADO: string;
}