export interface Personal_Schedule {
    id: number;
    DIA: string;
    HORA_INICIO: string;
    HORA_FIN: string;
    NOMBRE: string;
    TIEMPO_ATENCION: number;
}
