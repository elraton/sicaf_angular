export interface Patient {
    id: number;
    DNI: string;
    P_NOMBRE: string;
    S_NOMBRE: string;
    P_APELLIDO: string;
    S_APELLIDO: string;
    DIRECCION: string;
    CELULAR: string;
    TELEFONO: string;
    F_NACIMIENTO: string;
    DISTRITO: string;
    EMAIL: string;
    TIPO_PACIENTE: string;
    NOMBRE_TUTOR: string;
    CELULAR_TUTOR: string;
    
    ASEGURADORA: string;
    TIPO_SEGURO: string;
    COPAGO_FIJO: number;
    COPAGO_VARIABLE: number;
    F_REGISTRO: string;

    HHCC: number;
    SEDE: string;

    CONVENIO?: number;
}