export class Statics {

    public DAYS = [ {value: 'lu',name: 'Lunes'},
        {value: 'ma', name: 'Martes'},
        {value: 'mi', name: 'Miercoles'},
        {value: 'ju', name: 'Jueves'},
        {value: 'vi', name: 'Viernes'},
        {value: 'sa', name: 'Sabado'}
    ];

    public HOURS = [
        {value: 7, name: '07'},
        {value: 8, name: '08'},
        {value: 9, name: '09'},
        {value: 10, name: '10'},
        {value: 11, name: '11'},
        {value: 12, name: '12'},
        {value: 1, name: '01'},
        {value: 2, name: '02'},
        {value: 3, name: '03'},
        {value: 4, name: '04'},
        {value: 5, name: '05'},
        {value: 6, name: '06'}
    ];

    public MINUTES = [
        {value: 0, name: '00'},
        {value: 30, name: '30'}
    ];

    public SEDES = [
        {value: 'RONDA RECOLETA', name: 'RONDA RECOLETA',},
        {value: 'TORRE MEDICA', name: 'TORRE MEDICA'},
        {value: 'LA PERLA', name: 'LA PERLA'}
    ];

    public DURATIONS = [
        {value: 15, name: '15 minutos'},
        {value: 30, name: '30 minutos'},
        {value: 60, name: '60 minutos'}
    ];

    public TIPO_TARIFA = [{name:'ESTANDAR', value:'FULLPRICE'}, {name:'PROMOCIONAL', value:'PROMOCIONAL'}, {name:'ESPECIAL', value:'ESPECIAL'}, {name: 'FAMILIAR', value: 'FAMILIAR'}];
    public TIPO_PACIENTE = ['SEGURO', 'PARTICULAR'];
    public TIPO_PERSONA = ['NIÑO', 'ADULTO'];
    public STATUS = ['ACTIVO', 'INACTIVO'];
    public AREAS = ['CONTABILIDAD', 'ADMINISTRACION', 'FISIOTERAPEUTA', 'RECEPCION'];
    public CIVIL_STATUS = ['SOLTERO', 'CASADO', 'CONVIVIENTE', 'DIVORCIADO', 'VIUDO'];
    public CONDITIONS = ['CONTRATO', 'PLANILLA'];
    public SHIFTS = ['MAÑANA', 'TARDE'];
    
    public ASEGURADORAS_PARTICULAR = [{name:'ADULTO', value:'ADULTO'}, {name:'NIÑO', value:'NIÑO'}];
    public ASEGURADORAS_SEGURO = [{name:'POSITIVA', value:'POSITIVA'}, {name:'RIMAC', value:'RIMAC'}, {name:'MAPFRE', value:'MAPFRE'}, {name:'PACIFICO', value:'PACIFICO'}, {name:'SANNA', value:'SANNA'}];

    public TIPO_SEGURO_POSITIVA = [{name:'SOAT', value:'SOAT'}];
    public TIPO_SEGURO_RIMAC_MAPFRE = [{name:'SOAT',value:'SOAT'}, {name:'EPS',value:'EPS'}, {name:'SCTR',value:'SCTR'}, {name:'ESTUDIANTIL',value:'ESTUDIANTIL'}];
    public TIPO_SEGURO_PACIFICO = [
        {name:'SOAT',value:'SOAT'},
        {name:'EPS',value:'EPS'},
        {name:'PPS',value:'PPS'},
        {name:'SEGUROS',value:'SEGUROS'},
        {name:'MULTISEGUROS',value:'MULTISEGUROS'},
        {name:'CERRO VERDE',value:'CERRO VERDE'},
        {name:'SCTR',value:'SCTR'},
        {name:'OTROS',value:'OTROS'}];
    public TIPO_SEGURO_SANNA = [{name:'CONVENIO',value:'CONVENIO'}]; //pacientes con el logo de sanna
    public TIPO_SEGURO_PARTIULAR = [{name:'ESTANDAR', value:'FULLPRICE'}, {name:'PROMOCIONAL', value:'PROMOCIONAL'}, {name:'ESPECIAL', value:'ESPECIAL'}, {name: 'FAMILIAR', value: 'FAMILIAR'}];
    
    public PAYMENT_METHOD = [{name:'EFECTIVO', value:'EFECTIVO'}, {name:'TARJETA', value:'TARJETA'}];
    public COMPROBANTES = [{name:'TICKET', value:'TICKET'}, {name:'BOLETA', value:'BOLETA'}, {name:'FACTURA', value:'FACTURA'}];

    //Items para venta
    public ITEMS = [
        { id: 1, value: 'ITEM 1'},
        { id: 2, value: 'ITEM 2'},
        { id: 3, value: 'ITEM 3'}
    ];

    public MARCA = [
        { id: 1, value: 'Marca 1'},
        { id: 2, value: 'Marca 2'},
        { id: 3, value: 'Marca 3'},
    ];

    public TIPO = [
        { id: 1, value: 'Tipo 1'},
        { id: 2, value: 'Tipo 2'},
        { id: 3, value: 'Tipo 3'},
    ];

    public MODELOS = [
        { id: 1, value: 'Modelo 1'},
        { id: 2, value: 'Modelo 2'},
        { id: 3, value: 'Modelo 3'},
    ];
}