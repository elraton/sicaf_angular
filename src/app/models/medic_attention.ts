import { Patient } from './patient';
import { Personal } from './personal';

export interface Medic_Attention {
    id: number;
    patient: Patient;
    personal: Personal;
    work_place: string;
    schedule: Date;
}