import { Patient } from './patient';
import { Personal } from './personal';
export interface Calendar_Day {
    id: number;
    patient: Patient;
    status: string;
    comments: string;
    schedule: Date;
    personal: Personal;
    current_session: number;
}