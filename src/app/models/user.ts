export interface User {
    id: number;
    NOMBRE: string;
    APELLIDO: string;
    email: string;
    username: string;
    password?: string;
    AREA: string;
    SEDE: string;
    TURNO: string;
    photo: string;
}