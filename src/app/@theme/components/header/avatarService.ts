import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of as observableOf,  Observable,  BehaviorSubject } from 'rxjs';

@Injectable()
export class AvatarService {
    currentPhoto = '';
    private photoSource = new BehaviorSubject(this.currentPhoto);

    constructor() {}

    changePhoto(photo: string) {
        console.log('se envio el mensaje');
        this.photoSource.next(photo)
    }

    getPhoto(): Observable<string> {
        return observableOf(this.currentPhoto);
    }
}