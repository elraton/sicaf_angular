import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService, NbMenuItem } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { IncomecashService } from '../../../pages/incomecash/incomecash.service';
import { filter, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ProfileService } from '../../../pages/profile/profile.service';
import { AvatarService } from './avatarService';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
  providers: [IncomecashService, AvatarService]
})
export class HeaderComponent implements OnInit {


  @Input() position = 'normal';

  user: any;

  photoConnection;

  // userMenu = [{ title: 'Perfil' }, { title: 'Salir' }];

  userMenu = [
    { title: 'Perfil', data: { id: 'profile' } },
    { title: 'Salir', data: { id: 'logout' } },
  ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserData,
              private service: IncomecashService,
              private analyticsService: AnalyticsService,
              private layoutService: LayoutService,
              private router: Router,
              private authService: NbAuthService,
              private avatarService: AvatarService,
              private profileService: ProfileService) {
      this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {

        if (token.isValid()) {
          this.user = token.getPayload(); // here we receive a payload from the token and assigne it to our `user` variable 
        }

      });
  }

  ngOnInit() {
    /*this.userService.getUsers()
      .subscribe((users: any) => this.user = users.nick);this.user.photo = message */

    this.photoConnection = this.profileService.getPhoto().subscribe( (message: string) => message.length > 0 ? console.log('ola q ase') : console.log('holi') );
    
    this.service.getUser().subscribe(
        data => {
          this.user = JSON.parse(JSON.stringify(data)).success;
          localStorage.setItem('rol', this.user.AREA);
        },
        error => {}
    )

    this.menuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title),
      )
      .subscribe( (title) => {
        if ( title == 'Perfil') {
          this.router.navigate(['/pages/profile']);
        }
        if ( title == 'Salir') {
          this.router.navigate(['/auth/login']);
        }
      });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  getImage() {
    return 'assets/images/avatar/' + this.user.photo;
  }

  changePhoto(photo) {
    this.user.photo = photo;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
}
