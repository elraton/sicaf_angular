import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Fisiovida 2019</span>
    <div class="socials">
      <a href="https://github.com/elraton" target="_blank" class="ion ion-social-github"></a>
      <a href="https://www.linkedin.com/in/jhosimar-mamani-70017a56/" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
