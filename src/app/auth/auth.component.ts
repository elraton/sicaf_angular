import { Component } from '@angular/core';

@Component({
  selector: 'ngx-auth',
  styleUrls: ['auth.component.scss'],
  template: `
  <div class="container">
      <router-outlet></router-outlet>
  </div>
  `,
})
export class AuthComponent { }
