import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { NbAuthComponent } from '@nebular/auth';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot/forgot.component';

const routes: Routes = [{
  path: '',
  component: NbAuthComponent,
  children: [{
    path: 'login',
    component: LoginComponent,
  }, {
    path: 'forgot',
    component: ForgotPasswordComponent,
  }, {
    path: '**',
    component: LoginComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
