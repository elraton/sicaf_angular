import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NbLoginComponent } from '@nebular/auth';

@Component({
  selector: 'ngx-auth-login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html',
})
export class LoginComponent extends NbLoginComponent {
  showError = false;
  showSuccess = false;
  loading = false;
  login() {
    this.showError = false;
    this.showSuccess = false;
    this.loading = true;
    this.service.authenticate('username', {username: this.user.username, password: this.user.password})
    .subscribe(
      response => {
        this.loading = false;

        const res = JSON.parse(JSON.stringify(response));

        if ( !res.success ) {
          this.showError = true;
          return;
        }

        this.showSuccess = true;
        let token2;
        
        token2 = JSON.parse(JSON.stringify(res.token));
        localStorage.setItem('token', token2.token);
        this.router.navigate(['/pages']);
      },
      error => {
        this.loading = false;
        this.showError = true;
      }
    );

  }
}